<!-- Side Navbar -->
<nav class="side-navbar">
    <div class="side-navbar-wrapper">
        <!-- Sidebar Header    -->
        <div class="sidenav-header d-flex align-items-center justify-content-center">
            <!-- User Info-->
            <div class="sidenav-header-inner text-center"><img src="img/avatar-1.jpg" alt="person" class="img-fluid rounded-circle">
            <h2 class="h5">Anderson Hardy</h2><span>Web Developer</span>
            </div>
            <!-- Small Brand information, appears on minimized sidebar-->
            <div class="sidenav-header-logo"><a href="index.html" class="brand-small text-center"> <strong>B</strong><strong class="text-primary">D</strong></a></div>
        </div>
        <!-- Sidebar Navigation Menus-->
        <div class="main-menu">
            <h5 class="sidenav-heading">Main</h5>
            <ul id="side-main-menu" class="side-menu list-unstyled">                  
                <li><a href="{{ url('/') }}"> <i class="icon-home"></i>Home</a></li>
                <li><a href="#userDropdown" aria-expanded="false" data-toggle="collapse"> <i class="icon-interface-windows"></i>User Management</a>
                    <ul id="userDropdown" class="collapse list-unstyled ">
                        <li><a href="{{ route('register') }}">Add User</a></li>
                        <li><a href="{{ route('users') }}">Users List</a></li>
                        <li><a href="{{ route('create.roles') }}">Add Roles</a></li>
                        <li><a href="{{ route('roles') }}">Roles List</a></li>
                    </ul>
                </li>
            </ul>
        </div>
        <div class="admin-menu">
            <h5 class="sidenav-heading">Second menu</h5>
            <ul id="side-admin-menu" class="side-menu list-unstyled"> 
                <li> <a href="{{ route('settings') }}"> <i class="icon-settings"> </i>Settings</a></li>
            </ul>
        </div>
    </div>
</nav>