<div class="card">
    <div class="card-header d-flex align-items-center">
        <h4>Primary Informations</h4>
    </div>
    <div class="card-body">
    
        <div class="form-group{{ $errors->has('name') ? ' is-invalid' : '' }}">
            
            <label>Full name</label>
            
            <input type="text" name="name" placeholder="Full name" class="form-control" value="{{ old('name') }}">

            @if ($errors->has('name'))
                <span class="invalid-feedback">
                    <strong>{{ $errors->first('name') }}</strong>
                </span>
            @endif

        </div>
        
        <div class="form-group{{ $errors->has('email') ? ' is-invalid' : '' }}">
            
            <label>Email</label>
            
            <input type="email" name="email" placeholder="Email Address" class="form-control" value="{{ old('email') }}">

            @if ($errors->has('email'))
                <span class="invalid-feedback">
                    <strong>{{ $errors->first('email') }}</strong>
                </span>
            @endif

        </div>

        <div class="form-group{{ $errors->has('password') ? ' is-invalid' : '' }}">

            <label>Password</label>

            <input type="password" name="password" placeholder="Password" class="form-control">

            @if ($errors->has('password'))
                <span class="invalid-feedback">
                    <strong>{{ $errors->first('password') }}</strong>
                </span>
            @endif

        </div>

        <div class="form-group">

            <label>Confirm Password</label>

            <input type="password" name="password_confirmation" placeholder="Password" class="form-control">

        </div>
        
        <div class="form-group{{ $errors->has('father_name') ? ' is-invalid' : '' }}">
            
            <label>Father name</label>
            
            <input type="text" name="father_name" placeholder="Father Name" class="form-control" value="{{ old('father_name') }}">

            @if ($errors->has('father_name'))
                <span class="invalid-feedback">
                    <strong>{{ $errors->first('father_name') }}</strong>
                </span>
            @endif

        </div>
        
        <div class="form-group{{ $errors->has('cnic') ? ' is-invalid' : '' }}">
            
            <label>C.N.I.C</label>
            
            <input type="text" name="cnic" placeholder="C.N.I.C" class="form-control" value="{{ old('cnic') }}">

            @if ($errors->has('cnic'))
                <span class="invalid-feedback">
                    <strong>{{ $errors->first('cnic') }}</strong>
                </span>
            @endif

        </div>
        
        <div class="form-group date{{ $errors->has('dob') ? ' is-invalid' : '' }}">
            
            <label>Date of birth</label>
            
            <input type="text" name="dob" placeholder="Date of Birth" class="form-control" value="{{ old('dob') }}">

            <span class="input-group-addon"><i class="glyphicon glyphicon-th"></i></span>

            @if ($errors->has('dob'))
                <span class="invalid-feedback">
                    <strong>{{ $errors->first('dob') }}</strong>
                </span>
            @endif

        </div>
        
        <div class="form-group{{ $errors->has('gender') ? ' is-invalid' : '' }}">
            
            <label>Gender</label>

            <select name="gender" class="form-control">
                  <option value=''>Choose</option>
                  <option {{ old('gender') == 'Male'? 'selected':'' }}>Male</option>
                  <option {{ old('gender') == 'Female'? 'selected':'' }}>Female</option>
                  <option {{ old('gender') == 'Other'? 'selected':'' }}>Other</option>
            </select>
            
            @if ($errors->has('gender'))
                <span class="invalid-feedback">
                    <strong>{{ $errors->first('gender') }}</strong>
                </span>
            @endif

        </div>
        
        <div class="form-group{{ $errors->has('phone') ? ' is-invalid' : '' }}">
            
            <label>Phone</label>
            
            <input type="text" name="phone" placeholder="Phone" class="form-control" value="{{ old('phone') }}">

            @if ($errors->has('phone'))
                <span class="invalid-feedback">
                    <strong>{{ $errors->first('phone') }}</strong>
                </span>
            @endif

        </div>
        
        <div class="form-group{{ $errors->has('address') ? ' is-invalid' : '' }}">
            
            <label>Address</label>
            
            <textarea name="address" id="address" cols="30" rows="5" class="form-control">{{ old('address') }}</textarea>

            @if ($errors->has('address'))
                <span class="invalid-feedback">
                    <strong>{{ $errors->first('address') }}</strong>
                </span>
            @endif

        </div>

    </div>
</div>