<div class="card">
    <div class="card-header d-flex align-items-center">
        <h4>Qualification Information</h4>
    </div>
    <div class="card-body">
        
        
        <div class="form-group{{ $errors->has('education') ? ' is-invalid' : '' }}">
            
            <label>Last Degree/Education</label>
            
            <input type="text" name="education" placeholder="Last Degree/Education" class="form-control" value="{{ old('education') }}">

            @if ($errors->has('education'))
                <span class="invalid-feedback">
                    <strong>{{ $errors->first('education') }}</strong>
                </span>
            @endif

        </div>
        
        
        <div class="form-group{{ $errors->has('institute') ? ' is-invalid' : '' }}">
            
            <label>Institute</label>
            
            <input type="text" name="institute" placeholder="Institute" class="form-control" value="{{ old('institute') }}">

            @if ($errors->has('institute'))
                <span class="invalid-feedback">
                    <strong>{{ $errors->first('institute') }}</strong>
                </span>
            @endif

        </div>
        
        
        <div class="form-group{{ $errors->has('passing_year') ? ' is-invalid' : '' }}">
            
            <label>Passing Year</label>
            
            <input type="text" name="passing_year" placeholder="Passing Year" class="form-control" value="{{ old('passing_year') }}">

            @if ($errors->has('passing_year'))
                <span class="invalid-feedback">
                    <strong>{{ $errors->first('passing_year') }}</strong>
                </span>
            @endif

        </div>
        
    </div>
</div>