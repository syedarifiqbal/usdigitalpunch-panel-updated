<div class="card">

    <div class="card-header d-flex align-items-center"><h4>Employee Details</h4></div>

    <div class="card-body">

        <div class="row">

            <div class="col-lg-6">
                <div class="form-group{{ $errors->has('roles_id') ? ' is-invalid' : '' }}">
                    <label>Roles</label>
                    
                    <select name="roles_id[]" class="form-control" multiple>
                        @foreach(App\Models\Role::all() as $role)
                            <option value="{{ $role->id }}" {{ in_array($role->id, old('roles_id', []))? 'selected':'' }}>{{ $role->label }}</option>
                        @endforeach
                    </select>

                    @if ($errors->has('roles_id'))
                        <span class="invalid-feedback">
                            <strong>{{ $errors->first('roles_id') }}</strong>
                        </span>
                    @endif

                </div>


                <div class="form-group{{ $errors->has('bonus') ? ' is-invalid' : '' }}">
                    
                    <label>Bonus Rate</label>
                    
                    <input type="text" name="bonus" placeholder="Bonus Rate" class="form-control" value="{{ old('bonus') }}">

                    @if ($errors->has('bonus'))
                        <span class="invalid-feedback">
                            <strong>{{ $errors->first('bonus') }}</strong>
                        </span>
                    @endif

                </div>

            </div>
            <!-- .col-lg-6 -->
            

            <div class="col-lg-6">
                
                <div class="form-group{{ $errors->has('additional_bonus') ? ' is-invalid' : '' }}">
                    
                    <label>Additional Bonus</label>
                    
                    <input type="text" name="additional_bonus" placeholder="Additional Bonus" class="form-control" value="{{ old('additional_bonus') }}">

                    @if ($errors->has('additional_bonus'))
                        <span class="invalid-feedback">
                            <strong>{{ $errors->first('additional_bonus') }}</strong>
                        </span>
                    @endif

                </div>
                
                <div class="form-group{{ $errors->has('revision_bonus') ? ' is-invalid' : '' }}">
                    
                    <label>Revision Bonus</label>
                    
                    <input type="text" name="revision_bonus" placeholder="Revision Bonus" class="form-control" value="{{ old('revision_bonus') }}">

                    @if ($errors->has('revision_bonus'))
                        <span class="invalid-feedback">
                            <strong>{{ $errors->first('revision_bonus') }}</strong>
                        </span>
                    @endif

                </div>
                
                <div class="form-group{{ $errors->has('sale_wrap_commision') ? ' is-invalid' : '' }}">
                    
                    <label>Salewrap Commission</label>
                    
                    <input type="text" name="sale_wrap_commision" placeholder="Salewrap Commission" class="form-control" value="{{ old('sale_wrap_commision') }}">

                    @if ($errors->has('sale_wrap_commision'))
                        <span class="invalid-feedback">
                            <strong>{{ $errors->first('sale_wrap_commision') }}</strong>
                        </span>
                    @endif

                </div>
                
            </div>
            <!-- .col-lg-6 -->

            <div class="col-lg-12">
                
                <div class="form-group">
                    <input type="submit" value="Signin" class="btn btn-primary">
                </div>
                
            </div>

        </div>
        <!-- .row -->
        
    </div>

</div>