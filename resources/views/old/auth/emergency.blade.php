<div class="card">
    <div class="card-header d-flex align-items-center"> <h4>Contact for emergency cases</h4> </div>

    <div class="card-body">

        <div class="form-group{{ $errors->has('contact_person') ? ' is-invalid' : '' }}">
            
            <label>Person Name</label>
            
            <input type="text" name="contact_person" placeholder="Person Name" class="form-control" value="{{ old('contact_person') }}">

            @if ($errors->has('contact_person'))
                <span class="invalid-feedback">
                    <strong>{{ $errors->first('contact_person') }}</strong>
                </span>
            @endif

        </div>
        
        
        <div class="form-group{{ $errors->has('contact_person_relation') ? ' is-invalid' : '' }}">
            
            <label>Relationship</label>
            
            <input type="text" name="contact_person_relation" placeholder="Relationship" class="form-control" value="{{ old('contact_person_relation') }}">

            @if ($errors->has('contact_person_relation'))
                <span class="invalid-feedback">
                    <strong>{{ $errors->first('contact_person_relation') }}</strong>
                </span>
            @endif

        </div>
        
        
        <div class="form-group{{ $errors->has('contact_person_phone') ? ' is-invalid' : '' }}">
            
            <label>Phone number</label>
            
            <input type="text" name="contact_person_phone" placeholder="Phone number" class="form-control" value="{{ old('contact_person_phone') }}">

            @if ($errors->has('contact_person_phone'))
                <span class="invalid-feedback">
                    <strong>{{ $errors->first('contact_person_phone') }}</strong>
                </span>
            @endif

        </div>
        
        <div class="form-group{{ $errors->has('contact_person_address') ? ' is-invalid' : '' }}">
            
            <label>Address</label>
            
            <textarea name="contact_person_address" id="contact_person_address" cols="30" rows="5" class="form-control">{{ old('contact_person_address') }}</textarea>

            @if ($errors->has('contact_person_address'))
                <span class="invalid-feedback">
                    <strong>{{ $errors->first('contact_person_address') }}</strong>
                </span>
            @endif

        </div>
        
    </div>
</div>