@extends('layouts.app')

@section('content')

<header><h1 class="h3 display">Settings</h1></header>

<div class="row">
        
    <div class="col-lg-12">
        
        <div class="card">
            <div class="card-header">
                <nav class="nav nav-pills nav-justified">
                    <a class="flex-sm-fill text-sm-center nav-link {{ request('tab', 'general') == 'general'? 'active': '' }}" href="{{ route('settings', ['tab'=>'general']) }}">General</a>
                    <a class="flex-sm-fill text-sm-center nav-link {{ request('tab') == 'email'? 'active': '' }}" href="{{ route('settings', ['tab'=>'email']) }}">Email</a>
                    <a class="flex-sm-fill text-sm-center nav-link {{ request('tab') == 'ui'? 'active': '' }}" href="{{ route('settings', ['tab'=>'ui']) }}">UI</a>
                </nav>
                <!-- <h4>Settings</h4> -->
            </div>
            <div class="card-body">
    
        <div class="form-group{{ $errors->has('name') ? ' is-invalid' : '' }}">
        
        <label>Company name</label>
        
        <input type="text" name="name" placeholder="Full name" class="form-control" value="{{ old('name', setting('name')) }}">

        @if ($errors->has('name'))
            <span class="invalid-feedback">
                <strong>{{ $errors->first('name') }}</strong>
            </span>
        @endif

    </div>
    
    <div class="form-group{{ $errors->has('email') ? ' is-invalid' : '' }}">
        
        <label>Company name</label>
        
        <input type="email" name="email" placeholder="Full name" class="form-control" value="{{ old('email', setting('email')) }}">

        @if ($errors->has('email'))
            <span class="invalid-feedback">
                <strong>{{ $errors->first('email') }}</strong>
            </span>
        @endif

    </div>
    
    <div class="form-group{{ $errors->has('phone') ? ' is-invalid' : '' }}">
        
        <label>Company name</label>
        
        <input type="text" name="phone" placeholder="Full name" class="form-control" value="{{ old('phone', setting('phone')) }}">

        @if ($errors->has('phone'))
            <span class="invalid-feedback">
                <strong>{{ $errors->first('phone') }}</strong>
            </span>
        @endif

    </div>
    
    <div class="form-group{{ $errors->has('country') ? ' is-invalid' : '' }}">
        
        <label>Country</label>
        
        <select name="country" class="form-control selectApi" data-api-endpoint="{{ route('select.country') }}"></select>

        @if ($errors->has('country'))
            <span class="invalid-feedback">
                <strong>{{ $errors->first('country') }}</strong>
            </span>
        @endif

    </div>
        
        <div class="form-group{{ $errors->has('address') ? ' is-invalid' : '' }}">
            
            <label>Address</label>
            
            <textarea name="address" id="address" cols="30" rows="5" class="form-control" placeholder="Please add you company adderss">{{ old('address') }}</textarea>

            @if ($errors->has('address'))
                <span class="invalid-feedback">
                    <strong>{{ $errors->first('address') }}</strong>
                </span>
            @endif

        </div>
                
            </div>
        </div>
    </div>
</div>

@endsection

@section('header_styles')

@include('components.select2-stylesheets');

@endsection

@section('footer_scripts')

@include('components.select2-scripts');

<script>

   var options = {
      	//   "dom": "Bfrtip",
      	    "processing":true,
    	    "serverSide":true,
    	    "order":[],
      		"ajax": {
      			url: $('#datatable-url').val(),
      			// type:"POST"
      		},
        	"buttons": tableButtons,
            "responsive": true,
            "initComplete": function(settings, json) {}
    };
    
    // $("#datatable").DataTable(options);

</script>
@endsection