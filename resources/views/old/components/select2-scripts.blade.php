<script src="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.6-rc.0/js/select2.min.js"></script>

<script>
    $(function(){

        $.ajaxSetup({
            headers: { 'X-CSRF-Token' : $('meta[name=_token]').attr('content') }
        });

        $('.selectApi').select2({
            "width":"100%",
            ajax : {
                // url : '{{-- route("select2customer") --}}',
                url : function(){ return $(this).data('api-endpoint'); },
                dataType : 'json',
                method: 'POST',
                delay : 200,
                data : function(params){
                    return {
                        '_token' : '{{ csrf_token() }}',
                        q : params.term,
                        page : params.page
                    };
                },
                processResults : function(data, params){
                    // console.log(params);
                    params.page = params.current_page || 1;
                    return {
                        //results : data,
                        results: $.map(data.data, function (item) {
                            return {
                                text: item.name,                        
                                id: item.id,
                                flag: item.flag,
                            }
                        }),
                        pagination: {
                            more : (params.page  * 10) < data.total
                        }
                    };
                }
            },
            minimumInputLength : 1,

            templateResult : function (data){
                if(data.loading) return data.text;
                var markup = "<img src="+data.flag+" style='width: 50px; height: auto;'></img> &nbsp; "+ data.text;
                return markup;
            },
            templateSelection : function(data)
            {
                return data.text;
                return "<img src="+data.flag+" style='width: 50px; height: auto;'></img> &nbsp; "+ data.text;
            },
            escapeMarkup : function(markup){ return markup; }
        });
    });
    
</script>