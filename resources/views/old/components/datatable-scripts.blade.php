<script src="https://cdn.datatables.net/1.10.16/js/jquery.dataTables.min.js"></script>
<script src="https://cdn.datatables.net/1.10.16/js/dataTables.bootstrap4.min.js"></script>
<script src="https://cdn.datatables.net/fixedheader/3.1.3/js/dataTables.fixedHeader.min.js"></script>
<script src="https://cdn.datatables.net/responsive/2.2.1/js/dataTables.responsive.min.js"></script>
<script src="https://cdn.datatables.net/responsive/2.2.1/js/responsive.bootstrap.min.js"></script>

<script>
$(function (){

	var DataTable = $.fn.dataTable;
	/* Set the defaults for DataTables initialisation */
	$.extend( true, DataTable.defaults, {
		"processing":true,
		"serverSide":true,
		"order":[],
		"ajax": {
			url: $('#datatable-url').val(),
			// type:"POST"
			'_token' : $('meta[name=_token]').attr('content')
		},
		
		// "buttons": tableButtons,
		"responsive": true,

		dom:
			"<'row'<'col-sm-12 col-md-6'l><'col-sm-12 col-md-6'<'#searchType'>f>>" +
			"<'row'<'col-sm-12'tr>>" +
			"<'row'<'col-sm-12 col-md-5'i><'col-sm-12 col-md-7'p>>",
		renderer: 'bootstrap'
	} );

	 var table = $('table').DataTable();
 
    new $.fn.dataTable.FixedHeader( table );

	$("#searchType").html('<select class="form-control form-control-sm"><option>Active</option><option>Inactive</option></select>');

});

</script>