@extends('layouts.app')

@section('content')

<header><h1 class="h3 display">User List</h1></header>

<div class="row">
        
    <div class="col-lg-12">
        
        <div class="card">
            <div class="card-header">
                <h4>Compact Tables</h4>
            </div>
            <div class="card-body">
                <!-- <div class="table-responsive"> -->
                <input type="hidden" id="datatable-url" value="{{ route('dt.users', request('status',null) ? ['status'=>request('status',null)]:null) }}">
                <table class="table table-striped table-sm" id="datatable">
                    <thead>
                        <tr>
                            <th>Full name</th>
                            <th>Email</th>
                            <th>Phone</th>
                            <th>Role</th>
                            <th>Address</th>
                            <th>Status</th>
                            <th>Action</th>
                        </tr>
                    </thead>
                    <tbody>
                    </tbody>
                </table>
                <!-- </div> -->
            </div>
        </div>
    </div>
</div>

@endsection

@section('header_styles')

@include('components.datatable-stylesheets');

@endsection

@section('footer_scripts')

@include('components.datatable-scripts');

<script>

   var options = {
      	//   "dom": "Bfrtip",
      	    "processing":true,
    	    "serverSide":true,
    	    "order":[],
      		"ajax": {
      			url: $('#datatable-url').val(),
      			// type:"POST"
      		},
        	"buttons": tableButtons,
            "responsive": true,
            "initComplete": function(settings, json) {}
    };
    
    // $("#datatable").DataTable(options);

</script>
@endsection