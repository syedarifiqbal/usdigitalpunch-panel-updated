<?php
$user = auth()->user();
$menu = isset($menu)? $menu:'';
$submenu = isset($submenu)? $submenu:'';
?>

<aside class="main-sidebar hidden-print " >

    <section class="sidebar" id="sidebar-scroll">
        
        <div class="user-panel">
            
            <div class="f-left image">
                <img src="{{ asset('assets/images/avatar-1.png') }}" alt="{{ auth()->user()->name }}" class="img-circle">
            </div>

            <div class="f-left info">
                <p>{{ auth()->user()->name }}</p>
                <p class="designation">{{ auth()->user()->title?? 'User' }} <i class="icofont icofont-caret-down m-l-5"></i></p>
            </div>

        </div>
        <!-- sidebar profile Menu-->
        <ul class="nav sidebar-menu extra-profile-list">
            <li>
                <a class="waves-effect waves-dark" href="{{ route('profile') }}" target="_blank">
                    <i class="icon-user"></i>
                    <span class="menu-text">View Profile</span>
                    <span class="selected"></span>
                </a>
            </li>
            <li>
                <a class="waves-effect waves-dark" href="{{ route('settings') }}" target="_blank">
                    <i class="icon-settings"></i>
                    <span class="menu-text">Settings</span>
                    <span class="selected"></span>
                </a>
            </li>
            <li>
                <a class="waves-effect waves-dark" href="{{ route('logout') }}"
                   onclick="event.preventDefault();
                                                     document.getElementById('logout-form').submit();">
                    <i class="icon-logout"></i>
                    <span class="menu-text">Logout</span>
                    <span class="selected"></span>
                </a>
            </li>
        </ul>
        <!-- Sidebar Menu-->
        <ul class="sidebar-menu">
            <li class="nav-level">Navigation</li>
            
            <li class="treeview{{ activeMenu($menu, 'dashboard') }}">
                <a class="waves-effect waves-dark" href="{{ url('') }}">
                    <i class="icon-speedometer"></i><span> Dashboard</span>
                </a>                
            </li>
            @can('show-all-company')
                <li class="treeview{{ activeMenu($menu, 'Company') }}">
                    <a class="waves-effect waves-dark" href="{{ route('choose.company') }}">
                        <i class="icon-speedometer"></i><span> Select Company</span>
                    </a>
                </li>
            @endcan

            <li class="nav-level">Transactions</li>

            @if( $user->can('add-client') || $user->can('view-client'))
            <li class="treeview{{ activeMenu($menu, 'clients') }}" target="_blank">
                <a class="waves-effect waves-dark" href="#!">
                    <i class="icon-briefcase"></i>
                    <span> Clients</span>
                    <i class="icon-arrow-down"></i>
                </a>
                <ul class="treeview-menu">
                    @can('add-client')
                    <li class="{{ activeMenu($submenu, 'add-client') }}">
                        <a class="waves-effect waves-dark" href="{{ route('clients.create') }}" target="_blank">
                            <i class="icon-arrow-right"></i> New Client
                        </a>
                    </li>
                    @endcan

                    @can('view-client')
                    <li class="{{ activeMenu($submenu, 'view-client') }}">
                        <a class="waves-effect waves-dark" href="{{ route('clients') }}" target="_blank">
                            <i class="icon-arrow-right"></i> View Clients
                        </a>
                    </li>
                    @endcan
                </ul>
            </li>
            @endif
            
            @if( $user->can('add-task') || $user->can('view-task'))
            <li class="treeview{{ activeMenu($menu, 'tasks') }}">
                <a class="waves-effect waves-dark" href="#!">
                    <i class="icon-briefcase"></i>
                    <span> Orders Management</span>
                    <i class="icon-arrow-down"></i>
                </a>
                <ul class="treeview-menu">
                    @can('add-task')
                    <li class="{{ activeMenu($submenu, 'add-task') }}">
                        <a class="waves-effect waves-dark" href="{{ route('tasks.create') }}" target="_blank">
                            <i class="icon-arrow-right"></i> New Task
                        </a>
                    </li>
                    @endcan

                    @can('view-task')
                    <li class="{{ activeMenu($submenu, 'view-task', !request('status')) }}">
                        <a class="waves-effect waves-dark" href="{{ route('tasks', ['design_type'=>'digitizing']) }}" target="_blank">
                            <i class="icon-arrow-right"></i> View Tasks
                        </a>
                    </li>
                    @endcan

                    @can('send-task')
                    <li class="{{ activeMenu($submenu, 'view-task', !!request('status')) }}">
                        <a class="waves-effect waves-dark" href="{{ route('tasks', ['status'=>'rfs']) }}" target="_blank">
                            <i class="icon-arrow-right"></i> RFS Tasks
                        </a>
                    </li>
                    @endcan
                </ul>
            </li>
            @endif

            <li class="treeview{{ activeMenu($menu, 'users') }}">
                <a class="waves-effect waves-dark" href="#!">
                    <i class="icon-user-follow"></i>
                    <span> Users Management</span>
                    <i class="icon-arrow-down"></i>
                </a>
                <ul class="treeview-menu">
                    
                    @can('add-user')
                    <li class="{{ activeMenu($submenu, 'add-user') }}">
                        <a class="waves-effect waves-dark" href="{{ route('register') }}" target="_blank">
                            <i class="icon-arrow-right"></i> Add User
                        </a>
                    </li>
                    @endcan
                    
                    @can('view-user')
                    <li class="{{ activeMenu($submenu, 'view-user') }}">
                        <a class="waves-effect waves-dark" href="{{ route('users') }}" target="_blank">
                            <i class="icon-arrow-right"></i> Users List
                        </a>
                    </li>
                    @endcan
                    
                    @can('add-role')
                    <li class="{{ activeMenu($submenu, 'add-role') }}">
                        <a class="waves-effect waves-dark" href="{{ route('roles.create') }}" target="_blank">
                            <i class="icon-arrow-right"></i> Add Role
                        </a>
                    </li>
                    @endcan

                    @can('view-role')
                    <li class="{{ activeMenu($submenu, 'view-role') }}">
                        <a class="waves-effect waves-dark" href="{{ route('roles') }}" target="_blank">
                            <i class="icon-arrow-right"></i> Roles List
                        </a>
                    </li>
                    @endcan
                    @if(auth()->user()->is_developer)
                    <li class="{{ activeMenu($submenu, 'add-permission') }}">
                        <a class="waves-effect waves-dark" href="{{ route('permissions.create') }}">
                            <i class="icon-arrow-right"></i> Add Permission
                        </a>
                    </li>
                    <li class="{{ activeMenu($submenu, 'view-permission') }}">
                        <a class="waves-effect waves-dark" href="{{ route('permissions') }}">
                            <i class="icon-arrow-right"></i> Permissions List
                        </a>
                    </li>
                    @endif
                </ul>
            </li>

            <li class="treeview">
                <a class="waves-effect waves-dark" href="#!">
                    <i class="icon-user-follow"></i>
                    <span> Reports</span>
                    <i class="icon-arrow-down"></i>
                </a>

                <ul class="treeview-menu">
                    
                    @can('view-bonus')
                    <li class="{{ activeMenu($submenu, 'view-bonus') }}">
                        <a class="waves-effect waves-dark" href="{{ route('bonus') }}" target="_blank">
                            <i class="icon-arrow-right"></i> Bonus
                        </a>
                    </li>
                    @endcan
                    
                    @can('view-today-tasks-list')
                    <li class="{{ activeMenu($submenu, 'view-bonus') }}">
                        <a class="waves-effect waves-dark" href="{{ route('reports.today_task') }}" target="_blank">
                            <i class="icon-arrow-right"></i> Today's Tasks
                        </a>
                    </li>
                    @endcan

                    @can('order-report')
                    <li class="{{ activeMenu($submenu, 'order-report') }}">
                        <a class="waves-effect waves-dark" href="{{ route('reports.today_task') }}" target="_blank">
                            <i class="icon-arrow-right"></i> Orders Report
                        </a>
                    </li>
                    @endcan

                    @can('customer-invoice')
                    <li class="{{ activeMenu($submenu, 'order-report') }}">
                        <a class="waves-effect waves-dark" href="{{ route('reports.orders.invoice') }}" target="_blank">
                            <i class="icon-arrow-right"></i> Customer Invoice
                        </a>
                    </li>
                    @endcan

                    @can('sales-bonus')
                    <li class="{{ activeMenu($submenu, 'sales-bonus') }}">
                        <a class="waves-effect waves-dark" href="{{ route('reports.orders.sales') }}" target="_blank">
                            <i class="icon-arrow-right"></i> Sales Bonus
                        </a>
                    </li>
                    @endcan

                    @can('sales-customers')
                    <li class="{{ activeMenu($submenu, 'sales-customer') }}">
                        <a class="waves-effect waves-dark" href="{{ route('reports.sales.customers.orders') }}" target="_blank">
                            <i class="icon-arrow-right"></i> Sales Customer Report
                        </a>
                    </li>
                    @endcan
                </ul>
            </li>

            @if(in_array('Super Admin', request('roles')))
            <li class="treeview">
                <a class="waves-effect waves-dark" href="{{ route('settings') }}" target="_blank">
                    <i class="icon-speedometer"></i><span> Settings</span>
                </a>                
            </li>
            @endif
        </ul>
    </section>
</aside>