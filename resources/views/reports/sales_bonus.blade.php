<!doctype html>
<html lang="en">
    <head>
        <!-- Required meta tags -->
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

        <!-- Bootstrap CSS -->
        <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.1.0/css/bootstrap.min.css" integrity="sha384-9gVQ4dYFwwWSjIDZnLEWnxCjeSWFphJiwGPXr1jddIhOegiu1FwO5qRGvFXOdJZ4" crossorigin="anonymous">

        <title>Bonus Report</title>

        <style>
            /* header{
                position: fixed;
                top: 0;
                width: 100%;
                height: 200px;
            }
            body{
                margin-top: 150px;
            } */
        </style>

    </head>
    <body>
    
        <div class="container">
            <header>
                <div class="row">
                    <div class="col">
                        <h1 class="text-center"><u>Sales Bonus Report</u></h1>
                        <p>User Name: {{ $user->name }} <br>
                        Date From {{ $from }} to {{ $to }}</p>
                    </div>
                </div>
            </header>

            <table class="table table-bordered table-striped">
                <tr>
                    <th>Date</th>
                    <th>Ref</th>
                    <th>Design</th>
                    <th>Type</th>
                    <th>Order</th>
                    <th>Bonus</th>
                </tr>
                @foreach($bonuses as $b)
                    <tr>
                        <td>{{ $b->date->format('d/m/Y') }}</td>
                        <td>{{ $b->id }}</td>
                        <td>{{ $b->name }}</td>
                        <td>{{ $b->design_type }}</td>
                        <td>{{ $b->bonus_type }}</td>
                        <td class="text-right">{{ number_format($user->sale_wrap_commision, 2) }}</td>
                    </tr>
                @endforeach
                <tr>
                    <th>Total</th>
                    <th></th>
                    <th></th>
                    <th></th>
                    <th>{{ $bonuses->count() }}</th>
                    <th class="text-right">{{ number_format($bonuses->count() * $user->sale_wrap_commision, 2) }}</th>
                </tr>

            </table>
        </div>

    </body>
</html>

