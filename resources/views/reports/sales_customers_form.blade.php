@extends('layouts.master')

@section('styles')

    <!-- <link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet"> -->
    <link rel="stylesheet" href="http://cdn.materialdesignicons.com/2.4.85/css/materialdesignicons.min.css">

    {{--<link rel="stylesheet" href="{{ asset('assets/plugins/bootstrap/css/bootstrap.min.css') }}">--}}
    <link rel="stylesheet" href="{{ asset('assets/plugins/bootstrap-material-datetimepicker/css/bootstrap-material-datetimepicker.css') }}">

    <link href="{{ asset('css/customize-datatable.css') }}" rel="stylesheet">

    <style>
        div.material-table table tfoot th:first-child{
            text-align: left;
        }
    </style>

@endsection

@section('content')

    <div class="row">
        <!-- Form Control starts -->
        <div class="col-lg-12">
            <div class="card">
                <div class="card-header"><h5 class="card-header-text">Filters</h5>

                </div>

                <div class="card-block">
                    <form class="form-inline" method="post" id="filterForm">
                        @csrf
                        <div class="form-group">
                            <label class="sr-only form-control-label" for="from_date">From Date</label>
                            <input type="text" class="form-control date" name="from" id="from_date" placeholder="From Date" readonly value="">
                        </div>
                        <div class="form-group">
                            <label class="sr-only form-control-label" for="end_date">End Date</label>
                            <input type="text" class="form-control date" name="to" id="end_date" placeholder="End Date" readonly value="">
                        </div>
                        <div class="form-group">
                            <label class="sr-only form-control-label" for="end_date">User</label>
                            <select name="user_id" class="form-control">
                                @foreach(App\User::where('sale_wrap_commision', '>', 0)->get() as $user)
                                    <option value="{{ $user->id }}">{{ $user->name }}</option>
                                @endforeach
                            </select>
                        </div>
                        <button type="submit" class="btn btn-primary waves-effect waves-light">Submit</button>
                    </form>
                </div>
            </div>
        </div>
        <!-- Form Control ends -->
    </div>

@endsection

@section('footer_scripts')

    <script src="{{ asset('assets/plugins/moment/moment.js') }}"></script>
    <script src="{{ asset('assets/plugins/bootstrap-datepicker/dist/js/bootstrap-datepicker.min.js') }}"></script>
    <script src="{{ asset('assets/plugins/bootstrap-datepicker/js/bootstrap-datetimepicker.min.js') }}"></script>

    <script src="{{ asset('assets/plugins/datepicker/js/moment-with-locales.min.js') }}"></script>
    <script src="{{ asset('assets/plugins/bootstrap-material-datetimepicker/js/bootstrap-material-datetimepicker.js') }}"></script>

    <script>
        $(function () {
            $('.date').bootstrapMaterialDatePicker({
                time: false,
                clearButton: true,
                format: 'YYYY-MM-DD'
            });
        });
    </script>

@endsection