<!doctype html>
<html lang="en">
<head>
    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

    <title>Invoice @if($from_date && $from_date) | {{ $from_date }} - {{ $end_date }} @endif</title>

    <style>
        header {
            position: fixed;
            top: 0;
            width: 100%;
            height: 150px;
        }

        body {
            margin-top: 100px;
            font-size: 12px;
        }
        
        table{
            border-collapse: collapse;
            width: 100%;
        }

        table tr th {
            text-transform: capitalize;
        }

        table tr th, table tr td {
            border: 1px solid #ddd;
            padding: 5px;
        }

        table tr:nth-child(odd) {
            background-color: #fafafa;
        }
        
        .text-center{
            text-align:center;
        }
    </style>

</head>
<body>

<header>
    <div class="row">
        <div class="col">
            <h1 class="text-center"><u>Customer Order Report</u></h1>
            <p>Customer Name: {{ $client->name }} <br>
                Date Range: @if($from_date && $from_date) {{ $from_date }} - {{ $end_date }} @else ALL @endif
            </p>
        </div>
    </div>
</header>

<?php
$total = [];
$tip = [];
$column = ['new' => 'bonus', 'checking' => 'approval_bonus', 'revision' => 'revision_bonus', 'edit' => 'edit_bonus'];
?>

<table class="table table-bordered table-striped">
    <tr>
        <th>S.NO</th>
        <th>DATE</th>
        <th>DESIGN</th>
        <th>TYPE</th>
        <th>ORDER</th>
        <th>SUBMITTED TIME</th>
        <th>STATUS</th>
        <th>PRICE</th>
    </tr>
    <?php $counter = 0; ?>
    @foreach($orders as $order)
        <tr>
            <td>{{ ++$counter }}</td>
            <td>{{ $order->date->format('d/m/Y') }}</td>
            <td>{{ $order->name }}</td>
            <td>{{ $order->design_type }}</td>
            <td>{{ $order->order_type }}</td>
            <td>{{ $order->delivery_date->format('d/m/Y h:i:s') }}</td>
            <td>{{ $order->status }}</td>
            <td class="text-right">{{ $order->getCurrency() }} {{ number_format($order->price) }}</td>
        </tr>
    @endforeach

    <tr>
        <th colspan="7">Total</th>
        <th class="text-right">{{ $orders[0]->getCurrency() }} {{ number_format($orders->sum('price')) }}</th>
    </tr>
</table>

</body>
</html>

