<!doctype html>
<html lang="en">
    <head>
        <!-- Required meta tags -->
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

        <!-- Bootstrap CSS -->
        <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.1.0/css/bootstrap.min.css" integrity="sha384-9gVQ4dYFwwWSjIDZnLEWnxCjeSWFphJiwGPXr1jddIhOegiu1FwO5qRGvFXOdJZ4" crossorigin="anonymous">

        <title>Sales Customers Report</title>

        <style>
            header{
                position: fixed;
                top: 0;
                width: 100%;
                height: 200px;
            }
            body{
                margin-top: 150px;
            }
        </style>

    </head>
    <body>
        <header>
            <div class="row">
                <div class="col">
                    <h1 class="text-center"><u>Sales Customers Report</u></h1>
                    <p>User Name: {{ $user->name }} <br>
                    Date From {{ $from }} to {{ $to }}</p>
                </div>
            </div>
        </header>

        <table class="table table-bordered table-striped">
            <tr>
                <th>Sr.</th>
                <th>Customer name</th>
                <th>Nick name</th>
                <th>Email</th>
                <th>Total Order</th>
            </tr>
            @foreach($clients as $client)
                <tr>
                    <td>{{ $loop->index+1 }}</td>
                    <td>{{ $client->name }}</td>
                    <td>{{ $client->nick_name }}</td>
                    <td><a href="mailto:{{ strtolower($client->email) }}">{{ $client->email }}</a></td>
                    <td class="text-right">{{ number_format($client->orders_count) }}</td>
                </tr>
            @endforeach

            <tr>
                <th>Total</th>
                <th></th>
                <th></th>
                <th></th>
                <th class="text-right">{{ number_format($clients->sum('orders_count')) }}</th>
            </tr>

        </table>

    </body>
</html>

