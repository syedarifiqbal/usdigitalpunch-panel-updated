@extends('layouts.master')

@section('styles')

    <!-- <link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet"> -->
    <link rel="stylesheet" href="http://cdn.materialdesignicons.com/2.4.85/css/materialdesignicons.min.css">

    {{--<link rel="stylesheet" href="{{ asset('assets/plugins/bootstrap/css/bootstrap.min.css') }}">--}}
    <link rel="stylesheet" href="{{ asset('assets/plugins/bootstrap-material-datetimepicker/css/bootstrap-material-datetimepicker.css') }}">

    <link href="{{ asset('css/customize-datatable.css') }}" rel="stylesheet">

    <style>
        div.material-table table tfoot th:first-child{
            text-align: left;
        }
    </style>

@endsection

@section('content')

    <div class="row">
        <!-- Form Control starts -->
        <div class="col-lg-12">
            <div class="card">
                <div class="card-header"><h5 class="card-header-text">Filters</h5>

                </div>

                <div class="card-block">
                    <form class="form-inline" method="post" id="filterForm">
                        <div class="form-group">
                            <label class="sr-only form-control-label" for="from_date">From Date</label>
                            <input type="text" class="form-control date" id="from_date" placeholder="From Date" readonly value="{{ $from_date }}">
                        </div>
                        <div class="form-group">
                            <label class="sr-only form-control-label" for="end_date">End Date</label>
                            <input type="text" class="form-control date" id="end_date" placeholder="End Date" readonly value="{{ $end_date }}">
                        </div>
                        <div class="form-group">
                            <label class="sr-only form-control-label" for="design_type">Order Type</label>
                            <select name="design_type" id="design_type"  class="form-control">
                                <option value="all"{{ $design_type=='all' ? ' selected':'' }}>All</option>
                                <option value="Digitizing"{{ $design_type=='Digitizing' ? ' selected':'' }}>Digitize</option>
                                <option value="Vector"{{ $design_type=='Vector' ? ' selected':'' }}>Vector</option>
                                <option value="Quotation"{{ $design_type=='Quotation' ? ' selected':'' }}>Quote</option>
                            </select>
                        </div>
                        <button type="submit" class="btn btn-primary waves-effect waves-light">Filter</button>
                    </form>
                </div>
            </div>
        </div>
        <!-- Form Control ends -->
    </div>


    <div id="admin" class="col s12">
        <div class="card material-table">
            <div class="table-header">
                <span class="table-title">Customers Invoices</span>
                <div class="actions">
                    <input type="hidden" id="datatable-url" value="{{ route('dt.clients', request('status',null) ? ['status'=>request('status',null)]:null) }}">
                    <a href="#" class="table-search-toggle waves-effect btn-flat nopadding"><i class="mdi mdi-magnify"></i></a>
                </div>
            </div>
            <table id="simple-datatable">
                <thead>
                <tr>
                    <th>Date</th>
                    <th>Customer</th>
                    <th>Last Order</th>
                    <th>Email</th>
                    <th>Total Orders</th>
                    <th>Actions</th>
                </tr>
                </thead>
                <tbody>
                    @foreach($customers as $customer)
                        <tr>
                            <td>{{ optional(optional($customer->latestOrder)->created_at)->format('d/m/Y') }}</td>
                            <td>{{ $customer->name }}</td>
                            <td>{{ optional($customer->latestOrder)->name }}</td>
                            <td>{{ $customer->email }}</td>
                            <td>{{ $customer->orders_count }}</td>
                            <td>
                                @if($customer->orders_count)
                                    <div class="btn-group">
                                        <a class="btn btn-primary btn-sm" href="{{ route('reports.orders.invoice.pdf', ['client' => $customer->id, 'from_date' => $from_date, 'end_date' => $end_date]) }}">
                                            <i class="mdi mdi-file-pdf" style="color:white"></i>
                                        </a>
                                        <a class="btn btn-info btn-sm" href="{{ route('reports.orders.invoice.excel', ['client' => $customer->id, 'from_date' => $from_date, 'end_date' => $end_date]) }}">
                                            <i class="mdi mdi-file-excel" style="color:white"></i>
                                        </a>
                                    </div>
                                @endif
                            </td>
                        </tr>
                    @endforeach
                </tbody>
                <tfoot>
                    <tr>
                        <th colspan="4">
                            Total New <br>
                            Total Revision <br>
                            Total Edit
                        </th>
                        <th colspan="2">
                            {{ $customers->sum('orders_count') }} <br>
                            {{ $totalRevision }} <br>
                            {{ $totalEdit }}
                        </th>
                    </tr>
                </tfoot>
            </table>
        </div>
    </div>

@endsection

@section('footer_scripts')

    <script src="{{ asset('assets/plugins/moment/moment.js') }}"></script>
    <script src="{{ asset('assets/plugins/bootstrap-datepicker/dist/js/bootstrap-datepicker.min.js') }}"></script>
    <script src="{{ asset('assets/plugins/bootstrap-datepicker/js/bootstrap-datetimepicker.min.js') }}"></script>

    <script src="{{ asset('assets/plugins/datepicker/js/moment-with-locales.min.js') }}"></script>
    <script src="{{ asset('assets/plugins/bootstrap-material-datetimepicker/js/bootstrap-material-datetimepicker.js') }}"></script>

    <script>
        $(function () {

            $('.date').bootstrapMaterialDatePicker({
                time: false,
                clearButton: true,
                format: 'YYYY-MM-DD'
            });

            $('#filterForm').on('submit', function(e){
                e.preventDefault();
                
                var from_date = $('#from_date').val();
                var end_date = $('#end_date').val();
                var design_type = $('#design_type').val();
                var url = '{{ route('reports.orders.invoice') }}';
                if(!$.trim(from_date) || !end_date.trim())
                {
                    alert('Please choose from and end date');
                    return;
                }
                // window.location = url + '/' + from_date + '/' + end_date + '/' + design_type;
                window.open(url + '/' + from_date + '/' + end_date + '/' + design_type);
            })
        });
    </script>

    @include('components.datatable-scripts')

@endsection