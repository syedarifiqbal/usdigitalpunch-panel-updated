<!doctype html>
<html lang="en">
<head>
    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

    <!-- Bootstrap CSS -->
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.1.0/css/bootstrap.min.css" integrity="sha384-9gVQ4dYFwwWSjIDZnLEWnxCjeSWFphJiwGPXr1jddIhOegiu1FwO5qRGvFXOdJZ4" crossorigin="anonymous">

    <title>Bonus Report</title>

    {{--<style>
        header{
            position: fixed;
            top: 0;
            width: 100%;
            height: 200px;
        }
        body{
            margin-top: 150px;
        }
    </style>--}}

</head>
<body>

<section class="container">
    <div class="row">
        <div class="col">
            <header>
                <div class="row">
                    <div class="col">
                        <h1 class="text-center"><u>Bonus Report</u></h1>
                        <p>User Name: {{ $user->name }} <br>
                            Type: {{ $type }} <br>
                            Date From {{ $from->format('d/m/Y') }} to {{ $to->format('d/m/Y') }}</p>
                    </div>
                </div>
            </header>
        </div>
    </div>
</section>
<?php
$total = [];
$tip = [];
$edit = 0;
// bonus type => column name in user table.
$column = ['new' => 'bonus', 'checking' => 'approval_bonus', 'revision' => 'revision_bonus', 'edit' => 'edit_bonus', 'wrong job' => 'new'];
//    dd($bonuses);
?>


<section class="container">
    <div class="row">
        <div class="col">
            <table class="table table-bordered table-striped">
                <tr>
                    <th>Date</th>
                    <th>Ref</th>
                    <th>Design</th>
                    <th>Type</th>
                    <th>Order</th>
                    <th>Bonus</th>
                    <th>Tip</th>
                </tr>
                @foreach($bonuses as $b)
                    @php
                        if($b->rs < 0)
                            $total['wrong job'][] = $b->rs;
                        else
                            $total[strtolower($b->bonus_type)][] = $b->rs;
                        $tip[] = $b->tip;
                    @endphp
                    <tr>
                        <td>{{ $b->task->date->format('d/m/Y') }}</td>
                        <td>{{ $b->task->id }}</td>
                        <td>
                            
                            @if($b->rs < 0)
                                @php
                                $wrongJobReference = ' / ' . optional($b->task->childrens()->first())->id
                                @endphp
                            @else
                                @php $wrongJobReference = ''; @endphp
                            @endif
                            
                            {{ $b->task->name }} {{ $wrongJobReference }}
                            
                        </td>
                        <td>{{ $b->task->design_type }}</td>
                        <td>
                            @if($b->rs < 0)
                                WJ
                            @else
                                {{ $b->bonus_type }}
                            @endif
                        </td>
                        <td class="text-right">{{ number_format($b->rs, 2) }}</td>
                        <td class="text-right">{{ number_format($b->tip, 2) }}</td>
                    </tr>
                @endforeach

                <tr>
                    <th>Total</th>
                    <th></th>
                    <th></th>
                    <th></th>
                    <th></th>
                    <th>{{ $bonuses->sum('rs') }}</th>
                    <th>{{ $bonuses->sum('tip') }}</th>
                </tr>
                {{--{{ dd($total) }}--}}
            </table>

            <table class="table table-bordered table-striped">

                @foreach($total as $name => $bonus)

                    <tr>
                        <th>{{ $name }}</th>
                        @php $rs = $name === 'wrong job'? $user->bonus * -1 : $user->{$column[strtolower($name)]} @endphp
                        <td>{{ count($bonus) }} x {{ $rs }} = {{ count($bonus) * $rs }}</td>
                    </tr>

                @endforeach

            </table>
        </div>
    </div>
</section>
</body>
</html>

