@extends('layouts.master')


@section('styles')

<link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">

<link rel="stylesheet" href="{{ asset('assets/plugins/bootstrap/css/bootstrap.min.css') }}">

<link rel="stylesheet" href="{{ asset('assets/plugins/bootstrap-material-datetimepicker/css/bootstrap-material-datetimepicker.css') }}">

<link href="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.6-rc.0/css/select2.min.css" rel="stylesheet" />

<link href="{{ asset('css/customize-select2.css') }}" rel="stylesheet" />

@endsection

@section('content')

<div class="card">

    <div class="card-block">
    
        <form action="{{ route('bonus.pdf') }}" method="get" target="_blank">
        
            {{ inputField('From', 'from_date', old('from_date'), $errors->first('date'), [ 'class'=>'date']) }}

            {{ inputField('To', 'to_date', old('to_date'), $errors->first('date'), [ 'class'=>'date']) }}

            {{ selectField('Type', 'type', 
                ['All' => 'All', 'New' => 'New', 'Revision' => 'Revision', 'Edit' => 'Edit', 'Deduction' => 'Deduction'], 
                old('type'), $errors->first('type'), ['blank_first' => false]) }}
            
            {{ selectField('User', 'user_id', [], old('user_id'), $errors->first('user_id'), ['endpoint' => route('select.user') ]) }}

            <div class="form-group">

                <input type="submit" value="View PDF" class="btn btn-primary">

            </div>

        </form>

    </div>

</div>

@endsection


@section('footer_scripts')

<script src="{{ asset('assets/plugins/moment/moment.js') }}"></script>
<script src="{{ asset('assets/plugins/bootstrap-datepicker/dist/js/bootstrap-datepicker.min.js') }}"></script>
<script src="{{ asset('assets/plugins/bootstrap-datepicker/js/bootstrap-datetimepicker.min.js') }}"></script>

<script src="{{ asset('assets/plugins/datepicker/js/moment-with-locales.min.js') }}"></script>
<script src="{{ asset('assets/plugins/bootstrap-material-datetimepicker/js/bootstrap-material-datetimepicker.js') }}"></script>
<script src="{{ asset('assets/pages/elements.js') }}"></script>
<script src="{{ asset('assets/pages/elements-materialize.js') }}"></script>

@include('components.select2-scripts')

<script>

$(function () {

    $('.date').bootstrapMaterialDatePicker({
        time: false,
        clearButton: true,
        format: 'DD/MM/YYYY'
    });

})

</script>

@endsection