@extends('layouts.master')

@section('content')

<table class="table">
    
    <thead>
        <tr>
            <th>Name</th>
            <th>Order Type</th>
            <th>Order Count</th>
            <th>Tips</th>
        </tr>  
    </thead>
    <tbody>
    	@foreach($records as $record)
    	<tr>
    		<td>{{ $record->name }}</td>
    		<td>{{ $record->order_type }}</td>
    		<td>{{ $record->order_count }}</td>
    		<td>{{ $record->tip }}</td>
    	</tr>
    	@endforeach
    </tbody>

</table>

@endsection