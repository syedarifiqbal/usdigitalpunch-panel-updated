@extends('layouts.master')

@section('styles')
<style>
th {
    padding: 0 !important;
    white-space: nowrap;
}
</style>
@endsection

@section('content')

<div class="col-xl-12 col-lg-12">
    
    <div class="card">
        <div class="card-header">
            <h5 class="card-header-text">Task Details</h5>
            <div class="actions">
                <a href="http://localhost:8000/clients/create" class="modal-trigger waves-effect btn-flat nopadding">
                    <i class="mdi mdi-plus"></i>
                </a>
                <a href="#" class="table-search-toggle waves-effect btn-flat nopadding"></a>
            </div>

        </div>
        <div class="card-block">
            <div class="view-info">
                <div class="row">
                    <div class="col-lg-12">
                        <div class="general-info">
                            <div class="row">
                                <div class="col-lg-12 col-xl-6">
                                    <table class="table m-0">
                                        <tbody>
                                        <tr>
                                            <th scope="row">ORDER ID</th>
                                            <td>{{ $task->id }}</td>
                                        </tr>
                                        <tr>
                                            <th scope="row">Date</th>
                                            <td>{{ $task->date->format('d/m/Y') }}</td>
                                        </tr>
                                        <tr>
                                            <th scope="row">Customer Name</th>
                                            <td>{{ ucwords($task->client->nick_name) }}</td>
                                        </tr>
                                        <tr>
                                            <th scope="row">Design Name</th>
                                            <td>{{ $task->name }}</td>
                                        </tr>
                                        <tr>
                                            <th scope="row">PO Number</th>
                                            <td>{{ $task->po_number }}</td>
                                        </tr>
                                        <tr>
                                            <th scope="row">Status</th>
                                            <td>{{ $task->status }}</td>
                                        </tr>
                                        <tr>
                                            <th scope="row">Approved By</th>
                                            <td>{{ optional($task->checker)->name ?? '-' }}</td>
                                        </tr>
                                        <tr>
                                            <th scope="row">Additional Information</th>
                                            <?php $remarks = collect(explode("||", $task->remarks)); ?>
                                            <td><?php $remarks->each(function($line, $i){
                                                echo $i+1 . ") $line <br>";
                                           }) ?></td>
                                       </tr>
                                       </tbody>
                                   </table>
                               </div>
                               <!-- end of table col-lg-6 -->

                               <div class="col-lg-12 col-xl-6">
                                   <table class="table">
                                       <tbody>
                                       <tr>
                                           <th scope="row">Delivery Date</th>
                                           <td>{{ $task->delivery_date->format('d/m/Y') }}</td>
                                        </tr>
                                        <tr>
                                            <th scope="row">Priority</th>
                                            <td>{{ $task->priority }}</td>
                                        </tr>
                                        <tr>
                                            <th scope="row">Required Format</th>
                                            <td>{{ collect($task->required_format)->implode(', ') }}</td>
                                        </tr>
                                        <tr>
                                            <th scope="row">Width</th>
                                            <td>{{ $task->width }}</td>
                                        </tr>
                                        <tr>
                                            <th scope="row">Height</th>
                                            <td>{{ $task->height }}</td>
                                        </tr>
                                        <tr>
                                            <th scope="row">Unit</th>
                                            <td>{{ $task->unit }}</td>
                                        </tr>
                                        <tr>
                                            <th scope="row">Placement</th>
                                            <td>{{ $task->placement }}</td>
                                        </tr>
                                        <tr>
                                            <th scope="row">Customer Instruction</th>
                                            <td>{{ $task->client->instructions }}</td>
                                        </tr>
                                        </tbody>
                                    </table>
                                </div>
                                <!-- end of table col-lg-6 -->
                            </div>
                            <!-- end of row -->
                        </div>
                        <!-- end of general info -->
                    </div>
                    <!-- end of col-lg-12 -->
                </div>
                <!-- end of row -->
            </div>
            <!-- end of view-info -->
        </div>
        <!-- end of card-block -->
    </div>
    <!-- end of card -->

</div>

<div class="col-md-12 col-lg-6">
    
    <div class="card">
        <div class="card-header"><h5 class="card-header-text">Order Files</h5></div>
        <div class="card-block">
            <div class="technical-skill">
                
                @foreach($task->attachments()->where('for_delivery', 0)->get() as $file)

                    <a href="{{ Storage::url($file->path) }}" target="_blank">

                        <h6 title="{{ $file->filename }}">{{ $file->filename }}</h6>

                    </a>

                @endforeach
                @if($task->parent)
                    @foreach($task->parent->attachments()->where('for_delivery', 0)->get() as $file)

                        <a href="{{ Storage::disk('local')->url($file->path) }}">

                            <h6 title="{{ $file->filename }}">{{ $file->filename }}</h6>

                        </a>

                    @endforeach
                @endif
            </div>
            <!-- end of technical skill -->

        </div>
        <!-- end of card-block -->

    </div>
    <!-- end of card -->

</div>

<div class="col-md-12 col-lg-6">
    
    <div class="card">
        <div class="card-header"><h5 class="card-header-text">Delivered Files</h5></div>
        
        <div class="card-block">
        
            <div class="technical-skill">
                
                @foreach($task->attachments()->where('for_delivery', 1)->get() as $file)

                    <a href="{{ Storage::url($file->path) }}">

                        <h6 title="{{ $file->filename }}">{{ $file->filename }}</h6>

                    </a>

                @endforeach

            </div>
            <!-- end of technical skill -->

        </div>
        <!-- end of card-block -->

    </div>
    <!-- end of card -->

</div>


@endsection

@section('footer_scripts')

<!-- <script src="https://js.pusher.com/4.1/pusher.min.js"></script> -->

<!-- notifications -->

<script>

$(function () {

    // console.log('listening');
    
    // Pusher.logToConsole = true;

    // var pusher = new Pusher('279144f9545204ca4e09', {
    //   cluster: 'ap2',
    //   encrypted: true
    // });

    // var channel = pusher.subscribe('notification.1');
    // channel.bind('App\\Events\\TaskAssigned', function(data) {
    //     var html = '<li class="bell-notification">' +
    //             '<a href="' + data.url + '" class="media">' +
    //             '<span class="media-left media-icon">' +
    //             '<img class="img-circle" src="{{ asset('assets/images/avatar-1.png') }}" alt="User Image">' +
    //             '</span>' +
    //             '<div class="media-body"><span class="block">' + data.description + '</span><span class="text-muted block-time">2min ago</span></div>' +
    //         '</a>'+
    //     '</li>';
    //     $(html).insertAfter($("#notifications li:first"));
    // });

    // var channel = pusher.subscribe('private-App.User.1');
    // channel.bind('App\\Notifications\\TaskAssigned', function(data) {
    //     console.log(data);
    // });

});

</script>

@endsection