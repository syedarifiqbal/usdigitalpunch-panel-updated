@extends('layouts.master')

@section('styles')

<link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">

<link href="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.6-rc.0/css/select2.min.css" rel="stylesheet" />

<link href="{{ asset('css/customize-select2.css') }}" rel="stylesheet" />

@endsection

@section('content')


<form class="col" action="{{ route('tasks.store.assign', ['task' => $task]) }}" method="POST">

    @csrf
    
    <div class="card">

        <div class="card-header"><h5 class="card-header-text">Assign to Designer</h5></div>

        <div class="card-block">
                
            <?php

            if( $task->desinger ){
                $designer = $task->desinger->name;
            }
            elseif( old('designer') ){
                $designer = optional(\App\User::find(old('designer', 0)))->name;
            }
            else{
                $designer = '';
            }

            ?>

            {{ selectField('Designer', 'designer', [], old('designer', $task->designer), $errors->first('designer'), ['endpoint' => route('select.user'), 'select2Defaults' => $designer ]) }}

            
            <div class="form-group">
                <input type="submit" value="Assign" class="btn btn-primary">
            </div>

        </div>
        <!-- end of card-block -->
    </div>
    <!-- end of card -->

</form>

@endsection


@section('footer_scripts')

@include('components.select2-scripts')

<script src="{{ asset('assets/pages/elements.js') }}"></script>
<script src="{{ asset('js/contact-table-form.js') }}"></script>

@endsection