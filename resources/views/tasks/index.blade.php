@extends('layouts.master')

@section('styles')

<!-- <link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet"> -->
<link rel="stylesheet" href="http://cdn.materialdesignicons.com/2.4.85/css/materialdesignicons.min.css">

<link href="{{ asset('css/customize-datatable.css') }}" rel="stylesheet">

@endsection

@section('content')

<div id="admin" class="col s12">
    <div class="card material-table">
        <div class="table-header">
            <span class="table-title">Tasks List</span>
            <div class="actions">

                <input type="hidden" id="datatable-url" value="{{ route('dt.tasks', params([])) }}">

                <select name="" id="status">
                    <option value="{{ route('tasks', params(['status'=>''])) }}">All</option>
                    <option {{ request('status') === 'pending'? 'selected':'' }} value="{{ route('tasks', params(['status'=>'pending'])) }}">Pending</option>
                    <option {{ request('status') === 'complete'? 'selected':'' }} value="{{ route('tasks', params(['status'=>'complete'])) }}">Complete</option>
                    <option {{ request('status') === 'rfs'? 'selected':'' }} value="{{ route('tasks', params(['status'=>'rfs'])) }}">RFS</option>
                </select>
                
                <select name="" id="designType">
                    <option value="{{ route('tasks', params(['design_type'=>''])) }}">All</option>
                    <option {{ request('design_type') === 'digitizing'? 'selected':'' }} value="{{ route('tasks', params(['design_type'=>'digitizing'])) }}">Digitizing</option>
                    <option {{ request('design_type') === 'vector'? 'selected':'' }} value="{{ route('tasks', params(['design_type'=>'vector'])) }}">Vector</option>
                    <option {{ request('design_type') === 'quote'? 'selected':'' }} value="{{ route('tasks', params(['design_type'=>'quote'])) }}">Quote</option>
                </select>

                <select name="" id="unsignedTasks">
                    <option {{ request('unsigned_tasks', 'both') === 'both'? 'selected': '' }} value="{{ route('tasks', params(['unsigned_tasks'=>'both'])) }}">Assinged/Unsigned</option>
                    <option {{ request('unsigned_tasks') === 'false'? 'selected': '' }} value="{{ route('tasks', params(['unsigned_tasks'=>'false'])) }}">Assinged Tasks</option>
                    <option {{ request('unsigned_tasks') === 'true'? 'selected': '' }} value="{{ route('tasks', params(['unsigned_tasks'=>'true'])) }}">Unsigned Tasks</option>
                </select>

                <a href="{{ route('tasks.create') }}" class="modal-trigger waves-effect btn-flat nopadding"><i class="mdi mdi-plus"></i></a>
                <a href="#" class="table-search-toggle waves-effect btn-flat nopadding"><i class="mdi mdi-magnify"></i></a>
            </div>
        </div>
        <div class="table-responsive">
            <table id="datatable" data-page-length="@setting('table_page_length', 25)">
                <thead>
                    <tr>
                        <th>ID</th>
                        <th>Date</th>
                        <th>Design Name</th>
                        <th>Customer</th>
                        <th>Old Ref</th>
                        <th>Order Type</th>
                        <th>PO #</th>
                        <th>Priority</th>
                        <th>Designer</th>
                        <th>Status</th>
                        <th>Action</th>
                    </tr>
                </thead>
                <tbody>
                
                </tbody>
            </table>
        </div>
    </div>
</div>

<a id="fab" href="{{ route('tasks.create') }}" class="btn btn-icon waves-effect waves-light btn-primary"><i class="mdi mdi-plus"></i></a>

@endsection

@section('footer_scripts')

<script type="text/javascript" src="{{ asset('assets/pages/elements.js') }}"></script>

@include('components.datatable-scripts');

<script>

$('#designType, #unsignedTasks, #status').on('change', function(e){
    window.location = $(this).val();
});

</script>

@endsection