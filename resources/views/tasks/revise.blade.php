@extends('layouts.master')

@section('styles')

<link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">

<link href="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.6-rc.0/css/select2.min.css" rel="stylesheet" />

<link href="{{ asset('css/customize-select2.css') }}" rel="stylesheet" />

@endsection

@section('content')


<form class="col" action="{{ route('tasks.revise.store', ['task' => $task]) }}" method="POST" enctype="multipart/form-data">

    @csrf
    
    <div class="card">

        <div class="card-header"><h5 class="card-header-text">Revise Task</h5></div>

        <div class="card-block">

            {{ selectField('Rejection Level', 'rejection_level', ['Revision' => 'Revision', 'Edit' => 'Edit'], old('rejection_level'), $errors->first('rejection_level')) }}

            {{ textField('Remarks', 'remarks', old('remarks', $task->remarks), $errors->first('remarks')) }}

            <div class="row">
                <div class="col-md-6">
                    <div class="form-group">
                        <input type="submit" value="Revise Task" class="btn btn-primary">
                    </div>
                </div>
                <div class="col-md-6">
                    <div class="form-group">
                        <file-input-change></file-input-change>
                    </div>
                </div>
            </div>

        </div>
        <!-- end of card-block -->

    </div>

    <!-- end of card -->

</form>

@endsection


@section('footer_scripts')

@include('components.select2-scripts')

<script src="{{ asset('assets/pages/elements.js') }}"></script>
<script src="{{ asset('js/contact-table-form.js') }}"></script>

@endsection