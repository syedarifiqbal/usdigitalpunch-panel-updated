@extends('layouts.master')

@section('styles')
<style>
th {
    padding: 0 !important;
    white-space: nowrap;
}
</style>
@endsection

@section('content')



<div class="col-xl-12 col-lg-12">
    
    <div class="card">
        <div class="card-header">
            <h5 class="card-header-text">Task Details</h5>
            <div class="actions">
                <a href="http://localhost:8000/clients/create" class="modal-trigger waves-effect btn-flat nopadding">
                    <i class="mdi mdi-plus"></i>
                </a>
                <a href="#" class="table-search-toggle waves-effect btn-flat nopadding">
                    asdf
                </a>
            </div>

        </div>
        <div class="card-block">
            <div class="view-info">
                <div class="row">
                    <div class="col-lg-12">
                        <div class="general-info">
                            <div class="row">
                                <div class="col-lg-12 col-xl-6">
                                    <table class="table m-0">
                                        <tbody>
                                        <tr>
                                            <th scope="row">ORDER ID</th>
                                            <td>{{ $task->id }}</td>
                                        </tr>
                                        <tr>
                                            <th scope="row">Date</th>
                                            <td>{{ $task->date->format('d/m/Y') }}</td>
                                        </tr>
                                        <tr>
                                            <th scope="row">Customer Name</th>
                                            <td>{{ ucwords($task->client->nick_name) }}</td>
                                        </tr>
                                        <tr>
                                            <th scope="row">Design Name</th>
                                            <td>{{ $task->name }}</td>
                                        </tr>
                                        <tr>
                                            <th scope="row">PO Number</th>
                                            <td>{{ $task->po_number }}</td>
                                        </tr>
                                        <tr>
                                            <th scope="row">Status</th>
                                            <td>{{ $task->status }}</td>
                                        </tr>
                                        <tr>
                                            <th scope="row">Approved By</th>
                                            <td>{{ optional($task->checker)->name ?? '-' }}</td>
                                        </tr>
                                        <tr>
                                            <th scope="row">Additional Information</th>
                                            <?php $remarks = collect(explode("\n\r", $task->remarks)); ?>
                                            <td><?php $remarks->each(function($line, $i){
                                                echo $i+1 . ") $line <br>";
                                           }) ?></td>
                                       </tr>
                                       </tbody>
                                   </table>
                               </div>
                               <!-- end of table col-lg-6 -->

                               <div class="col-lg-12 col-xl-6">
                                   <table class="table">
                                       <tbody>
                                       <tr>
                                           <th scope="row">Delivery Date</th>
                                           <td>{{ $task->delivery_date->format('d/m/Y') }}</td>
                                        </tr>
                                        <tr>
                                            <th scope="row">Priority</th>
                                            <td>{{ $task->priority }}</td>
                                        </tr>
                                        <tr>
                                            <th scope="row">Required Format</th>
                                            <td>{{ collect($task->required_format)->implode(', ') }}</td>
                                        </tr>
                                        <tr>
                                            <th scope="row">Width</th>
                                            <td>{{ $task->width }}</td>
                                        </tr>
                                        <tr>
                                            <th scope="row">Height</th>
                                            <td>{{ $task->height }}</td>
                                        </tr>
                                        <tr>
                                            <th scope="row">Placement</th>
                                            <td>{{ $task->placement }}</td>
                                        </tr>
                                        <tr>
                                            <th scope="row">Customer Instruction</th>
                                            <td>{{ $task->client->instructions }}</td>
                                        </tr>
                                        </tbody>
                                    </table>
                                </div>
                                <!-- end of table col-lg-6 -->
                            </div>
                            <!-- end of row -->
                        </div>
                        <!-- end of general info -->
                    </div>
                    <!-- end of col-lg-12 -->
                </div>
                <!-- end of row -->
            </div>
            <!-- end of view-info -->
        </div>
        <!-- end of card-block -->
    </div>
    <!-- end of card -->

</div>

<form action="{{ route('tasks.update', ['task' => $record->id, 'submit' => 'submit']) }}" method="post" enctype="multipart/form-data">

    @csrf()
    @method('put')

    <div class="col-md-12 col-lg-6">
        
        <div class="card">
            <div class="card-header"><h5 class="card-header-text">Delivered Files</h5></div>
            
            <div class="card-block">
            
                {{ inputField('Width', 'delivery_width', old('delivery_width', $record->delivery_width), $errors->first('delivery_width')) }}

                {{ inputField('Height', 'delivery_height', old('delivery_height', $record->delivery_height), $errors->first('delivery_height')) }}

                {{ inputField('No stitches', 'stitches', old('stitches', $record->stitches), $errors->first('stitches')) }}

                {{ textField('Remarks:', 'delivery_remarks', old('delivery_remarks', $record->delivery_remarks), $errors->first('delivery_remarks')) }}
                
                <div class="form-group">
                    <input type="submit" value="Save" class="btn btn-primary">
                </div>

            </div>
            <!-- end of card-block -->

        </div>
        <!-- end of card -->

    </div>

    <div class="col-md-12 col-lg-6">
        
        <div class="card">

            <div class="card-header"><h5 class="card-header-text">Order Files</h5></div>

            <div class="card-block">

                <file-input-change></file-input-change>

                @if( $errors->has('uploads') )
                    <p>{{ $errors->first('uploads') }}</p>
                @endif

                @foreach($record->attachments()->where('for_delivery', 1)->get() as $file)
                    @if(in_array($file->extension, ['jpeg', 'jpg', 'png', 'gif', 'tif']))
                        <img src="{{ Storage::disk('s3')->url($file->path) }}" alt="{{ $file->name }}" class="img-fluid">
                    @else
                        <a href="{{ Storage::disk('s3')->url($file->path) }}">
                            <h6 title="{{ $file->filename }}">{{ $file->filename }}</h6>
                        </a>
                    @endif
                @endforeach

            </div>
            <!-- end of card-block -->

        </div>
        <!-- end of card -->

    </div>

</form>

@endsection

@section('footer_scripts')

<script src="{{ asset('assets/pages/elements.js') }}"></script>

<script>

$(function () {



});

</script>

@endsection