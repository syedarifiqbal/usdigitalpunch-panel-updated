@extends('layouts.master')

@section('styles')
<link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">
<link rel="stylesheet" href="{{ asset('assets/plugins/bootstrap/css/bootstrap.min.css') }}">
<link rel="stylesheet" href="{{ asset('assets/plugins/bootstrap-material-datetimepicker/css/bootstrap-material-datetimepicker.css') }}">
<link href="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.6-rc.0/css/select2.min.css" rel="stylesheet" />
<link href="{{ asset('css/customize-select2.css') }}" rel="stylesheet" />
@endsection

@section('content')
<form  method="POST" action="{{ $record->id? route('tasks.update', ['task' => $record->id]): route('tasks.store') }}" class="card-block" enctype="multipart/form-data">
    @csrf
    @if($record->id) @method('put') @endif
    <div class="row">
        <!-- .col-md-6 -->
        <div class="col-md-6">
            <div class="card">
                <div class="card-header">
                    <h5 class="card-header-text">Add new Task</h5>
                </div>
                <div class="card-block">
                    {{ inputField('Date', 'date', old('date', $record->date?$record->date->format('d/m/Y'):''), $errors->first('date'), [ 'class'=>'date']) }}
                    {{ inputField('Design Name', 'name', old('name', $record->name), $errors->first('name'), [ 'class'=>'name']) }}
                    <?php if( old('client_id') ){
                        $client = optional(App\Models\Client::find(old('client_id')))->name;
                    } elseif( $record->client ){
                        $client = $record->client->name;
                    } else { $client = ''; } ?>
                    {{ selectField('Customer', 'client_id', [], old('client_id', $record->client_id), $errors->first('client_id'), ['endpoint' => route('select.client'), 'select2Defaults' => $client ]) }}
                    {{ selectField('Design Type', 'design_type', ['Digitizing' => 'Digitizing', 'Vector' => 'Vector', 'Quotation' => 'Quotation'], old('design_type', $record->design_type), $errors->first('design_type')) }}

                    @if( $record->status == 'Complete' )
                    <input type="hidden" value="{{ $record->order_type }}" name="order_type"/>
                    @else
                    {{ selectField('Order Type', 'order_type', ['New' => 'New', 'Revision' => 'Revision', 'Edit' => 'Edit'], old('order_type', $record->order_type), $errors->first('order_type')) }}
                    @endif
                    
                    {{ selectField('Priority', 'priority', ['Normal' => 'Normal', 'Urgent' => 'Urgent', 'Most Urgent' => 'Most Urgent'], old('priority', $record->priority), $errors->first('priority'), ['blank_first' => false]) }}
                    {{ inputField('Price', 'price', old('price', $record->price), $errors->first('price'), [ 'class'=>'price']) }}
                    {{ textField('Remarks', 'remarks', old('remarks', $record->remarks), $errors->first('remarks')) }}
                    <div class="form-group">
                        <input type="submit" value="Save" class="btn btn-primary">
                    </div>
                </div>
            </div>
            <!-- .card -->
        </div>
        <!-- .col-md-6 -->
        <!-- .col-md-6 -->
        <div class="col-md-6">
            <div class="card">
                <div class="card-header">
                    <h5 class="card-header-text">Task Information</h5>
                </div>
                <div class="card-block">
                    {{ inputField('Delivery Date', 'delivery_date', old('delivery_date', $record->delivery_date?$record->delivery_date->format('d/m/Y'):''), $errors->first('delivery_date'), [ 'class'=>'date']) }}
                    {{ inputField('Width', 'width', old('width', $record->width), $errors->first('width')) }}
                    {{ inputField('Height', 'height', old('height', $record->height), $errors->first('height')) }}
                    {{ inputField('PO Number', 'po_number', old('po_number', $record->po_number), $errors->first('po_number')) }}
                    {{ selectField('Placement', 'placement', $placements, old('placement', $record->placement), $errors->first('placement')) }}
                    {{ selectField('Unit', 'unit', ['in' => 'Inch', 'cm'=>'Centimetre', 'mm' => 'Millimeter'], old('unit', $record->unit), $errors->first('unit')) }}
                    <div>
                        <div class="md-input-wrapper">
                            <label>Required Format</label>
                            <br><br>
                        </div>
                        @foreach($allowedFormats as $format)
                        {{ checkboxField( $format, 'required_format[]', $format, old('required_format', $record->required_format), $errors->first('required_format'), ['display_errors' => false]) }}
                        @endforeach
                    </div>
                    @if( $errors->has('required_format') )
                        <p>{{ $errors->first('required_format') }}</p>
                    @endif
                    {{--<div class="md-group-add-on">--}}
                        {{--<span class="md-add-on-file">--}}
                            {{--<button class="btn btn-success waves-effect waves-light">File</button>--}}
                        {{--</span>--}}
                        {{--<div class="md-input-file">--}}
                            {{--<input type="file" class="" name="uploads[]" multiple/>--}}
                            {{--<input type="text" class="md-form-control md-form-file">--}}
                            {{--<label class="md-label-file">Upload File</label>--}}
                        {{--</div>--}}
                    {{--</div>--}}
                    <file-input-change></file-input-change>
                    @if( $errors->has('uploads') )
                        <p>{{ $errors->first('uploads') }}</p>
                    @endif
                </div>
                <!-- .card-block -->
            </div>
            <!-- .card -->
        </div>
        <!-- .col-md-6 -->
    </div>
    <!-- .row -->
</form>
@endsection
@section('footer_scripts')
<script src="{{ asset('assets/plugins/moment/moment.js') }}"></script>
<script src="{{ asset('assets/plugins/bootstrap-datepicker/dist/js/bootstrap-datepicker.min.js') }}"></script>
<script src="{{ asset('assets/plugins/bootstrap-datepicker/js/bootstrap-datetimepicker.min.js') }}"></script>

<script src="{{ asset('assets/plugins/datepicker/js/moment-with-locales.min.js') }}"></script>
<script src="{{ asset('assets/plugins/bootstrap-material-datetimepicker/js/bootstrap-material-datetimepicker.js') }}"></script>
<script src="{{ asset('assets/pages/elements.js') }}"></script>
<script src="{{ asset('assets/pages/elements-materialize.js') }}"></script>

@include('components.select2-scripts')

<script>
$(function () {
    $('.date').bootstrapMaterialDatePicker({
        time: false,
        clearButton: true,
        format: 'DD/MM/YYYY'
    });
})
</script>
@endsection