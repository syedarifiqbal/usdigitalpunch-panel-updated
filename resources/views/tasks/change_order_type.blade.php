@extends('layouts.master')

@section('content')


<form class="col" action="{{ route('tasks.changeOrderType.store', ['task' => $task->id]) }}" method="POST">

    @csrf
    
    <div class="card">
        <div class="card-header"><h5 class="card-header-text">Change Task Type</h5></div>

        <div class="card-block">
            <?php
            $types = ['Revision' => 'Revision', 'Edit' => 'Edit', 'New' => 'New'];
                unset($types[$task->order_type]);
            ?>
            {{ selectField('Order Type', 'order_type', $types, old('rejection_level'), $errors->first('rejection_level')) }}

            <div class="form-group">

                <input type="submit" value="Change" class="btn btn-primary">

            </div>

        </div>
        <!-- end of card-block -->

    </div>

    <!-- end of card -->

</form>

@endsection


@section('footer_scripts')

@include('components.select2-scripts')

<script src="{{ asset('assets/pages/elements.js') }}"></script>
<script src="{{ asset('js/contact-table-form.js') }}"></script>

@endsection