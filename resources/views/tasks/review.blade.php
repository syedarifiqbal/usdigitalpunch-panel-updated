@extends('layouts.master')

@section('styles')

    <style>
        .btn.btn-no-ui {
            background: none;
            box-shadow: none;
            padding: 0;
        }
    </style>

<link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">

<link href="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.6-rc.0/css/select2.min.css" rel="stylesheet" />

<link href="{{ asset('css/customize-select2.css') }}" rel="stylesheet" />

@endsection

@section('content')


<form class="col" action="{{ route('tasks.review.store', ['task' => $task->id]) }}" method="POST">

    @csrf
    
    <div class="row">

        <div class="col-md-4">
            <div class="card">
                <div class="card-header">
                    <h5 class="card-header-text">Send to Client</h5>
                </div>
                <div class="card-block">
                    <?php $currencies = ['USD' => 'Dollar ($)', 'GBP' => 'Pound (£)', 'EUR' => 'Euro (€)', 'CA' => 'CA ($)', 'NZD' => 'NZD ($)']; ?>
                    {{ selectField('Currency', 'currency', $currencies, old('currency'), $errors->first('currency'), ['first_blank' => false]) }}
                    {{ inputField('Price', 'price', old('price', $task->price), $errors->first('price')) }}
                    @if( strtolower($task->status) != 'complete' )
                    {{ inputField('Additional Bonus', 'additional_bonus', old('additional_bonus', $task->additional_bonus), $errors->first('additional_bonus')) }}
                    {{ selectField('Approved by', 'approved_by', $checkers, old('approved_by', $task->approved_by), $errors->first('approved_by')) }}
                    @endif
                    @foreach($task->attachments()->where('for_delivery', 1)->get() as $file)
                    <div class="form-check ">
                        {{--<a href="{{ Storage::disk('s3')->url($file->path) }}" target="_blank">--}}
                        <a href="{{ asset('storage/' . $file->path ) }}" target="_blank">
                                <i class="icofont icofont-eye"></i>
                        </a>
                        <input class="form-check-input" name="files[]" type="checkbox" value="{{ $file->id }}" id="{{ $file->id }}_file" style="margin-left: 0;" checked />
                        <label class="form-check-label" for="{{ $file->id }}_file">{{ $file->filename }}</label>
                        <button type="button" class="btn delete-file btn-no-ui btn-sm float-right" data-file="{{ $file->id }}"><i class="icofont icofont-trash text-danger"></i></button>
                    </div>
                    @endforeach
                    @if( $errors->has('files') )
                        <p>{{ $errors->first('files') }}</p>
                    @endif

                    @if( strtolower($task->status) == 'complete' )
                    <div class="form-group">
                        <input type="submit" value="Resend to Customer" class="btn btn-primary">
                    </div>
                    @else
                    <div class="form-group">
                        <input type="submit" value="Send and Complete" class="btn btn-primary">
                    </div>
                    @endif
                        <a href="{{ route('tasks.submit', ['task' => $task->id, 'submit' => 'submit']) }}" class="btn btn-secondary">Upload more files</a>
                </div>
                <!-- end of card-block -->
            </div>
            @if(!empty($task->client->contacts))
            <!-- end of card -->
            <div class="card">
                <div class="card-header">
                    <h5 class="card-header-text">Client Contacts List</h5>
                </div>
                <div class="card-block">
                    <ul class="list-group">
                        @foreach($task->client->contacts as $contact)
                            <li class="list-group-item">&lt;{{ $contact->name }}&gt; {{ $contact->email }}</li>
                        @endforeach
                    </ul>
                </div>
                <!-- end of card-block -->
            </div>
            <!-- end of card -->
            @endif
            <div class="card">
                <div class="card-header">
                    <h5 class="card-header-text">Digitizer Details</h5>
                </div>
                <div class="card-block">
                    <ul class="list-group">
                        <li class="list-group-item">Width: {{ $task->delivery_width }}</li>
                        <li class="list-group-item">Height: {{ $task->delivery_height }}</li>
                        <li class="list-group-item">Stitches: {{ $task->stitches }}</li>
                        <li class="list-group-item">Digitizer Remarks: {{ $task->delivery_remarks }}</li>
                    </ul>
                </div>
                <!-- end of card-block -->
            </div>
            <!-- end of card -->

        </div>
        <!-- .col -->

        <div class="col-md-8">
            <div class="card">
                <div class="card-header"><h5 class="card-header-text">Task Details</h5></div>
                <div class="card-block">
                    <div class="view-info">
                        <div class="row">
                            <div class="col-lg-12">
                                <div class="general-info">
                                    <div class="row">
                                        <div class="col-lg-12 col-xl-6">
                                            <table class="table m-0">
                                                <tbody>
                                                <tr>
                                                    <th scope="row">Assigned User</th>
                                                    <td>{{ optional($task->designer)->name }}</td>
                                                </tr>
                                                <tr>
                                                    <th scope="row">ORDER ID</th>
                                                    <td>{{ $task->id }}</td>
                                                </tr>
                                                <tr>
                                                    <th scope="row">Date</th>
                                                    <td>{{ $task->date->format('d/m/Y') }}</td>
                                                </tr>
                                                <tr>
                                                    <th scope="row">Customer Name</th>
                                                    <td>{{ ucwords($task->client->nick_name) }}</td>
                                                </tr>
                                                <tr>
                                                    <th scope="row">Design Name</th>
                                                    <td>{{ $task->name }}</td>
                                                </tr>
                                                <tr>
                                                    <th scope="row">PO Number</th>
                                                    <td>{{ $task->po_number }}</td>
                                                </tr>
                                                <tr>
                                                    <th scope="row">Status</th>
                                                    <td>{{ $task->status }}</td>
                                                </tr>
                                                <tr>
                                                    <th scope="row">Approved By</th>
                                                    <td>{{ optional($task->checker)->name }}</td>
                                                </tr>
                                                <tr>
                                                    <th scope="row">Additional Information</th>
                                                    <td>{{ $task->remarks }}</td>
                                                </tr>
                                                </tbody>
                                            </table>
                                        </div>
                                        <!-- end of table col-lg-6 -->

                                        <div class="col-lg-12 col-xl-6">
                                            <table class="table">
                                                <tbody>
                                                <tr>
                                                    <th scope="row">Customer Email</th>
                                                    <td>{{ $task->client->email }}</td>
                                                </tr>
                                                <tr>
                                                    <th scope="row">Delivery Date</th>
                                                    <td>{{ $task->delivery_date->format('d/m/Y') }}</td>
                                                </tr>
                                                <tr>
                                                    <th scope="row">Priority</th>
                                                    <td>{{ $task->priority }}</td>
                                                </tr>
                                                <tr>
                                                    <th scope="row">Required Format</th>
                                                    <td>{{ collect($task->required_format)->implode(', ') }}</td>
                                                </tr>
                                                <tr>
                                                    <th scope="row">Width</th>
                                                    <td>{{ $task->width }}</td>
                                                </tr>
                                                <tr>
                                                    <th scope="row">Height</th>
                                                    <td>{{ $task->height }}</td>
                                                </tr>
                                                <tr>
                                                    <th scope="row">Unit</th>
                                                    <td>{{ $task->unit }}</td>
                                                </tr>
                                                <tr>
                                                    <th scope="row">Placement</th>
                                                    <td>{{ $task->placement }}</td>
                                                </tr>
                                                <tr>
                                                    <th scope="row">Customer Instruction</th>
                                                    {{--$task->load('client')--}}
                                                    <td>{!! nl2br($task->client->instructions) !!}</td>
                                                </tr>
                                                </tbody>
                                            </table>
                                        </div>
                                        <!-- end of table col-lg-6 -->
                                    </div>
                                    <!-- end of row -->
                                    <div class="row">
                                        <div class="col-lg-12">
                                            <table class="table">
                                                <tr>
                                                    <th scope="row">Customer Secret Instruction</th>
                                                    <td>{!! nl2br($task->client->secret_instructions) !!}</td>
                                                </tr>
                                            </table>
                                        </div>
                                    </div>
                                </div>
                                <!-- end of general info -->
                            </div>
                            <!-- end of col-lg-12 -->
                        </div>
                        <!-- end of row -->
                    </div>
                    <!-- end of view-info -->
                </div>
                <!-- end of card-block -->
            </div>
            <!-- end of card -->
        </div>
    </div>
</form>

@endsection

@section('footer_scripts')

@include('components.select2-scripts')

<script>
    {{--{{ route('tasks.file.delete', [$task->id, $file->id]) }}--}}
    $('.delete-file').on('click', function(){
        $this = $(this);
        if(!confirm('do you want to delete this file?'))
            return;
        var fileId = $this.data('file');
        $.ajax({
            method: 'delete',
            url: "/tasks/{{ $task->id }}/"+fileId+"/delete",
            data: { '_token' : token = document.head.querySelector('meta[name="csrf-token"]').content },
            success: function(data)
            {
                if(data.status)
                    $this.parent().fadeOut(function(){
                        $this.remove();
                    });
            }
        })
    });
</script>

<script src="{{ asset('assets/pages/elements.js') }}"></script>
<script src="{{ asset('js/contact-table-form.js') }}"></script>

@endsection