@extends('layouts.master')

@section('styles')

    <!-- <link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet"> -->
    <link rel="stylesheet" href="http://cdn.materialdesignicons.com/2.4.85/css/materialdesignicons.min.css">

    {{--<link rel="stylesheet" href="{{ asset('assets/plugins/bootstrap/css/bootstrap.min.css') }}">--}}
    <link rel="stylesheet" href="{{ asset('assets/plugins/bootstrap-material-datetimepicker/css/bootstrap-material-datetimepicker.css') }}">

@endsection

@section('content')

<!-- Header end -->
<div class="row">
    <div class="col-xl-3 col-lg-4">
        <div class="card faq-left">
            <div class="social-profile">
                <img class="img-fluid" src="{{ asset('assets/images/social/profile.jpg') }}" alt="">
                <div class="profile-hvr m-t-15">
                    <i class="icofont icofont-ui-edit p-r-10 c-pointer"></i>
                    <i class="icofont icofont-ui-delete c-pointer"></i>
                </div>
            </div>
            <div class="card-block">
                <h4 class="f-18 f-normal m-b-10 txt-primary">{{ auth()->user()->name }}</h4>
                <h5 class="f-14">{{ auth()->user()->designation }}</h5>
                <p class="m-b-15">{{ auth()->user()->bio }}</p>
                <ul>
                    <li class="faq-contact-card">
                        <i class="icofont icofont-telephone"></i>
                        {{ auth()->user()->cell }}
                    </li>
                    <!-- <li class="faq-contact-card">
                        <i class="icofont icofont-world"></i>
                        <a href="http://phoenixcoded.com">www.phoenixcoded.com</a>
                    </li> -->
                    <li class="faq-contact-card">
                        <i class="icofont icofont-email"></i>
                        <a href="mailto:{{ auth()->user()->email }}">{{ auth()->user()->email }}</a>
                    </li>
                </ul>
                <div class="faq-profile-btn">
                    <button type="button" class="btn btn-primary waves-effect waves-light">Follows
                    </button>
                    <button type="button" class="btn btn-success waves-effect waves-light">Message
                    </button>
                </div>

            </div>
        </div>
        <!-- end of card-block -->
        
        <div class="card">
            <div class="card-header"><h5 class="card-header-text">Total Tasks</h5></div>
            <div class="card-block">
                <div class="technical-skill">
                    <h6>Digitizing</h6>
                    <div class="faq-progress">
                        <div class="progress">
                            <span class="faq-text1"></span>
                            <div class="faq-bar1"></div>
                        </div>
                    </div>
                    <h6>Vector</h6>
                    <div class="faq-progress">
                        <div class="progress">
                            <span class="faq-text2"></span>
                            <div class="faq-bar2"></div>
                        </div>
                    </div>
                    <h6>Quote</h6>
                    <div class="faq-progress">
                        <div class="progress">
                            <span class="faq-text3"></span>
                            <div class="faq-bar3"></div>
                        </div>
                    </div>
                </div>
                <!-- end of technical skill -->
            </div>
            <!-- end of card-block -->
        </div>
        <!-- end of card -->

    </div>
    <!-- end of col-lg-3 -->

    <!-- start col-lg-9 -->
    <div class="col-xl-9 col-lg-8">
        <!-- Nav tabs -->
        <div class="tab-header">
            <ul class="nav nav-tabs md-tabs tab-timeline" role="tablist">
                <li class="nav-item">
                    <a class="nav-link {{ !request('from')? 'active': '' }}" data-toggle="tab" href="#personal" role="tab">Personal Info</a>
                    <div class="slide"></div>
                </li>
                <li class="nav-item">
                    <a class="nav-link" data-toggle="tab" href="#project" role="tab">Tasks</a>
                    <div class="slide"></div>
                </li>
                <li class="nav-item">
                    <a class="nav-link {{ request('from')? 'active': '' }}" data-toggle="tab" href="#bonus" role="tab">Bonus</a>
                    <div class="slide"></div>
                </li>
                <li class="nav-item">
                    <a class="nav-link" data-toggle="tab" href="#client" role="tab">My Client</a>
                    <div class="slide"></div>
                </li>
            </ul>
        </div>
        <!-- end of tab-header -->

        <div class="tab-content">
            <div class="tab-pane {{ !request('from')? 'active': '' }}" id="personal" role="tabpanel">
                <div class="card">
                    <div class="card-header"><h5 class="card-header-text">About Me</h5>
                        <button id="edit-btn" type="button" class="btn btn-primary waves-effect waves-light f-right" >
                            <i  class="icofont icofont-edit"></i>
                        </button>
                    </div>
                    <div class="card-block">
                        <div class="view-info">
                            <div class="row">
                                <div class="col-lg-12">
                                    <div class="general-info">
                                        <div class="row">
                                            <div class="col-lg-12 col-xl-6">
                                                <table class="table m-0">
                                                    <tbody>
                                                    <tr>
                                                        <th scope="row">Full Name</th>
                                                        <td>{{ auth()->user()->name }}</td>
                                                    </tr>
                                                    <tr>
                                                        <th scope="row">Gender</th>
                                                        <td>{{ ucwords(auth()->user()->gender) }}</td>
                                                    </tr>
                                                    <tr>
                                                        <th scope="row">Birth Date</th>
                                                        <!-- <td>October 25th, 1990</td> -->
                                                        <td>{{ auth()->user()->dob->format('F dS, Y') }}</td>
                                                    </tr>
                                                    <tr>
                                                        <th scope="row">Marital Status</th>
                                                        <td>{{ auth()->user()->marital_status }}</td>
                                                    </tr>
                                                    <tr>
                                                        <th scope="row">Location</th>
                                                        <td>{{ auth()->user()->address }}</td>
                                                    </tr>
                                                    </tbody>
                                                </table>
                                            </div>
                                            <!-- end of table col-lg-6 -->

                                            <div class="col-lg-12 col-xl-6">
                                                <table class="table">
                                                    <tbody>
                                                    <tr>
                                                        <th scope="row">Email</th>
                                                        <td><a href="#!">{{ auth()->user()->email }}</a></td>
                                                    </tr>
                                                    <tr>
                                                        <th scope="row">Mobile Number</th>
                                                        <td>{{ auth()->user()->cell }}</td>
                                                    </tr>
                                                    <tr>
                                                        <th scope="row">Twitter</th>
                                                        <td>{{ auth()->user()->twitter }}</td>
                                                    </tr>
                                                    <tr>
                                                        <th scope="row">Skype</th>
                                                        <td>{{ auth()->user()->skype }}</td>
                                                    </tr>
                                                    <tr>
                                                        <th scope="row">Website</th>
                                                        <td><a href="#!">www.demo.com</a></td>
                                                    </tr>
                                                    </tbody>
                                                </table>
                                            </div>
                                            <!-- end of table col-lg-6 -->
                                        </div>
                                        <!-- end of row -->
                                    </div>
                                    <!-- end of general info -->
                                </div>
                                <!-- end of col-lg-12 -->
                            </div>
                            <!-- end of row -->
                        </div>
                        <!-- end of view-info -->

                        <div class="edit-info">
                            <div class="row">
                                <div class="col-lg-12">
                                    <div class="general-info">
                                        <div class="row">
                                            <div class="col-lg-6">

                                                @include('users.profile-edit')
                                                
                                            </div>
                                            <!-- end of table col-lg-6 -->

                                            <div class="col-lg-6">
                                                <table class="table">
                                                    <tbody>
                                                    <tr>
                                                        <td>
                                                            <div class="md-group-add-on">
                                                                <span class="md-add-on">
                                                                    <i class="icofont icofont-email"></i>
                                                                </span>
                                                                <div class="md-input-wrapper">
                                                                    <input type="email" class="md-form-control">
                                                                    <label>Email</label>
                                                                </div>
                                                            </div>
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td>
                                                            <div class="md-group-add-on">
                                                                <span class="md-add-on">
                                                                    <i class="icofont icofont-mobile-phone"></i>
                                                                </span>
                                                                <div class="md-input-wrapper">
                                                                    <input type="number" class="md-form-control">
                                                                    <label>Mobile Number</label>
                                                                </div>
                                                            </div>
                                                        </td>

                                                    </tr>
                                                    <tr>
                                                        <td>
                                                            <div class="md-group-add-on">
                                                                <span class="md-add-on">
                                                                    <i class="icofont icofont-social-twitter"></i>
                                                                </span>
                                                                <div class="md-input-wrapper">
                                                                    <input type="email" class="md-form-control">
                                                                    <label>Twitter Id</label>
                                                                </div>
                                                            </div>
                                                        </td>

                                                    </tr>
                                                    <tr>
                                                        <td>
                                                            <div class="md-group-add-on">
                                                                <span class="md-add-on">
                                                                    <i class="icofont icofont-social-skype"></i>
                                                                </span>
                                                                <div class="md-input-wrapper">
                                                                    <input type="email" class="md-form-control">
                                                                    <label>Skype Id</label>
                                                                </div>
                                                            </div>
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td>
                                                            <div class="md-group-add-on">
                                                                <span class="md-add-on">
                                                                    <i class="icofont icofont-web"></i>
                                                                </span>
                                                                <div class="md-input-wrapper">
                                                                    <input type="text" class="md-form-control">
                                                                    <label>Website</label>
                                                                </div>
                                                            </div>
                                                        </td>
                                                    </tr>
                                                    </tbody>
                                                </table>

                                            </div>

                                            <!-- end of table col-lg-6 -->
                                        </div>
                                        <!-- end of row -->
                                        <div class="text-center">
                                            <a href="#!" class="btn btn-primary waves-effect waves-light m-r-20">Save</a>
                                            <a href="#!" id="edit-cancel" class="btn btn-default waves-effect">Cancel</a>
                                        </div>
                                    </div>
                                    <!-- end of edit info -->
                                </div>
                                <!-- end of col-lg-12 -->
                            </div>
                            <!-- end of row -->

                        </div>
                        <!-- end of view-info -->
                    </div>
                    <!-- end of card-block -->
                </div>
                <!-- end of card-->

            </div>
            <!-- end of tab-pane -->
            <!-- end of about us tab-pane -->

            <!-- start tab-pane of project tab -->
            @include('users.tasks_tab')
            <!-- end of project pane -->

            <!-- start a question pane  -->

            <div class="tab-pane {{ request('from')? 'active': '' }}" id="bonus" role="tabpanel">
                @if(auth()->user()->sale_wrap_commision == 0)
                    @include('users.bonus_tab')
                @else
                    @include('users.sales_bonus_tab')
                @endif
            </div>
            <!-- end of tab pane question -->

            <!-- start memeber ship tab pane -->

            <div class="tab-pane" id="client" role="tabpanel">
                @include('users.client_tab')
            </div>
            <!-- end of memebership tab pane -->

        </div>
        <!-- end of main tab content -->
    </div>
</div>

@endsection


@section('footer_scripts')

<script src="{{ asset('assets/plugins/moment/moment.js') }}"></script>
<script src="{{ asset('assets/plugins/bootstrap-material-datetimepicker/js/bootstrap-material-datetimepicker.js') }}"></script>
<script src="{{ asset('assets/pages/counter.js') }}"></script>
<script src="{{ asset('assets/pages/elements.js') }}"></script>
<script src="{{ asset('assets/pages/profile.js') }}"></script>


    {{--<script src="{{ asset('assets/plugins/bootstrap-datepicker/dist/js/bootstrap-datepicker.min.js') }}"></script>--}}
    {{--<script src="{{ asset('assets/plugins/bootstrap-datepicker/js/bootstrap-datetimepicker.min.js') }}"></script>--}}

    {{--<script src="{{ asset('assets/plugins/datepicker/js/moment-with-locales.min.js') }}"></script>--}}
    <script src="{{ asset('assets/plugins/bootstrap-material-datetimepicker/js/bootstrap-material-datetimepicker.js') }}"></script>

    <script>
        $(function () {

            $('.date').bootstrapMaterialDatePicker({
                time: false,
                clearButton: true,
                format: 'YYYY-MM-DD'
            });

            $('#filterForm').on('submit', function(e){
                e.preventDefault();
                var from_date = $('#from_date').val();
                var end_date = $('#end_date').val();
                var url = '{{ route('profile') }}';
                if(!$.trim(from_date) || !end_date.trim())
                {
                    alert('Please choose from and end date');
                    return;
                }
                window.location = url + '/' + from_date + '/' + end_date;
            })
        });
    </script>

@endsection