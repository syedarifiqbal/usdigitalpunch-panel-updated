<div class="tab-pane" id="project" role="tabpanel">
    <div class="card">
        <div class="card-header">
            <div class="row">
                <div class="col-md-4">
                    <h5 class="card-header-text">Your Bonus</h5>
                </div>
                <div class="col-md-8">
                    <form class="form-inline" method="post" id="filterForm">
                        <div class="form-group">
                            <label class="sr-only form-control-label" for="from_date">From Date</label>
                            <input type="text" class="form-control date" id="from_date" placeholder="From Date" readonly>
                        </div>
                        <div class="form-group">
                            <label class="sr-only form-control-label" for="end_date">End Date</label>
                            <input type="text" class="form-control date" id="end_date" placeholder="End Date" readonly>
                        </div>
                        <button type="submit" class="btn btn-primary waves-effect waves-light">Filter</button>
                    </form>
                </div>
            </div>
        </div>
        <!-- end of card-header  -->
        <div class="row">
            <div class="col-lg-12">
                <div class="project-table">
                    <table class="table">
                        <thead>
                        <tr>
                            <th>Order#</th>
                            <th>Date</th>
                            <th>Design</th>
                            <th>Type</th>
                            <th>Bonus</th>
                        </tr>
                        </thead>
                        <tbody>
                        @php $totals = [ 'new' => 0, 'revision' => 0, 'edit' => 0 ]; @endphp
                        @foreach($bonusTasks as $task)
                            <tr>
                                <td>{{ $task->id }}</td>
                                <td>{{ $task->date->format('d/m/Y') }}</td>
                                <td>{{ $task->name }}</td>
                                <td>{{ $task->order_type }}</td>
                                @php $totals[strtolower($task->order_type)]++; @endphp
                                <td>{{ auth()->user()->sale_wrap_commision }}</td>
                            </tr>
                        @endforeach
                        </tbody>
                        <tfoot>
                        <tr>
                            <th colspan="3">
                                Total
                            </th>
                            <th>
                            </th>
                            <th>
                                {{ $bonusTasks->count() }} x {{ number_format(auth()->user()->sale_wrap_commision) }} =
                                {{ $bonusTasks->count() * auth()->user()->sale_wrap_commision }}
                            </th>
                        </tr>
                        </tfoot>
                    </table>
                </div>
                <!-- end of project table -->

                <div class="text-center">

                    {{ $tasks->links() }}

                </div>

            </div>
            <!-- end of col-lg-12 -->
        </div>
        <!-- end of row -->
    </div>
    <!-- end of card-main -->
</div>