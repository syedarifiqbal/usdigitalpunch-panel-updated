<form action="{{ route('post.profile') }}" method="POST">

    @csrf()

    <table class="table">
        <tbody>
        <tr>
            <td>
                <div class="md-group-add-on">
                    <span class="md-add-on">
                        <i class="icofont icofont-ui-user"></i>
                    </span>
                    <div class="md-input-wrapper">
                        <input type="text" class="md-form-control md-valid" name="name" value="{{ old('name', auth()->user()->name) }}">
                        <label>Full Name</label>
                    </div>
                </div>
            </td>

        </tr>
        <tr>
            <td>

                <div class="form-radio">
                    <div class="md-group-add-on">
                        <span class="md-add-on">
                            <i class="icofont icofont-group-students"></i>
                        </span>
                        <div class="radio radiofill radio-inline">
                            <label>
                                <input type="radio"  name="gender" value="{{ old('gender', auth()->user()->gender) == 'male' ? 'checked': '' }}" value="male"><i class="helper"></i> Male
                            </label>
                        </div>
                        <div class="radio radiofill radio-inline">
                            <label>
                                <input type="radio"  name="gender" value="{{ old('gender', auth()->user()->gender) == 'female' ? 'checked': '' }}" value="female""><i class="helper"></i> Female
                            </label>
                        </div>
                    </div>
                </div>
            </td>

        </tr>
        <tr>
            <td>
                <div class="md-group-add-on">
                <span class="md-add-on">
                        <i class="icofont icofont-ui-calendar"></i>
                    </span>
                    <div class="md-input-wrapper">
                        <input type="text" id="date" class="md-form-control md-valid" value="{{ auth()->user()->dob->format('Y') }}">
                        <label>Birthday Date</label>
                    </div>
                </div>
            </td>
        </tr>
        <tr>
            <td>
                <div class="md-group-add-on">
                    <span class="md-add-on">
                        <i class="icofont icofont-users-alt-4"></i>
                    </span>
                    <div class="md-input-wrapper">
                        <select class="md-form-control">
                            <option>Select Marital Status</option>
                            <option>Married</option>
                            <option>Unmarried</option>
                        </select>
                    </div>
                </div>
            </td>
        </tr>
        <tr>
            <td>
                <div class="md-group-add-on">
                    <span class="md-add-on">
                        <i class="icofont icofont-location-pin"></i>
                    </span>
                    <div class="md-input-wrapper">
                        <textarea class="md-form-control" cols="2" rows="4"></textarea>
                        <label>Address</label>

                    </div>
                </div>
            </td>
        </tr>
        </tbody>
    </table>

</form>