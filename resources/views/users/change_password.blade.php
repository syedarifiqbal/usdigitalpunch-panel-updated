@extends('layouts.master')
@section('content')
<form method="POST" action="{{ $user->id?route('users.change.password.post', ['user' => $user->id]): route('register') }}">
    @csrf
    <section class="forms">
        <div class="row justify-content-center">
            <!-- .col-lg-6 -->
            <div class="col-lg-8">
                <div class="card">
                    <div class="card-header d-flex align-items-center">
                        <h4>Change Password ({{ $user->name }})</h4>
                    </div>
                    <div class="card-block">
                        {{ inputField('Password', 'password', '', $errors->first('password'), ['type' => 'password']) }}

                        {{ inputField('Password Confirmation', 'password_confirmation', '', $errors->first('password_confirmation'), ['type' => 'password']) }}
                        <div class="form-group">
                            <input type="submit" value="Save" class="btn btn-primary">
                        </div>
                    </div>
                </div>
            </div>
            <!-- .col-lg-6 -->
        </div>
        <!-- .row -->
    </section>
</form>
@endsection
@section('footer_scripts')
<script src="{{ asset('assets/pages/elements.js') }}"></script>
@endsection
