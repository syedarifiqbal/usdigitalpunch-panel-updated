<div class="tab-pane" id="project" role="tabpanel">
    <div class="card">
        <div class="card-header">
            <div class="row">
                <div class="col-md-4">
                    <h5 class="card-header-text">Your Bonus</h5>
                </div>
                <div class="col-md-8">
                    <form class="form-inline" method="post" id="filterForm">
                        <div class="form-group">
                            <label class="sr-only form-control-label" for="from_date">From Date</label>
                            <input type="text" class="form-control date" id="from_date" placeholder="From Date" readonly>
                        </div>
                        <div class="form-group">
                            <label class="sr-only form-control-label" for="end_date">End Date</label>
                            <input type="text" class="form-control date" id="end_date" placeholder="End Date" readonly>
                        </div>
                        <button type="submit" class="btn btn-primary waves-effect waves-light">Filter</button>
                    </form>
                </div>
            </div>
        </div>
        <!-- end of card-header  -->
        <div class="row">
            <div class="col-lg-12">
                <div class="project-table">
                    <table class="table">
                        <thead>
                        <tr>
                            <th>Order#</th>
                            <th>Date</th>
                            <th>Design</th>
                            <th>Type</th>
                            <th>Bonus</th>
                            <th>Tip</th>
                        </tr>
                        </thead>
                        <tbody>
                        @php $totals = [ 'new' => 0, 'revision' => 0, 'edit' => 0, 'wj' => 0 ]; @endphp
                        @foreach($bonuses as $bonus)
                            <tr>
                                <td>{{ $bonus->task->id }}</td>
                                <td>{{ $bonus->task->date->format('d/m/Y') }}</td>
                                <td>
                                    @if($bonus->rs < 0)
                                        @php
                                        $orderType = 'WJ';
                                        $wrongJobReference = ' / ' . optional($bonus->task->childrens()->first())->id
                                        @endphp
                                    @else
                                        @php 
                                            $orderType = $bonus->task->order_type;
                                            $wrongJobReference = ''; 
                                        @endphp
                                    @endif
                                    {{ $bonus->task->name }} {{ $wrongJobReference }}
                                </td>
                                <td>{{ $orderType }}</td>
                                @php $totals[strtolower($orderType)]++; @endphp
                                <td>{{ $bonus->rs }}</td>
                                <td>{{ $bonus->tip }}</td>
                            </tr>
                        @endforeach
                        </tbody>
                        <tfoot>
                        <tr>
                            <th colspan="4">
                                New &nbsp;&nbsp;&nbsp;&nbsp; ({{ $totals['new'] }})<br>
                                Revision &nbsp;&nbsp;&nbsp;&nbsp; ({{ $totals['revision'] }})<br>
                                Edit &nbsp;&nbsp;&nbsp;&nbsp; ({{ $totals['edit'] }})<br>
                                Wrong Job &nbsp;&nbsp;&nbsp;&nbsp; ({{ $totals['wj'] }})<br>
                                Total &nbsp;&nbsp;&nbsp;&nbsp; ({{ $bonuses->count() }}) <br>
                                Grand Total &nbsp;&nbsp;&nbsp;&nbsp; <br>
                            </th>
                            <th>
                                {{ $bonuses->where('task.order_type', 'New')->where('rs', '>', 0)->sum('rs') }} <br>
                                {{ $bonuses->where('task.order_type', 'Revision')->where('rs', '>', 0)->sum('rs') }} <br>
                                {{ $bonuses->where('task.order_type', 'Edit')->where('rs', '>', 0)->sum('rs') }} <br>
                                {{ $bonuses->where('task.order_type', 'New')->where('rs', '<', 0)->sum('rs') }} <br>
                                {{ $bonuses->sum('rs') }} <br>
                                {{ $bonuses->sum('rs') . ' + ' . $bonuses->sum('tip') }} = {{ $bonuses->sum('rs') + $bonuses->sum('tip') }} <br>
                            </th>
                            <th>
                                {{ $bonuses->where('task.order_type', 'New')->where('rs', '>', 0)->sum('tip') }} <br>
                                {{ $bonuses->where('task.order_type', 'Revision')->sum('tip') }} <br>
                                {{ $bonuses->where('task.order_type', 'Edit')->sum('tip') }} <br>
                                {{ $bonuses->where('task.order_type', 'New')->where('rs', '<', 0)->sum('tip') }} <br>
                                {{ $bonuses->sum('tip') }} <br>
                                 <br>
                            </th>
                        </tr>
                        </tfoot>
                    </table>
                </div>
                <!-- end of project table -->

                <div class="text-center">

                    {{ $tasks->links() }}

                </div>

            </div>
            <!-- end of col-lg-12 -->
        </div>
        <!-- end of row -->
    </div>
    <!-- end of card-main -->
</div>