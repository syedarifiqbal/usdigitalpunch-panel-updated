@extends('layouts.master')

@section('content')

    <?php $isSuperAdmin = in_array('Super Admin', request('roles')) || in_array('Admin', request('roles')); ?>

<div class="card">
    <div class="card-header">

        <h4>System / User Settings</h4>

    </div>
    <div class="card-block">
        <div class="row">

            <div class="col-md-3">

                <ul class="nav nav-tabs md-tabs tabs-right b-none" role="tablist">
                    @if($isSuperAdmin)
                    <li class="nav-item">
                        <a class="nav-link {{ request('tab', 'general') == 'general'? 'active': '' }}" data-toggle="tab" href="{{ route('settings', ['tab'=>'general']) }}" role="tab">General</a>
                        <div class="slide"></div>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link {{ request('tab') == 'email'? 'active': '' }}" data-toggle="tab" href="{{ route('settings', ['tab'=>'email']) }}" role="tab">Email</a>
                        <div class="slide"></div>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link {{ request('tab') == 'smtp'? 'active': '' }}" data-toggle="tab" href="{{ route('settings', ['tab'=>'smtp']) }}" role="tab">SMTP</a>
                        <div class="slide"></div>
                    </li>
                    @endif
                    <li class="nav-item">
                        <a class="nav-link {{ request('tab') == 'ui'? 'active': '' }}" data-toggle="tab" href="{{ route('settings', ['tab'=>'ui']) }}" role="tab">UI</a>
                        <div class="slide"></div>
                    </li>
                     @if($isSuperAdmin)
                     <li class="nav-item">
                        <a class="nav-link {{ request('tab') == 'email_template'? 'active': '' }}" data-toggle="tab" href="{{ route('settings', ['tab'=>'email_template']) }}" role="tab">Email Template</a>
                        <div class="slide"></div>
                    </li>
                    @endif
                </ul>
            </div>
            <div class="col-md-9">
                <div class="tab-content">

                    <form method="POST" action="{{ route('settings.post', [ 'tab' => request('tab', 'general') ]) }}" class="tab-pane active" id="{{ request('tab', 'general') }}" role="tabpanel">
                        @csrf
                        @if( request('tab', 'general') == 'general' && $isSuperAdmin)
                        <?php $phoneFieldName = 'phone_' . session('company_id', 1); ?>
                        <?php $addressFieldName = 'address_' . session('company_id', 1); ?>
                        <?php $skypeFieldName = 'skype_' . session('company_id', 1); ?>
                        {{ inputField('Skype', $skypeFieldName, old($skypeFieldName, setting($skypeFieldName)), $errors->first($skypeFieldName)) }}
                        {{ inputField('Phone', $phoneFieldName, old($phoneFieldName, setting($phoneFieldName)), $errors->first($phoneFieldName)) }}
                        {{ inputField('Allowed File Extension', 'allowed_extension', old('allowed_extension', setting('allowed_extension')), $errors->first('allowed_extension')) }}
                        {{ textField('Address', $addressFieldName, old($addressFieldName, setting($addressFieldName)), $errors->first($addressFieldName)) }}
                        @endif
                        @if( request('tab') == 'email' && $isSuperAdmin)
                            <?php $fromFieldName = 'email_from_' . session('company_id', 1); ?>
                            <?php $toFieldName = 'email_to_' . session('company_id', 1); ?>
                            <?php $ccFieldName = 'email_cc_' . session('company_id', 1); ?>
                            <?php $bccFieldName = 'email_bcc_' . session('company_id', 1); ?>
                            {{ inputField('Email From', $fromFieldName, old($fromFieldName, setting($fromFieldName)), $errors->first($fromFieldName)) }}
                            {{ inputField('Email To', $toFieldName, old($toFieldName, setting($toFieldName)), $errors->first($toFieldName)) }}
                            {{ inputField('Email CC', $ccFieldName, old($ccFieldName, setting($ccFieldName)), $errors->first($ccFieldName)) }}
                            {{ inputField('Email BCC', $bccFieldName, old($bccFieldName, setting($bccFieldName)), $errors->first($bccFieldName)) }}
                        @endif

                        @if( request('tab') == 'smtp' && $isSuperAdmin)
                        {{ inputField('Host name', 'smtp_host', old('smtp_host', setting('smtp_host')), $errors->first('smtp_host')) }}
                        {{ inputField('Port', 'smtp_port', old('smtp_port', setting('smtp_port', 25)), $errors->first('smtp_port')) }}
                        {{ inputField('Username', 'smtp_username', old('smtp_username', setting('smtp_username')), $errors->first('smtp_username')) }}
                        {{ inputField('Password', 'smtp_password', old('smtp_password', setting('smtp_password')), $errors->first('smtp_password')) }}
                        @endif
                        @if( request('tab') == 'ui' )
                        {{ inputField('Record Per Page', 'table_page_length', old('table_page_length', setting('table_page_length', 25)), $errors->first('table_page_length')) }}
                        @endif
                        @if( request('tab') == 'email_template' && $isSuperAdmin)
                            <?php $templateFieldName = 'delivery_email_template_' . session('company_id', 1); ?>
                            <div>
                            {{ textField('', $templateFieldName, old($templateFieldName, setting($templateFieldName)), $errors->first($templateFieldName), ['id' => 'editor']) }}
                            <table class="table table-bordered">
                                <tr>
                                    <th colspan="2">Available Dynamic Contents</th>
                                </tr>
                                <tr>
                                    <td>[DESIGN_REFERENCE]</td>
                                    <td>[ORDER_DATE]</td>
                                </tr>
                                <tr>
                                    <td>[DELIVERY_DATE]</td>
                                    <td>[CUSTOMER_NAME]</td>
                                </tr>
                                <tr>
                                    <td>[DESIGN_NAME]</td>
                                    <td>[PO_NUMBER]</td>
                                </tr>
                                <tr>
                                    <td>[ORDER_TYPE]</td>
                                    <td>[DESIGN_TYPE]</td>
                                </tr>
                                <tr>
                                    <td>[DESIGN_WIDTH]</td>
                                    <td>[DESIGN_HEIGHT]</td>
                                </tr>
                                <tr>
                                    <td>[NO_STITCHES]</td>
                                    <td>[PRICE]</td>
                                </tr>
                                <tr>
                                    <td>[INSTRUCTIONS]</td>
                                    <td>[CUSTOMER_SECRET_INSTRUCTIONS]</td>
                                </tr>
                            </table>
                            </div>
                        @endif

                        <div class="form-group">
                            <input type="submit" value="Save" class="btn btn-primary">
                        </div>

                    </form>

                    {{--<input type="text" id="lfmUrl" class="form-control">--}}
                    {{--<button id="ifmLauncher">Choose Image</button>--}}

                </div>

            </div>

        </div>

    </div>

</div>

@endsection

@section('styles')

@endsection

@section('footer_scripts')

    @if( request('tab') == 'email_template' )
        <script src="https://cdn.ckeditor.com/4.10.0/standard/ckeditor.js"></script>

        <style>
            .ck.ck-content{
                min-height: 300px;
            }
        </style>

        <script>

            CKEDITOR.replace( 'editor', {
                filebrowserImageBrowseUrl: '/laravel-filemanager?type=Images',
                filebrowserImageUploadUrl: '/laravel-filemanager/upload?type=Images&_token=',
                filebrowserBrowseUrl: '/laravel-filemanager?type=Files',
                filebrowserUploadUrl: '/laravel-filemanager/upload?type=Files&_token='
            });
        </script>
    @endif

<script>

    var lfm = function(options, cb) {

        var route_prefix = (options && options.prefix) ? options.prefix : '/laravel-filemanager';

        window.open(route_prefix + '?type=' + options.type || 'file', 'FileManager', 'width=900,height=600');
        window.SetUrl = cb;
    };

    // $('#ifmLauncher').click(function(){
    //     lfm({type: 'image'}, function(url, path) {
    //         $('#lfmUrl').val(url[0].url);
    //         console.log(url, path)
    //     });
    // });

    $('.nav-tabs .nav-link').on('click', function(){
        window.location = $(this).attr('href');
    });

</script>

@endsection