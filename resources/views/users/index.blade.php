@extends('layouts.master')

@section('styles')

<link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">

<link href="{{ asset('css/customize-datatable.css') }}" rel="stylesheet">

@endsection

@section('content')

<div id="admin" class="col s12">
    <div class="card material-table">
        <div class="table-header">
            <span class="table-title">Users List</span>
            <div class="actions">
                <input type="hidden" id="datatable-url" value="{{ route('dt.users', request('status',null) ? ['status'=>request('status',null)]:null) }}">
                <a href="{{ route('register') }}" class="modal-trigger waves-effect btn-flat nopadding"><i class="material-icons">person_add</i></a>
                <a href="#" class="table-search-toggle waves-effect btn-flat nopadding"><i class="material-icons">search</i></a>
            </div>
        </div>
        <table id="datatable">
            <thead>
                <tr>
                    <th>Full name</th>
                    <th>Email</th>
                    <th>Phone</th>
                    <th>Role</th>
                    <th>Address</th>
                    <th>Status</th>
                    <th>Action</th>
                </tr>
            </thead>
            <tbody></tbody>
        </table>
    </div>
</div>

@endsection


@section('footer_scripts')

@include('components.datatable-scripts');

@endsection