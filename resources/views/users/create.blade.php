@extends('layouts.master')

@section('styles')

<link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">
<link rel="stylesheet" href="{{ asset('assets/plugins/bootstrap/css/bootstrap.min.css') }}">
<link rel="stylesheet" href="{{ asset('assets/plugins/bootstrap-material-datetimepicker/css/bootstrap-material-datetimepicker.css') }}">

@endsection

@section('content')
      
<form method="POST" action="{{ $user->id?route('udpate.users', ['user' => $user->id]): route('register') }}">

    @if($user->id)
        @method('put')
    @endif

    @csrf
    
    <section class="forms">
    
            <div class="row">

                <!-- .col-lg-6 -->
                <div class="col-lg-6">
                    @include('auth.primary')
                </div>
                <!-- .col-lg-6 -->

                <!-- .col-lg-6 -->
                <div class="col-lg-6">
                    @include('auth.emergency')
                    @include('auth.qualification')
                </div>
                <!-- .col-lg-6 -->

                <!-- .col-lg-6 -->
                <div class="col-lg-12">
                    @include('auth.bonus_salary')
                </div>
                <!-- .col-lg-6 -->
                

            </div>
            <!-- .row -->

    </section>

</form>

@endsection

@section('footer_scripts')

<script src="{{ asset('assets/plugins/moment/moment.js') }}"></script>
<script src="{{ asset('assets/plugins/bootstrap-datepicker/dist/js/bootstrap-datepicker.min.js') }}"></script>
<script src="{{ asset('assets/plugins/bootstrap-datepicker/js/bootstrap-datetimepicker.min.js') }}"></script>

<script src="{{ asset('assets/plugins/datepicker/js/moment-with-locales.min.js') }}"></script>
<script src="{{ asset('assets/plugins/bootstrap-material-datetimepicker/js/bootstrap-material-datetimepicker.js') }}"></script>
<script src="{{ asset('assets/pages/elements.js') }}"></script>

<script>
$(function () {

    $('.date').bootstrapMaterialDatePicker({
        time: false,
        clearButton: true,
        format: 'DD/MM/YYYY'
    });

})

</script>

@endsection
