<div class="tab-pane" id="project" role="tabpanel">
    <div class="card">
        <div class="card-header">
            <h5 class="card-header-text">Your Clients Lists</h5>
        </div>
        <!-- end of card-header  -->
        <div class="row">
            <div class="col-lg-12">
                <div class="project-table">
                    <div class="project-table">
                        <table class="table">
                            <thead>
                            <tr>
                                <th>Id#</th>
                                <th>Name</th>
                                <th>Email</th>
                                <th>Phone</th>
                            </tr>
                            </thead>
                            <tbody>
                            @foreach(auth()->user()->clients as $client)
                                <tr>
                                    <td>{{ $client->id }}</td>
                                    <td>{{ $client->name }}</td>
                                    <td>{{ $client->email }}</td>
                                    <td>{{ $client->phone }}</td>
                                </tr>
                            @endforeach
                            </tbody>
                            
                            </tfoot>
                        </table>
                    </div>

                </div>
                <!-- end of project table -->
            </div>
            <!-- end of col-lg-12 -->
        </div>
        <!-- end of row -->
    </div>
    <!-- end of card-main -->
</div>