<div class="tab-pane" id="project" role="tabpanel">
    <div class="card">
        <div class="card-header">
            <h5 class="card-header-text">Your Tasks</h5>
            @can('add-task')
            <button type="button" class="btn btn-primary waves-effect waves-light f-right">
                + ADD PROJECTS</button>
            @endcan
        </div>
        <!-- end of card-header  -->
        <div class="row">
            <div class="col-lg-12">
                <div class="project-table">
                    <div class="table-responsive">
                        <table class="table">
                            <thead>
                            <tr>
                                <!-- <th class="text-center txt-primary pro-pic">Photo</th> -->
                                <th class="text-center txt-primary">Client</th>
                                <th class="text-center txt-primary">Design</th>
                                <th class="text-center txt-primary">Date</th>
                                <th class="text-center txt-primary">Design Type</th>
                                <th class="text-center txt-primary">Order Type</th>
                                <th class="text-center txt-primary">Status</th>
                                <th class="text-center txt-primary">Action</th>
                            </tr>
                            </thead>
                            <tbody class="text-center">
                            @foreach($tasks as $task)
                            <tr>
                                <!-- <td>
                                    <img src="{{ asset('assets/images/avatar-2.png') }}" class="img-circle" alt="tbl">
                                </td> -->
                                <td>{{ $task->client->nick_name }}</td>
                                <td>{{ $task->name }}</td>
                                <!-- <td>Oct 25th, 2015</td> -->
                                <td>{{ $task->date->format('F dS, Y') }}</td>
                                <td>{{ $task->design_type }}</td>
                                <td>{{ $task->order_type }}</td>
                                
                                @if($task->status == 'complete')
                                    <td class="text-center"><span class="label label-success">{{ $task->status }}</span></td>
                                @endif
                                @if($task->status == 'pending')
                                    <td class="text-center"><span class="label label-warning">{{ $task->status }}</span></td>
                                @else
                                    <td class="text-center"><span class="label label-info">{{ $task->status }}</span></td>
                                @endif
                                
                                <td class="faq-table-btn">
                                    
                                    @if(strtolower($task->status) != 'complete')
                                    <a href="{{ route('tasks.submit', ['task' => $task->id, 'submit' => 'submit']) }}" class="btn btn-primary waves-effect waves-light" data-toggle="tooltip" data-placement="top" title="Resubmit the job">
                                        <i class="icofont icofont-refresh"></i>
                                    </a>
                                    @endif

                                </td>
                            </tr>
                            @endforeach
                            </tbody>
                        </table>
                        <!-- end of table -->
                    </div>
                    <!-- end of table responsive -->
                </div>
                <!-- end of project table -->
                
                <div class="text-center">

                    {{ $tasks->links() }}

                </div>

            </div>
            <!-- end of col-lg-12 -->
        </div>
        <!-- end of row -->
    </div>
    <!-- end of card-main -->
</div>