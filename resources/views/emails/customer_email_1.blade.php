<div>
<h5 style="font-variant-ligatures:normal">Dear {{ $task->client->nick_name }}</h5>
<p style="font-variant-ligatures:normal">We are glad to inform you that your order is ready and we hope you will be pleased with it. Please see the attached files and please do contact us if you require any changes. We look forward to your feedback.</p>
<table cellpadding="10" style="font-variant-ligatures:normal; color:navy">

    <tbody>
    <tr style="color:rgb(255,255,255); background-image:initial; background-color:navy; background-size:initial; background-origin:initial; background-clip:initial; background-position:initial; background-repeat:initial">
        <td colspan="2">Following are the details of your order</td>
    </tr>
    <tr>
        <td style="width: 250px;">Company Name</td>
        <td>{{ $task->client->name }}</td>
    </tr>
    <tr>
        <td style="width: 250px;">Customer Name</td>
        <td>{{ $task->client->nick_name }}</td>
    </tr>
    <tr>
        <td style="width: 250px;">Reference Number</td>
        <td>{{ $task->id }}</td>
    </tr>
    <tr>
        <td style="width: 250px;">P.O# Design Name</td>
        <td>{{ $task->po_number }} {{ $task->name }}</td>
    </tr>
    <tr>
        <td style="width: 250px;">Type</td>
        <td>{{ $task->design_type }} Order</td>
    </tr>
    <tr>
        <td style="width: 250px;">Height</td>
        <td>{{ $task->delivery_height }}{{ $task->unit }}</td>
    </tr>
    <tr>
        <td style="width: 250px;">Width</td>
        <td>{{ $task->delivery_width }}{{ $task->unit }}</td>
    </tr>
    <tr>
        <td style="width: 250px;">No. of Stitches</td>
        <td>{{ $task->stitches }}</td>
    </tr>
    <tr>
        <td style="width: 250px;">Price</td>
        <td>{{ $task->currency }} {{ $task->price }}</td>
    </tr>
    <tr>
        <td style="width: 250px;">Your Instruction</td>
        <td>
            <pre>
            <?php $remarks = collect(explode("||", $task->remarks)); ?>
            <?php $remarks->each(function($line, $i){
                echo $i+1 . ") $line <br>";
            }) ?>
            </pre>
        </td>
    </tr>

    <tr>
        <td colspan="2">

            <table style="border-collapse:collapse;">
                <tbody>

                <tr>
                    <td style="border: 1px solid navy;">Admin Comment</td>
                    <td style="border: 1px solid navy;">{{ $task->delivery_remarks }}<br>
                        <p>Thank you and Good Luck</p>
                    </td>
                </tr>
                <tr>
                    <td></td>
                    <td style="padding:20px; font-size: 0.9em;">Please feel free to let us know if you require any changes or if  we can assist you somehow. We sill work with you till you get fully satisfied with the order.
                    </td>
                </tr>
                <tr>
                    <td style="padding:20px; font-size: 1.3em; font-weight: bold;">Note:</td>
                    <td style="padding:20px; font-size: 0.9em; font-style: italic; color: red;">We have done our best with your order <span style="background-color: yellow; font-weight: bold; color: navy;">PLEASE PROOF YOUR FILES</span> as Us Digital Punch will not ne responsible or embroidered/printed items.<br><span style="background-color: yellow; color: navy;">Recomended us to your friends or colleagues in the exchange of benefits.</span>
                    </td>
                </tr>
                </tbody>
            </table>
        </td>
    </tr>

    </tbody>
</table>

<h4 align="center" style="font-variant-ligatures:normal">Thank you for utilizing our services.</h4>
</div>