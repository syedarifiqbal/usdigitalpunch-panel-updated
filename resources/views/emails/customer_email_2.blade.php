<h1 style="font-family:'comic sans ms', sans-serif, serif, EmojiFont;color:red;font-size:22px;text-align:left;" >Hi {{ $task->client->nick_name }},</h1>
<table id="emailWrapper" style="font-family:'comic sans ms', sans-serif, serif, EmojiFont;border-left-width:2px;border-left-style:solid;border-left-color:blue;width:100%;" >
    <tr style="font-family:'comic sans ms', sans-serif, serif, EmojiFont;" >
        <th style="font-family:'comic sans ms', sans-serif, serif, EmojiFont;" >
            <h3 align="left" style="font-family:'comic sans ms', sans-serif, serif, EmojiFont;margin-top:0;" >Hope you are fine.</h3>
        </th>
    </tr>
    <tr>
        <th style="text-align: left">
            We are glad to inform you that your Order is ready. Please see the attached files and please do contact us if you require any changes. We look forward to your feedback.
        </th>
    </tr>
    <tr style="font-family:'comic sans ms', sans-serif, serif, EmojiFont;" >
        <td style="font-family:'comic sans ms', sans-serif, serif, EmojiFont;" >
            <table class="detail" style="font-family:'comic sans ms', sans-serif, serif, EmojiFont;border-collapse:collapse;width:50%;" >
                <tr style="font-family:'comic sans ms', sans-serif, serif, EmojiFont;" >
                    <td colspan="4" style="font-family:'comic sans ms', sans-serif, serif, EmojiFont;border-width:1px;border-style:dashed;border-color:#ccc;padding-top:10px;padding-bottom:10px;padding-right:10px;padding-left:10px;" >
                        <strong style="font-family:'comic sans ms', sans-serif, serif, EmojiFont;" >Following are the details of your order:</strong>
                    </td>
                </tr>
                <tr style="font-family:'comic sans ms', sans-serif, serif, EmojiFont;" >
                    <td style="font-family:'comic sans ms', sans-serif, serif, EmojiFont;border-width:1px;border-style:dashed;border-color:#ccc;padding-top:10px;padding-bottom:10px;padding-right:10px;padding-left:10px;" >Price:</td>
                    <td colspan="3" style="font-family:'comic sans ms', sans-serif, serif, EmojiFont;border-width:1px;border-style:dashed;border-color:#ccc;padding-top:10px;padding-bottom:10px;padding-right:10px;padding-left:10px;" >
                        <span class="color-red" style="font-family:'comic sans ms', sans-serif, serif, EmojiFont;color:red;" >{{ $task->currency }} {{ $task->price }}</span>
                    </td>
                </tr>
                <tr style="font-family:'comic sans ms', sans-serif, serif, EmojiFont;" >
                    <td style="font-family:'comic sans ms', sans-serif, serif, EmojiFont;border-width:1px;border-style:dashed;border-color:#ccc;padding-top:10px;padding-bottom:10px;padding-right:10px;padding-left:10px;" >Height:</td>
                    <td style="font-family:'comic sans ms', sans-serif, serif, EmojiFont;border-width:1px;border-style:dashed;border-color:#ccc;padding-top:10px;padding-bottom:10px;padding-right:10px;padding-left:10px;" >Inch(es) {{ $task->unit == 'in'? $task->delivery_height: '--' }}"</td>
                    <td style="font-family:'comic sans ms', sans-serif, serif, EmojiFont;border-width:1px;border-style:dashed;border-color:#ccc;padding-top:10px;padding-bottom:10px;padding-right:10px;padding-left:10px;" >cm {{ $task->unit == 'cm'? $task->delivery_height: '--' }}</td>
                    <td style="font-family:'comic sans ms', sans-serif, serif, EmojiFont;border-width:1px;border-style:dashed;border-color:#ccc;padding-top:10px;padding-bottom:10px;padding-right:10px;padding-left:10px;" >mm {{ $task->unit == 'mm'? $task->delivery_height: '--' }}</td>
                </tr>
                <tr style="font-family:'comic sans ms', sans-serif, serif, EmojiFont;" >
                    <td style="font-family:'comic sans ms', sans-serif, serif, EmojiFont;border-width:1px;border-style:dashed;border-color:#ccc;padding-top:10px;padding-bottom:10px;padding-right:10px;padding-left:10px;" >Width:</td>
                    <td style="font-family:'comic sans ms', sans-serif, serif, EmojiFont;border-width:1px;border-style:dashed;border-color:#ccc;padding-top:10px;padding-bottom:10px;padding-right:10px;padding-left:10px;" >Inch(es) {{ $task->unit == 'in'? $task->delivery_width: '--' }}"</td>
                    <td style="font-family:'comic sans ms', sans-serif, serif, EmojiFont;border-width:1px;border-style:dashed;border-color:#ccc;padding-top:10px;padding-bottom:10px;padding-right:10px;padding-left:10px;" >cm {{ $task->unit == 'cm'? $task->delivery_width: '--' }}</td>
                    <td style="font-family:'comic sans ms', sans-serif, serif, EmojiFont;border-width:1px;border-style:dashed;border-color:#ccc;padding-top:10px;padding-bottom:10px;padding-right:10px;padding-left:10px;" >mm {{ $task->unit == 'mm'? $task->delivery_width: '--' }}</td>
                </tr>
                <tr style="font-family:'comic sans ms', sans-serif, serif, EmojiFont;" >
                    <td style="font-family:'comic sans ms', sans-serif, serif, EmojiFont;border-width:1px;border-style:dashed;border-color:#ccc;padding-top:10px;padding-bottom:10px;padding-right:10px;padding-left:10px;" >No of Stitches:</td>
                    <td colspan="3" style="font-family:'comic sans ms', sans-serif, serif, EmojiFont;border-width:1px;border-style:dashed;border-color:#ccc;padding-top:10px;padding-bottom:10px;padding-right:10px;padding-left:10px;" >
                        <span class="color-red" style="font-family:'comic sans ms', sans-serif, serif, EmojiFont;color:red;" >{{ $task->stitches }}</span>
                    </td>
                </tr>
                <tr style="font-family:'comic sans ms', sans-serif, serif, EmojiFont;" >
                    <td style="font-family:'comic sans ms', sans-serif, serif, EmojiFont;border-width:1px;border-style:dashed;border-color:#ccc;padding-top:10px;padding-bottom:10px;padding-right:10px;padding-left:10px;" >Your Instruction:</td>
                    <td colspan="3" style="font-family:'comic sans ms', sans-serif, serif, EmojiFont;border-width:1px;border-style:dashed;border-color:#ccc;padding-top:10px;padding-bottom:10px;padding-right:10px;padding-left:10px;" >
                        <pre>
                        <?php $remarks = collect(explode("||", $task->remarks)); ?>
                        <?php $remarks->each(function($line, $i){
                            echo $i+1 . ") $line <br>";
                        }) ?>
                        </pre>
                    </td>
                </tr>
                <tr style="font-family:'comic sans ms', sans-serif, serif, EmojiFont;" >
                    <td style="font-family:'comic sans ms', sans-serif, serif, EmojiFont;border-width:1px;border-style:dashed;border-color:#ccc;padding-top:10px;padding-bottom:10px;padding-right:10px;padding-left:10px;" >Admin Comment:</td>
                    <td colspan="3" style="font-family:'comic sans ms', sans-serif, serif, EmojiFont;border-width:1px;border-style:dashed;border-color:#ccc;padding-top:10px;padding-bottom:10px;padding-right:10px;padding-left:10px;" >
                        <strong style="font-family:'comic sans ms', sans-serif, serif, EmojiFont;" >Thanks & Good Luck</strong>
                    </td>
                </tr>
            </table>
        </td>
    </tr>
    <tr style="font-family:'comic sans ms', sans-serif, serif, EmojiFont;" >
        <td style="font-family:'comic sans ms', sans-serif, serif, EmojiFont;" ><span class="highlight" style="font-family:'comic sans ms', sans-serif, serif, EmojiFont;color:red;background-color:yellow;display:inline-block;padding-top:5px;padding-bottom:5px;padding-right:10px;padding-left:10px;font-weight:bold;" >Please check your logo carefully before running final production.</span></td>
    </tr>
    <tr style="font-family:'comic sans ms', sans-serif, serif, EmojiFont;" >
        <td style="font-family:'comic sans ms', sans-serif, serif, EmojiFont;" >
            <p class="highlight" style="font-family:'comic sans ms', sans-serif, serif, EmojiFont;margin-bottom:0px;color:red;background-color:yellow;display:inline-block;padding-top:5px;padding-bottom:5px;padding-right:10px;padding-left:10px;font-weight:bold;" >Auster Digital Punch will not be responsible or embroidered/printed items.</p>
        </td>
    </tr>
    <tr style="font-family:'comic sans ms', sans-serif, serif, EmojiFont;" >
        <td style="font-family:'comic sans ms', sans-serif, serif, EmojiFont;" >
            <p class="color-green" style="font-family:'comic sans ms', sans-serif, serif, EmojiFont;margin-bottom:0px;color:green;" >Recommend us to your friends or colleagues in the exchange of benefits.</p>
        </td>
    </tr>
    <tr style="font-family:'comic sans ms', sans-serif, serif, EmojiFont;" >
        <td style="font-family:'comic sans ms', sans-serif, serif, EmojiFont;" >
            <p style="font-family:'comic sans ms', sans-serif, serif, EmojiFont;margin-bottom:0px;" >Thank you for utilizing our services. Please feel free to let us know if you need any changes.</p>
        </td>
    </tr>
    <tr style="font-family:'comic sans ms', sans-serif, serif, EmojiFont;" >
        <td style="font-family:'comic sans ms', sans-serif, serif, EmojiFont;" >
            <p style="font-family:'comic sans ms', sans-serif, serif, EmojiFont;margin-bottom:0px;" ><strong style="font-family:'comic sans ms', sans-serif, serif, EmojiFont;" >Best Regards.</strong></p>
        </td>
    </tr>
    <tr style="font-family:'comic sans ms', sans-serif, serif, EmojiFont;" >
        <td style="font-family:'comic sans ms', sans-serif, serif, EmojiFont;" >
            <h3 style="font-family:'comic sans ms', sans-serif, serif, EmojiFont;" >AUSTER DIGITAL PUNCH</h3>
        </td>
    </tr>
</table>