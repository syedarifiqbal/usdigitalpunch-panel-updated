<?php
$data = [
    '[DESIGN_REFERENCE]' => $task->id,
    '[CUSTOMER_NAME]' => $task->client->name,
    '[ORDER_DATE]' => $task->date,
    '[DELIVERY_DATE]' => $task->delivery_date,
    '[DESIGN_NAME]' => $task->name,
    '[PO_NUMBER]' => $task->po_number,
    '[ORDER_TYPE]' => $task->order_type,
    '[DESIGN_TYPE]' => $task->design_type,
    '[DESIGN_WIDTH]' => $task->width,
    '[DESIGN_HEIGHT]' => $task->height,
    '[NO_STITCHES]' => $task->no_stitches,
    '[PRICE]' => $_POST['currency'] . ' ' . $_POST['price'],
    '[CUSTOMER_SECRET_INSTRUCTIONS]' => $task->client->scret_instruction,
    '[INSTRUCTIONS]' => $task->remarks,
];

?>
{!! str_replace(array_keys($data), array_values($data), $template) !!}

{{--<h5>Dear {{ $task->client->name }}</h5>--}}
{{--<p>We are glad to inform you that your order is ready and we hope you will be pleased with it. Please see the attached files and please do contact us if you require any changes. We look forward to your feedback.</p>--}}
{{--<table style="color: navy;" cellpadding="10">--}}

    {{--<tbody>--}}
    {{--<tr style="background: navy; color: #fff;">--}}
        {{--<td colspan="2">Following are the details of your order</td>--}}
    {{--</tr>--}}
    {{--<tr>--}}
        {{--<td style="width: 250px;">Company Name</td>--}}
        {{--<td>{{ $task->client->name }}</td>--}}
    {{--</tr>--}}
    {{--<tr>--}}
        {{--<td style="width: 250px;">Customer Name</td>--}}
        {{--<td>{{ $task->client->name }}</td>--}}
    {{--</tr>--}}
    {{--<tr>--}}
        {{--<td style="width: 250px;">Reference Number</td>--}}
        {{--<td>{{ $task->id }}</td>--}}
    {{--</tr>--}}
    {{--<tr>--}}
        {{--<td style="width: 250px;">P.O# Design Name</td>--}}
        {{--<td>{{ $task->po_number }} {{ $task->name }}</td>--}}
    {{--</tr>--}}
    {{--<tr>--}}
        {{--<td style="width: 250px;">Type</td>--}}
        {{--<td>{{ $task->design_type }} Order</td>--}}
    {{--</tr>--}}
    {{--<tr>--}}
        {{--<td style="width: 250px;">Height</td>--}}
        {{--<td>{{ $task->delivery_height }}</td>--}}
    {{--</tr>--}}
    {{--<tr>--}}
        {{--<td style="width: 250px;">Width</td>--}}
        {{--<td>{{ $task->delivery_width }}</td>--}}
    {{--</tr>--}}
    {{--<tr>--}}
        {{--<td style="width: 250px;">No. of Stitches</td>--}}
        {{--<td>{{ $task->stitches }}</td>--}}
    {{--</tr>--}}
    {{--<tr>--}}
        {{--<td style="width: 250px;">Price</td>--}}
        {{--<td>{{ $task->currency }} {{ $task->price }}</td>--}}
    {{--</tr>--}}
    {{--<tr>--}}
        {{--<td style="width: 250px;">Your Instruction</td>--}}
        {{--<td>{{ $task->delivery_remarks }}</td>--}}
    {{--</tr>--}}

    {{--<tr>--}}
        {{--<td colspan="2">--}}

            {{--<table style="border-collapse:collapse;">--}}
                {{--<tbody>--}}

                {{--<tr>--}}
                    {{--<td style="border: 1px solid navy;">Admin Comment</td>--}}
                    {{--<td style="border: 1px solid navy;">{{ $task->delivery_remarks }}<br>--}}
                        {{--<p>Thank you and Good Luck</p>--}}
                    {{--</td>--}}
                {{--</tr>--}}
                {{--<tr>--}}
                    {{--<td></td>--}}
                    {{--<td style="padding:20px; font-size: 0.9em;">Please feel free to let us know if you require any changes or if  we can assist you somehow. We sill work with you till you get fully satisfied with the order.--}}
                    {{--</td>--}}
                {{--</tr>--}}
                {{--<tr>--}}
                    {{--<td style="padding:20px; font-size: 1.3em; font-weight: bold;">Note:</td>--}}
                    {{--<td style="padding:20px; font-size: 0.9em; font-style: italic; color: red;">We have done our best with your order <span style="background-color: yellow; font-weight: bold; color: navy;">PLEASE PROOF YOUR FILES</span> as Us Digital Punch will not ne responsible or embroidered/printed items.<br><span style="background-color: yellow; color: navy;">Recomended us to your friends or colleagues in the exchange of benefits.</span>--}}
                    {{--</td>--}}
                {{--</tr>--}}
                {{--</tbody>--}}
            {{--</table>--}}
        {{--</td>--}}
    {{--</tr>--}}

    {{--</tbody>--}}
{{--</table>--}}

{{--<h4 align="center">Thank you for utilizing our services.</h4>--}}