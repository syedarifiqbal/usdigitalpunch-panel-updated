@extends('layouts.master')

@section('styles')

<link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">

@endsection

@section('content')

<div class="card">
    <div class="card-header">
        <h5 class="card-header-title">Roles List</h5>
    </div>

    <div class="table-responsive">

        <table class="table table-striped">
            <thead>
                <tr>
                    <th>ID</th>
                    <th>Role</th>
                    <th>Label</th>
                    <th>Action</th>
                </tr>
            </thead>
            <tbody>
                @foreach( $roles as $role )

                    <tr>

                        <td>{{ $role->id }}</td>
                        <td>{{ $role->name }}</td>
                        <td>{{ $role->label }}</td>
                        <td>
                            <form action="{{ route('roles.delete', ['role' => $role->id]) }}" method="POST">
                                @method('delete')
                                @csrf
                                <a href="{{ route('roles.edit', ['role' => $role->id]) }}" class="btn btn-primary-btn waves-light waves-effect mini">Edit</a>
                                <button class="btn btn-primary-btn waves-effect mini" type="submit">Delete</button>
                            </form>
                        </td>

                    </tr>

                @endforeach
            </tbody>
        </table>
    </div>
</div>

<a id="fab" href="{{ route('roles.create') }}" class="btn btn-icon waves-effect waves-light btn-primary"><i class="material-icons">add</i></a>

@endsection

@section('footer_scripts')

<script type="text/javascript" src="{{ asset('assets/pages/elements.js') }}"></script>

@endsection