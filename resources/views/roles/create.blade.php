@extends('layouts.master')

@section('styles')

<link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">

<link href="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.6-rc.0/css/select2.min.css" rel="stylesheet" />

<link href="{{ asset('css/customize-select2.css') }}" rel="stylesheet" />

@endsection

@section('content')


<form  method="POST" action="{{ $role->id ? route('roles.update', ['role' => $role->id]) : route('roles.store') }}" class="card-block">

    @csrf

    @if( $role->id )
        @method('PUT')
    @endif

    <div class="row">

        <!-- .col-lg-6 -->
        <div class="col-lg-6">

            <div class="card">

                <div class="card-header">

                    <h5 class="card-header-text">@if($role->id) Edit @else Add @endif Role</h5>
                    
                </div>

                <div class="card-block">

                    {{ inputField('Name', 'name', old('name', $role->name), $errors->first('name')) }}

                    {{ inputField('Label', 'label', old('label', $role->label), $errors->first('label')) }}

                    <div class="form-group">
                        <input type="submit" value="Save" class="btn btn-primary">
                    </div>

                </div>
                
            </div>

        </div>
        <!-- .col-lg-6 -->

        <!-- .col-lg-6 -->
        <div class="col-lg-6">

            <div class="card">

                <div class="card-header">

                    <h5 class="card-header-text">Permissions</h5>
                    
                </div>

                <div class="card-block">
                    
                    <div class="col-sm-12 table-responsive">

                        <table class="table table-bordered table-striped" id="contactForm">        
                            
                            <thead>
                            
                                <tr>
                                
                                    <th>Module</th>
                                    <th>Permissions</th>

                                </tr>

                            </thead>

                            <tbody>

                                @foreach($permissions as $module => $perms)
                                <tr>
                                
                                    <th>
                                        {{ $module }}
                                    </th>
                                    <td>
                                        <div class="col">
                                            @foreach($perms as $perm)
                                            <div class="form-check ">
                                                <input class="form-check-input" name="permissions[]" type="checkbox" value="{{ $perm->id }}" id="{{ $perm->id }}_perm" style="margin-left: 0;"
                                                @if($hasPerms->contains($perm->id)) checked @endif />

                                                <label class="form-check-label" for="{{ $perm->id }}_perm">{{ $perm->label }}</label>
                                            </div>
                                            @endforeach
                                        </div>
                                    </td>
                                
                                </tr>

                                @endforeach
                            
                            </tbody>
                        
                        </table>

                    </div>

                    @if( $errors->has('permissions') )
                        <p class="has-error">{{ $errors->first('permissions') }}</p>
                    @endif

                </div>
                
            </div>

        </div>
        <!-- .col-lg-6 -->

    </div>
    <!-- .row -->

</form>

@endsection


@section('footer_scripts')

@include('components.select2-scripts')

<script src="{{ asset('assets/pages/elements.js') }}"></script>
<script src="{{ asset('js/contact-table-form.js') }}"></script>

@endsection