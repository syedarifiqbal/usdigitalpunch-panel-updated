@if($today_tasks->isEmpty())

<div class="card">
    <div class="card-header">
        <div class="card-header">
            <h5 class="card-header-text">There is no tasks for today</h5>
        </div>
    </div>
</div>
@else
<div id="card" class="col">
    <div class="card">
        <div class="card-header">
            <span class="table-title">Today's Tasks</span>
        </div>
        <div class="card-block">
            <table class="table" data-page-length="@setting('table_page_length', 25)">
                <thead>
                    <tr>
                        <th>ID</th>
                        <th>Date</th>
                        <th>Design Name</th>
                        <th>Customer</th>
                        <th>Designer</th>
                        <th>Status</th>
                        <th>Order Type</th>
                        <th>PO #</th>
                        <th>Priority</th>
                        <th>Action</th>
                    </tr>
                </thead>
                <tbody>
                    @foreach($today_tasks as $task)
                    <tr>
                        <td>{{ $task->id }}</td>
                        <td>{{ $task->date->format('d/m/Y') }}</td>
                        <td>{{ $task->name }}</td>
                        <td>{{ $task->client->nick_name }}</td>
                        <td>{{ optional($task->designer)->name }}</td>
                        <td>{{ $task->status }}</td>
                        <td>{{ $task->order_type }}</td>
                        <td>{{ $task->po_number }}</td>
                        <td>{{ $task->priority }}</td>
                        <td>
                            <a href="{{ route('tasks.show', [$task->id]) }}" title="View Task" class="btn btn-icon waves-effect waves-light btn-primary"><i class="icofont icofont-eye"></i></a>
                            <a href="{{ route('tasks.submit', [$task->id, 'submit' => 'submit']) }}" title="Submit the Task" class="btn btn-icon waves-effect waves-light btn-info"><i class="icofont icofont-space-shuttle"></i></a>
                        </td>
                    </tr>
                    @endforeach
                </tbody>
            </table>
        </div>
    </div>
</div>
@endif