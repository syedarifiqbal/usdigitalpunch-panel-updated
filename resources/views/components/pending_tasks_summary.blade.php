{{--{{ dd($countriesTask) }}--}}


<div class="row">
    <div class="col-md-6">
        <div class="summary-table-container">
            <span class="table-title">Order Summary</span>
            <div class="table-responsive">
                <table class="table table-bordered" style="font-size: 0.65em;">
                    <?php
                    $totalNew = 0;
                    $totalRevision = 0;
                    $totalEdit = 0;
                    $totalPending = 0;
                    $totalRfs = 0;
                    $totalUnsigned = 0;
                    ?>
                    <tr>
                        <th>Flag</th>
                        <th>New</th>
                        <th>Revision</th>
                        <th>Edit</th>
                        <th>Total</th>
                        <th>Pending</th>
                        <th>RFS</th>
                        <th>Unsigned</th>
                    </tr>
                    @foreach($pending_task_summary as $country => $summary)
                        <?php
                        $new = $summary['digitizing_new'] + $summary['vector_new'] + $summary['quotation_new'];
                        $revision = $summary['digitizing_revision'] + $summary['vector_revision'] + $summary['quotation_revision'];
                        $edit = $summary['digitizing_edit'] + $summary['vector_edit'] + $summary['quotation_edit'];
                        $pending = $summary['digitizing_pending'] + $summary['vector_pending'] + $summary['quotation_pending'];
                        $rfs = $summary['digitizing_rfs'] + $summary['vector_rfs'] + $summary['quotation_rfs'];
                        $unsigned = $summary['digitizing_unsigned'] + $summary['vector_unsigned'] + $summary['quotation_unsigned'];

                        $totalNew += $new;
                        $totalRevision += $revision;
                        $totalEdit += $edit;
                        $totalPending += $pending;
                        $totalRfs += $rfs;
                        $totalUnsigned += $unsigned;
                        ?>
                        <tr>
                            <td><img src="{{ str_replace('https://', 'http://', $summary['flag']) }}" alt="{{ $country }}" width="32"></td>
                            <td>{{ $new }}</td>
                            <td>{{ $revision }}</td>
                            <td>{{ $edit }}</td>
                            <td>{{ $new + $revision + $edit }}</td>
                            <td>{{ $pending }}</td>
                            <td>{{ $rfs }}</td>
                            <td>{{ $unsigned }}</td>
                        </tr>
                    @endforeach

                    <tr>
                        <th>Total</th>
                        <th>{{ $totalNew }}</th>
                        <th>{{ $totalRevision }}</th>
                        <th>{{ $totalEdit }}</th>
                        <th>{{ $totalNew + $totalRevision + $totalEdit }}</th>
                        <th>{{ $totalPending }}</th>
                        <th>{{ $totalRfs }}</th>
                        <th>{{ $totalUnsigned }}</th>
                    </tr>
                    </tbody>
                </table>
            </div>
        </div>
    </div>

    {{-----------------------------------------------------------------------------------------------------------}}
    {{--                                                Digitize Summary                                       --}}
    {{-----------------------------------------------------------------------------------------------------------}}

    <div class="col-md-6">
        <div class="summary-table-container">
            <span class="table-title">Digitize Summary</span>
            <div class="table-responsive">
                <table class="table table-bordered" style="font-size: 0.65em;">
                    <?php
                    $totalNew = 0;
                    $totalRevision = 0;
                    $totalEdit = 0;
                    $totalPending = 0;
                    $totalRfs = 0;
                    $totalUnsigned = 0;
                    ?>
                    <tr>
                        <th>Flag</th>
                        <th>New</th>
                        <th>Revision</th>
                        <th>Edit</th>
                        <th>Total</th>
                        <th>Pending</th>
                        <th>RFS</th>
                        <th>Unsigned</th>
                    </tr>
                    @foreach($pending_task_summary as $country => $summary)
                        <?php
                        $rowTotal = $summary['digitizing_new'] + $summary['digitizing_revision'] + $summary['digitizing_edit'];
                        $totalNew += $summary['digitizing_new'];
                        $totalRevision += $summary['digitizing_revision'];
                        $totalEdit += $summary['digitizing_edit'];
                        $totalPending += $summary['digitizing_pending'];
                        $totalRfs += $summary['digitizing_rfs'];
                        $totalUnsigned += $summary['digitizing_unsigned'];
                        ?>
                        @if (!$rowTotal)
                            @continue
                        @endif

                        <tr>
                            <td><img src="{{ str_replace('https://', 'http://', $summary['flag']) }}" alt="{{ $country }}" width="32"></td>
                            <td>{{ $summary['digitizing_new'] }}</td>
                            <td>{{ $summary['digitizing_revision'] }}</td>
                            <td>{{ $summary['digitizing_edit'] }}</td>
                            <td>{{ $rowTotal }}</td>
                            <td>{{ $summary['digitizing_pending'] }}</td>
                            <td>{{ $summary['digitizing_rfs'] }}</td>
                            <td>{{ $summary['digitizing_unsigned'] }}</td>
                        </tr>
                    @endforeach

                    <tr>
                        <th>Total</th>
                        <th>{{ $totalNew }}</th>
                        <th>{{ $totalRevision }}</th>
                        <th>{{ $totalEdit }}</th>
                        <th>{{ $totalNew + $totalRevision + $totalEdit }}</th>
                        <th>{{ $totalPending }}</th>
                        <th>{{ $totalRfs }}</th>
                        <th>{{ $totalUnsigned }}</th>
                    </tr>
                    </tbody>
                </table>
            </div>
        </div>
    </div>


    {{-----------------------------------------------------------------------------------------------------------}}
    {{--                                                 Vector Summary                                        --}}
    {{-----------------------------------------------------------------------------------------------------------}}

    <div class="col-md-6">
        <div class="summary-table-container">
            <span class="table-title">Vector Summary</span>
            <div class="table-responsive">
                <table class="table table-bordered" style="font-size: 0.65em;">
                    <?php
                    $totalNew = 0;
                    $totalRevision = 0;
                    $totalEdit = 0;
                    $totalPending = 0;
                    $totalRfs = 0;
                    $totalUnsigned = 0;
                    ?>
                    <tr>
                        <th>Flag</th>
                        <th>New</th>
                        <th>Revision</th>
                        <th>Edit</th>
                        <th>Total</th>
                        <th>Pending</th>
                        <th>RFS</th>
                        <th>Unsigned</th>
                    </tr>
                    @foreach($pending_task_summary as $country => $summary)
                        <?php
                        $rowTotal = $summary['vector_new'] + $summary['vector_revision'] + $summary['vector_edit'];

                        $totalNew += $summary['vector_new'];
                        $totalRevision += $summary['vector_revision'];
                        $totalEdit += $summary['vector_edit'];
                        $totalPending += $summary['vector_pending'];
                        $totalRfs += $summary['vector_rfs'];
                        $totalUnsigned += $summary['vector_unsigned'];
                        ?>
                        @if (!$rowTotal)
                            @continue
                        @endif

                        <tr>
                            <td><img src="{{ str_replace('https://', 'http://', $summary['flag']) }}" alt="{{ $country }}" width="32"></td>
                            <td>{{ $summary['vector_new'] }}</td>
                            <td>{{ $summary['vector_revision'] }}</td>
                            <td>{{ $summary['vector_edit'] }}</td>
                            <td>{{ $rowTotal }}</td>
                            <td>{{ $summary['vector_pending'] }}</td>
                            <td>{{ $summary['vector_rfs'] }}</td>
                            <td>{{ $summary['vector_unsigned'] }}</td>
                        </tr>
                    @endforeach

                    <tr>
                        <th>Total</th>
                        <th>{{ $totalNew }}</th>
                        <th>{{ $totalRevision }}</th>
                        <th>{{ $totalEdit }}</th>
                        <th>{{ $totalNew + $totalRevision + $totalEdit }}</th>
                        <th>{{ $totalPending }}</th>
                        <th>{{ $totalRfs }}</th>
                        <th>{{ $totalUnsigned }}</th>
                    </tr>
                    </tbody>
                </table>
            </div>
        </div>
    </div>


    {{-----------------------------------------------------------------------------------------------------------}}
    {{--                                                 Vector Summary                                        --}}
    {{-----------------------------------------------------------------------------------------------------------}}

    <div class="col-md-6">
        <div class="summary-table-container">
            <span class="table-title">quotation Summary</span>
            <div class="table-responsive">
                <table class="table table-bordered" style="font-size: 0.65em;">
                    <?php
                    $totalNew = 0;
                    $totalRevision = 0;
                    $totalEdit = 0;
                    $totalPending = 0;
                    $totalRfs = 0;
                    $totalUnsigned = 0;
                    ?>
                    <tr>
                        <th>Flag</th>
                        <th>New</th>
                        <th>Revision</th>
                        <th>Edit</th>
                        <th>Total</th>
                        <th>Pending</th>
                        <th>RFS</th>
                        <th>Unsigned</th>
                    </tr>
                    @foreach($pending_task_summary as $country => $summary)
                        <?php
                        $rowTotal = $summary['quotation_new'] + $summary['quotation_revision'] + $summary['quotation_edit'];

                        $totalNew += $summary['quotation_new'];
                        $totalRevision += $summary['quotation_revision'];
                        $totalEdit += $summary['quotation_edit'];
                        $totalPending += $summary['quotation_pending'];
                        $totalRfs += $summary['quotation_rfs'];
                        $totalUnsigned += $summary['quotation_unsigned'];
                        ?>
                        @if (!$rowTotal)
                            @continue
                        @endif

                        <tr>
                            <td><img src="{{ str_replace('https://', 'http://', $summary['flag']) }}" alt="{{ $country }}" width="32"></td>
                            <td>{{ $summary['quotation_new'] }}</td>
                            <td>{{ $summary['quotation_revision'] }}</td>
                            <td>{{ $summary['quotation_edit'] }}</td>
                            <td>{{ $rowTotal }}</td>
                            <td>{{ $summary['quotation_pending'] }}</td>
                            <td>{{ $summary['quotation_rfs'] }}</td>
                            <td>{{ $summary['quotation_unsigned'] }}</td>
                        </tr>
                    @endforeach

                    <tr>
                        <th>Total</th>
                        <th>{{ $totalNew }}</th>
                        <th>{{ $totalRevision }}</th>
                        <th>{{ $totalEdit }}</th>
                        <th>{{ $totalNew + $totalRevision + $totalEdit }}</th>
                        <th>{{ $totalPending }}</th>
                        <th>{{ $totalRfs }}</th>
                        <th>{{ $totalUnsigned }}</th>
                    </tr>
                    </tbody>
                </table>
            </div>
        </div>
    </div>

</div>

