@if($tasks->isEmpty())
    <div class="card">
        <div class="card-header">
            <div class="card-header">
                <h5 class="card-header-text">There is no pending task</h5>
            </div>
        </div>
    </div>
@else
    <div id="card" class="col">
        <div class="card">
            <div class="card-header">
                <span class="table-title">Your Pending Tasks</span>
                <div class="actions">
                </div>
            </div>
            <div class="card-block">
                <table class="table" data-page-length="@setting('table_page_length', 25)">
                    <thead>
                    <tr>
                        <th>ID</th>
                        <th>Date</th>
                        <th>Design Name</th>
                        <th>Customer</th>
                        <th>Designer</th>
                        <th>Order Type</th>
                        <th>Design Type</th>
                        <th>PO #</th>
                        <th>Priority</th>
                        <th>Created At</th>
                    </tr>
                    </thead>
                    <tbody>
                    @foreach($tasks as $task)
                        <tr class="{{ $task->isLate()? 'bg-danger':'' }}">
                            <td>{{ $task->id }}</td>
                            <td>{{ $task->date->format('d/m/Y') }}</td>
                            <td>{{ $task->name }}</td>
                            <td>{{ $task->client->nick_name }}</td>
                            <td>{{ optional($task->designer)->name }}</td>
                            <td>{{ $task->order_type }}</td>
                            <td>{{ $task->design_type }}</td>
                            <td>{{ $task->po_number }}</td>
                            <td>{{ $task->priority }}</td>
                            <td>{{ $task->created_at->format('d/m/Y h:i:s a') }}</td>
                        </tr>
                    @endforeach
                    </tbody>
                </table>
            </div>
        </div>
    </div>
@endif