@if($currentUsersTask->isEmpty())
<div class="summary-table-container">
    <h5 class="card-header-text">There is no task assigned to any user.</h5>
</div>
@else
<div id="card" class="col">
    <div class="summary-table-container">
        <span class="table-title">Tasks Signed to Users</span>
        <div class="table-responsive">
            <table class="table table-bordered" data-page-length="@setting('table_page_length', 25)">
                <tr>
                    <td><strong>Username</strong></td>
                    @foreach($currentUsersTask as $user)
                        <td>{{ $user->name }}</td>
                    @endforeach
                </tr>
                <tr>
                    <td><strong>No. of Tasks</strong></td>
                    @foreach($currentUsersTask as $tasks_count => $user)
                        <td>{{ $user->tasks_count }}</td>
                    @endforeach
                </tr>
                </tbody>
            </table>
        </div>
    </div>
</div>
@endif