@extends('layouts.master')

@section('styles')

<link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">

<link href="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.6-rc.0/css/select2.min.css" rel="stylesheet" />

<link href="{{ asset('css/customize-select2.css') }}" rel="stylesheet" />

@endsection

@section('content')


<form  method="POST" action="{{ $permission->id ? route('permissions.update', ['permission' => $permission->id]) : route('permissions.store') }}" class="card-block">

    @csrf

    @if( $permission->id )
        @method('PUT')
    @endif

    <div class="row">

        <!-- .col-lg-6 -->
        <div class="col-lg-6">

            <div class="card">

                <div class="card-header">

                    <h5 class="card-header-text">@if($permission->id) Edit @else Add @endif Permission</h5>
                    
                </div>

                <div class="card-block">

                    {{ inputField('Name', 'name', old('name', $permission->name), $errors->first('name')) }}

                    {{ inputField('Label', 'label', old('label', $permission->label), $errors->first('label')) }}

                    {{ inputField('module', 'module', old('module', $permission->module), $errors->first('module')) }}

                    <div class="form-group">
                        <input type="submit" value="Save" class="btn btn-primary">
                    </div>

                </div>
                
            </div>

        </div>
        <!-- .col-lg-6 -->

        <!-- .col-lg-6 -->
        <div class="col-lg-6">

            <div class="card">

                <div class="card-header">

                    <h5 class="card-header-text">Client Contact Details</h5>

                    <div class="f-right">
                        <a href="#" id="addContact">
                            <i class="icofont icofont-plus"></i>
                        </a>
                        <a href="#" id="deleteContact">
                            <i class="icofont icofont-trash"></i>
                        </a>
                    </div>
                    
                </div>

                <div class="card-block">
                    
                    <div class="col-sm-12 table-responsive">

                        <table class="table" id="contactForm">        
                            
                            <thead>
                            
                                <tr>
                                
                                    <th>Person Name</th>
                                    <th>Email</th>
                                    <th>Phone</th>

                                </tr>

                            </thead>

                            <tbody>

                                @if(old('contacts'))

                                @foreach(old('contacts') as $contact)
                                <tr>
                                
                                    <td>
                                        {{ inputField('', "contacts[$loop->index][name]", $contact['name'], $errors->first('contacts.'.$loop->index.'.name')) }}
                                    </td>
                                    <td>
                                        {{ inputField('', "contacts[$loop->index][email]", $contact['email'], $errors->first('contacts.'.$loop->index.'.email')) }}
                                    </td>
                                    <td>
                                        {{ inputField('', "contacts[$loop->index][phone]", $contact['phone'], $errors->first('contacts.'.$loop->index.'.phone')) }}
                                    </td>
                                
                                </tr>

                                @endforeach

                                @else
                            
                                <tr>
                                
                                    <td>
                                        {{ inputField('', 'contacts[0][name]', '') }}
                                    </td>
                                    <td>
                                        {{ inputField('', 'contacts[0][email]', '') }}
                                    </td>
                                    <td>
                                        {{ inputField('', 'contacts[0][phone]', '') }}
                                    </td>
                                
                                </tr>


                                @endif
                            
                            </tbody>
                        
                        </table>

                    </div>

                </div>
                
            </div>

        </div>
        <!-- .col-lg-6 -->

    </div>
    <!-- .row -->

</form>

@endsection


@section('footer_scripts')

@include('components.select2-scripts')

<script src="{{ asset('assets/pages/elements.js') }}"></script>
<script src="{{ asset('js/contact-table-form.js') }}"></script>

@endsection