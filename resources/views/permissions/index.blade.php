@extends('layouts.master')

@section('styles')

<link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">

@endsection

@section('content')

<div class="card">
    <div class="card-header">
        <h5 class="card-header-title">Permissions List</h5>
    </div>

    <div class="table-responsive">

        <table class="table table-striped">
            <thead>
                <tr>
                    <th>ID</th>
                    <th>Permissions</th>
                    <th>Label</th>
                    <th>Group</th>
                    <th>Action</th>
                </tr>
            </thead>
            <tbody>
                @foreach( $permissions as $permission )

                    <tr>

                        <td>{{ $permission->id }}</td>
                        <td>{{ $permission->name }}</td>
                        <td>{{ $permission->label }}</td>
                        <td>{{ $permission->module }}</td>
                        <td>
                            <a href="{{ route('permissions.edit', ['permission' => $permission->id]) }}" class="btn btn-primary-btn waves-light waves-effect mini">Edit</a>
                            <a href="{{ route('permissions.delete', ['permission' => $permission->id]) }}" class="btn btn-primary-btn waves-effect mini">Delete</a>
                        </td>

                    </tr>

                @endforeach
            </tbody>
        </table>
    </div>
</div>

<a id="fab" href="{{ route('permissions.create') }}" class="btn btn-icon waves-effect waves-light btn-primary"><i class="material-icons">add</i></a>

@endsection

@section('footer_scripts')

<script type="text/javascript" src="{{ asset('assets/pages/elements.js') }}"></script>

@endsection