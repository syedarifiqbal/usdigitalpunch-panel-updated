@extends('sales.layouts.app')

@section('content')

    <div class="container">
        <div class="row">
            <div class="col-md-12">
                <div class="card">
                    <div class="card-body">
                        <h5 class="card-title">All History</h5>
                        <table class="table table-striped table-bordered">
                            <tr>
                                <th>Date</th>
                                <th>Subject</th>
                                <th>Description</th>
                                <th>Duration</th>
                                <th>Feedback</th>
                                <th>Call by</th>
                            </tr>
                            @foreach($calls as $call)
                                <tr>
                                    <td>{{ $call->created_at->format('d/m/Y h:i:s A') }}</td>
                                    <td>{{ $call->subject }}</td>
                                    <td>{{ $call->description }}</td>
                                    <td>{{ $call->duration }}</td>
                                    <td>{{ $call->feedback }}</td>
                                    <td>{{ $call->owner->name }}</td>
                                </tr>
                            @endforeach
                        </table>
                    </div>
                </div>
            </div>

        </div>
    </div>

@endsection