@extends('sales.layouts.app')

@section('content')

<div class="container">
    <div class="row">
        <div class="col-md-12">
            @if(!count($clients))
                <div class="card">
                    <div class="card-body text-center">
                        <h5 class="card-title">My Customers List</h5>
                        <h5 class="card-subtitle mb-2 text-muted">No records found!</h5>
                    </div>
                </div>
            @else

            <div class="card">
                <div class="card-header">My Customers List</div>

                <div class="card-body">

                    <table class="table table-bordered table-striped">
                        <thead>
                            <tr>
                                <th>Customer no.</th>
                                <th>Name</th>
                                <th>Company</th>
                                <th>Phone</th>
                                <th>Email</th>
                                {{--<th>Actions</th>--}}
                            </tr>
                        </thead>
                        <tbody>
                        @foreach($clients as $client)
                            <tr>
                                <td>{{ $client->id }}</td>
                                <td>{{ $client->name }}</td>
                                <td>{{ $client->company }}</td>
                                <td>{{ $client->phone }}</td>
                                <td>{{ $client->email }}</td>
                                {{--<td>--}}
                                    {{--<div class="dropdown">--}}
                                        {{--<button class="btn dropdown-toggle" type="button" id="dropdownMenuButton" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">--}}
                                            {{--<i class="fa fa-ellipsis-v"></i>--}}
                                        {{--</button>--}}
                                        {{--<div class="dropdown-menu" aria-labelledby="dropdownMenuButton">--}}
                                            {{--<a class="dropdown-item" href="{{ route('leads.edit', ['lead' => $client->id]) }}">Edit</a>--}}
                                            {{--<form action="{{ route('leads.delete', ['lead' => $client->id]) }}" method="post">--}}
                                                {{--@method('delete')--}}
                                                {{--@csrf--}}
                                                {{--<button type="submit" class="dropdown-item">Delete</button>--}}
                                            {{--</form>--}}
                                            {{--<a class="dropdown-item" href="{{ route('leads.calls.create', ['lead' => $client->id]) }}">Schedule Call</a>--}}
                                            {{--<form action="{{ route('leads.move.to.customer', ['lead' => $client->id]) }}" method="post">--}}
                                                {{--@csrf--}}
                                                {{--<button type="submit" class="dropdown-item">Move to Customer</button>--}}
                                            {{--</form>--}}
                                            {{--<a href="{{ route('leads.show', ['lead' => $client->id]) }}" class="dropdown-item">History</a>--}}
                                        {{--</div>--}}
                                    {{--</div>--}}
                                {{--</td>--}}
                            </tr>
                        @endforeach
                        </tbody>
                    </table>
                </div>
            </div>

            @endif

        </div>
    </div>
</div>

<a href="{{ route('leads.create') }}" id="fab">+</a>

@endsection