@extends('sales.layouts.app')

@section('content')

<div class="container">
    <div class="row">
        <div class="col-md-3">
            <div class="card">
                <div class="card-body">
                    <h5 class="card-title">{{ $lead->name }}</h5>
                    <h6 class="card-subtitle mb-2 text-muted">Company: {{ $lead->company }}</h6>
                    <h6 class="card-subtitle mb-2 text-muted">Email: {{ $lead->email }}</h6>
                    <h6 class="card-subtitle mb-2 text-muted">Phone: {{ $lead->phone }}</h6>
                    <h6 class="card-subtitle mb-2 text-muted">Mobile: {{ $lead->mobile }}</h6>
                    <p class="card-text">{{ $lead->description }}</p>
                    <a href="{{ route('leads.edit', ['lead' => $lead->id]) }}" class="card-link">Edit</a>

                    <a href="#" class="card-link" onclick="event.preventDefault();
                                                     document.getElementById('leadDeleteFrom').submit();">Delete</a>
                    <form id="leadDeleteFrom" action="{{ route('leads.delete', ['lead' => $lead->id]) }}" style="display: none;">
                        @csrf
                        <button type="submit" class="card-link">Delete</button>
                    </form>
                </div>
            </div>
        </div>

        <div class="col-md-9">
            <div class="card">
                <div class="card-body">
                    <h5 class="card-title">History</h5>
                    <table class="table table-striped table-bordered">
                        <tr>
                            <th>Date</th>
                            <th>Subject</th>
                            <th>Description</th>
                            <th>Duration</th>
                            <th>Feedback</th>
                            <th>Call by</th>
                        </tr>
                        @foreach($lead->calls as $call)
                            <tr>
                                <td>{{ $call->created_at->format('d/m/Y h:i:s A') }}</td>
                                <td>{{ $call->subject }}</td>
                                <td>{{ $call->description }}</td>
                                <td>{{ $call->duration }}</td>
                                <td>{{ $call->feedback }}</td>
                                <td>{{ $call->owner->name }}</td>
                            </tr>
                        @endforeach
                    </table>
                </div>
            </div>
        </div>

    </div>
</div>

<a href="{{ route('leads.create') }}" id="fab">+</a>

@endsection