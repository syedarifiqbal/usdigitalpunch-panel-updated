@extends('sales.layouts.app')

@section('content')

<div class="container">
    <div class="row">
        <div class="col-md-12">
            @if(!count($leads))
                <div class="card">
                    <div class="card-body text-center">
                        <h5 class="card-title">My Calling List</h5>
                        <h5 class="card-subtitle mb-2 text-muted">No records found!</h5>
                    </div>
                </div>
            @else

            <div class="card">
                <div class="card-header">Leads</div>

                <div class="card-body">

                    <table class="table table-bordered table-striped">
                        <thead>
                            <tr>
                                <th>Customer no.</th>
                                <th>Name</th>
                                <th>Company</th>
                                <th>Phone</th>
                                <th>Email</th>
                                @empty($isCallingList)
                                    <th>Last Call on</th>
                                    <th>Last Feedback</th>
                                    <th>Total Calls</th>
                                @endempty
                                <th>Actions</th>
                            </tr>
                        </thead>
                        <tbody>
                        @foreach($leads as $lead)
                            <tr>
                                <td>{{ $lead->id }}</td>
                                <td>{{ $lead->name }}</td>
                                <td>{{ $lead->company }}</td>
                                <td>{{ $lead->phone }}</td>
                                <td>{{ $lead->email }}</td>
                                @empty($isCallingList)
                                    <td>{{ optional($lead->last_call_at)->format('d/m/Y h:i:s A') }}</td>
                                    <td>{{ $lead->last_feedback }}</td>
                                    <td>{{ $lead->calls_count }}</td>
                                    <td>
                                        <div class="dropdown">
                                            <button class="btn dropdown-toggle" type="button" id="dropdownMenuButton" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                                <i class="fa fa-ellipsis-v"></i>
                                            </button>
                                            <div class="dropdown-menu" aria-labelledby="dropdownMenuButton">
                                                <a class="dropdown-item" href="{{ route('leads.edit', ['lead' => $lead->id]) }}">Edit</a>
                                                <form action="{{ route('leads.delete', ['lead' => $lead->id]) }}" method="post">
                                                    @method('delete')
                                                    @csrf
                                                    <button type="submit" class="dropdown-item">Delete</button>
                                                </form>
                                                <a class="dropdown-item" href="{{ route('leads.calls.create', ['lead' => $lead->id]) }}">Schedule Call</a>
                                                <form action="{{ route('leads.move.to.customer', ['lead' => $lead->id]) }}" method="post">
                                                    @csrf
                                                    <button type="submit" class="dropdown-item">Move to Customer</button>
                                                </form>
                                                <a href="{{ route('leads.show', ['lead' => $lead->id]) }}" class="dropdown-item">History</a>
                                            </div>
                                        </div>
                                    </td>
                                @endempty
                                @if(isset($isCallingList))
                                    <td>
                                        <a href="{{ route('leads.calls.create', ['lead' => $lead->id]) }}"
                                           class="fa fa-phone fa-2x"></a>
                                    </td>
                                @endif
                            </tr>
                        @endforeach
                        </tbody>
                    </table>
                </div>
            </div>

            @endif

        </div>
    </div>
</div>

<a href="{{ route('leads.create') }}" id="fab">+</a>

@endsection