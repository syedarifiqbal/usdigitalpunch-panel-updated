@extends('sales.layouts.app')

@section('styles')
    <link href="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.6-rc.0/css/select2.min.css" rel="stylesheet" />
@endsection

@section('content')

<div class="container">
    <div class="row">
        <div class="col-md-12">
            <div class="card">
                <div class="card-header">
                    @if($call->id)
                        Update Call
                    @else
                        Add new Call
                    @endif
                </div>

                <div class="card-body">

                    <form method="POST" action="{{ $call->id? route('leads.calls.update', ['call' => $call->id, 'lead' => $lead->id]) :route('leads.calls.store', ['lead' => $lead->id]) }}">
                        @csrf
                        @if($call->id)
                            @method('put')
                        @endif

                        <div class="form-group row">
                            <label for="subject" class="col-md-4 col-form-label text-md-right">Subject</label>

                            <div class="col-md-6">
                                <input id="subject" type="text" class="form-control{{ $errors->has('subject') ? ' is-invalid' : '' }}"
                                       name="subject" value="{{ old('subject', $call->subject) }}" required autofocus>

                                @if ($errors->has('subject'))
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $errors->first('subject') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>

                        <div class="form-group row">
                            <label for="lead_id" class="col-md-4 col-form-label text-md-right">Lead</label>
                            <div class="col-md-6">
                                <select id="lead_id" class="selectApi form-control{{ $errors->has('lead_id') ? ' is-invalid' : '' }}" name="lead_id" required
                                data-api-endpoint="{{ route('select.leads') }}">
                                    <option value="{{ $lead->id }}">{{ $lead->name }}</option>
                                </select>
                                @if ($errors->has('lead_id'))
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $errors->first('lead_id') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>

                        <div class="form-group row">
                            <label for="duration" class="col-md-4 col-form-label text-md-right">Call Duration</label>

                            <div class="col-md-6">
                                <input id="duration" type="number" class="form-control{{ $errors->has('duration') ? ' is-invalid' : '' }}" name="duration" value="{{ old('duration', $call->duration) }}" required>

                                @if ($errors->has('duration'))
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $errors->first('duration') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>

                        <div class="form-group row">
                            <label for="feedback" class="col-md-4 col-form-label text-md-right">Feedback</label>

                            <div class="col-md-6">
                                <input id="feedback" type="tel" class="form-control{{ $errors->has('feedback') ? ' is-invalid' : '' }}" name="feedback" value="{{ old('feedback', $call->feedback) }}">

                                @if ($errors->has('feedback'))
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $errors->first('feedback') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>

                        <div class="form-group row">
                            <label for="details" class="col-md-4 col-form-label text-md-right">Call Details</label>

                            <div class="col-md-6">
                                <textarea id="details" class="form-control{{ $errors->has('details') ? ' is-invalid' : '' }}" name="details">{{ old('details', $call->details) }}</textarea>

                                @if ($errors->has('details'))
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $errors->first('details') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>

                        <div class="form-group row mb-0">
                            <div class="col-md-6 offset-md-4">
                                <button type="submit" class="btn btn-primary">Save</button>
                                <a href="{{ route('leads.calls.index', ['lead' => $lead->id]) }}" class="btn btn-primary">Cancel</a>
                            </div>
                        </div>
                    </form>

                </div>
            </div>
        </div>
    </div>
</div>

@endsection

@section('footer_scripts')

    @include('components.select2-scripts')

@endsection