@extends('layouts.master')

@section('styles')

<!-- <link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet"> -->
<link rel="stylesheet" href="http://cdn.materialdesignicons.com/2.4.85/css/materialdesignicons.min.css">

<link href="{{ asset('css/customize-datatable.css') }}" rel="stylesheet">

@endsection

@section('content')

<div id="admin" class="col s12">
    <div class="card material-table">
        <div class="table-header">
            <span class="table-title">Clients Lists</span>
            <div class="actions">
                <input type="hidden" id="datatable-url" value="{{ route('dt.clients', request('status',null) ? ['status'=>request('status',null)]:null) }}">
                <a href="{{ route('clients.create') }}" class="modal-trigger waves-effect btn-flat nopadding"><i class="mdi mdi-plus"></i></a>
                <a href="#" class="table-search-toggle waves-effect btn-flat nopadding"><i class="mdi mdi-magnify"></i></a>
            </div>
        </div>
        <table id="datatable">
            <thead>
                <tr>
                    <th>Name</th>
                    <th>Email</th>
                    <th>Phone</th>
                    <th>Contact</th>
                    <th>Address</th>
                    <th>Status</th>
                    <th>Action</th>
                </tr>
            </thead>
            <tbody>
            
            </tbody>
        </table>
    </div>
</div>

<a id="fab" href="{{ route('clients.create') }}" class="btn btn-icon waves-effect waves-light btn-primary"><i class="mdi mdi-plus"></i></a>

@endsection


@section('footer_scripts')

<script type="text/javascript" src="{{ asset('assets/pages/elements.js') }}"></script>

@include('components.datatable-scripts');

@endsection