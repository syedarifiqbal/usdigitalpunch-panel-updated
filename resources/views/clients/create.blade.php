@extends('layouts.master')

@section('styles')

<link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">

<link href="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.6-rc.0/css/select2.min.css" rel="stylesheet" />

<link href="{{ asset('css/customize-select2.css') }}" rel="stylesheet" />

@endsection

@section('content')


<form  method="POST" action="{{ $record->id ? route('clients.update', ['client' => $record->id]) : route('clients.store') }}" class="card-block">

    @csrf

    @if( $record->id )
        @method('PUT')
    @endif

    <div class="row">

        <!-- .col-lg-6 -->
        <div class="col-lg-6">

            <div class="card">

                <div class="card-header">

                    <h5 class="card-header-text">Client Information</h5>

                </div>

                <div class="card-block">

                    {{ inputField('Company Name', 'name', old('name', $record->name), $errors->first('name')) }}

                    {{ inputField('Nick Name', 'nick_name', old('nick_name', $record->nick_name), $errors->first('nick_name')) }}

                    <?php

                    if( $record->country ){
                        $country = $record->country->name;
                    }
                    elseif( old('country_id') ){
                        $country = optional(App\Models\Country::find(old('country_id', 0)))->name;
                    }
                    else{
                        $country = '';
                    }

                    ?>

                    {{ selectField('Country Name', 'country_id', [], old('country_id', $record->country_id), $errors->first('country_id'), ['endpoint' => route('select.country'), 'select2Defaults' => $country ]) }}
                    
                    {{ inputField('Email', 'email', old('email', $record->email), $errors->first('email'), [ 'type'=>'email']) }}

                    {{ inputField('Phone', 'phone', old('phone', $record->phone), $errors->first('phone')) }}

                    {{ inputField('price', 'price', old('price', $record->price)), $errors->first('price') }}

                    <?php

                    if( $record->owner ){
                        $owner = $record->owner->name;
                    }
                    elseif( old('referred_by') ){
                        $owner = optional(\App\User::find(old('referred_by', 0)))->name;
                    }
                    else{
                        $owner = '';
                    }

                    ?>

                    {{ selectField('Referred By', 'referred_by', [], old('referred_by', $record->referred_by), $errors->first('referred_by'), ['endpoint' => route('select.user'), 'select2Defaults' => $owner ]) }}
                    
                    {{ textField('Address:', 'address', old('address', $record->address), $errors->first('address')) }}

                    {{ textField('Customer Instructions:', 'instructions', old('instructions', $record->instructions), $errors->first('instructions')) }}

                    {{ textField('Secret Instructions:', 'secret_instructions', old('secret_instructions', $record->secret_instructions), $errors->first('secret_instructions')) }}

                    {{ selectField('Status', 'is_active', [1 => 'Active', 0 => 'On Hold'], old('is_active', $record->is_active)) }}

{{--                    {{ checkboxField( "On Hold", 'on_hold', 1, old('on_hold', $record->on_hold), $errors->first('on_hold'), ['display_errors' => false]) }}--}}

{{--                    <br><br>--}}

                    <div class="form-group">
                        <input type="submit" value="Save" class="btn btn-primary">
                    </div>

                </div>
                
            </div>

        </div>
        <!-- .col-lg-6 -->

        <!-- .col-lg-6 -->
        <div class="col-lg-6">

            <div class="card">

                <div class="card-header">

                    <h5 class="card-header-text">Client Contact Details</h5>

                    <div class="f-right">
                        <a href="#" id="addContact">
                            <i class="icofont icofont-plus"></i>
                        </a>
                        <a href="#" id="deleteContact">
                            <i class="icofont icofont-trash"></i>
                        </a>
                    </div>
                    
                </div>

                <div class="card-block">
                    
                    <div class="col-sm-12 table-responsive">

                        <table class="table" id="contactForm">        
                            
                            <thead>
                            
                                <tr>
                                
                                    <th>Person Name</th>
                                    <th>Email</th>
                                    <th>Phone</th>

                                </tr>

                            </thead>

                            <tbody>

                                @if(old('contacts'))

                                @foreach(old('contacts') as $contact)
                                <tr>
                                
                                    <td>
                                        {{ inputField('', "contacts[$loop->index][name]", $contact['name'], $errors->first('contacts.'.$loop->index.'.name')) }}
                                    </td>
                                    <td>
                                        {{ inputField('', "contacts[$loop->index][email]", $contact['email'], $errors->first('contacts.'.$loop->index.'.email')) }}
                                    </td>
                                    <td>
                                        {{ inputField('', "contacts[$loop->index][phone]", $contact['phone'], $errors->first('contacts.'.$loop->index.'.phone')) }}
                                    </td>
                                
                                </tr>

                                @endforeach

                                @elseif($record->contacts)

                                    @foreach($record->contacts as $contact)

                                        <tr>

                                            <td>
                                                {{ inputField('', 'contacts[0][name]', $contact->name) }}
                                            </td>
                                            <td>
                                                {{ inputField('', 'contacts[0][email]', $contact->email) }}
                                            </td>
                                            <td>
                                                {{ inputField('', 'contacts[0][phone]', $contact->phone) }}
                                            </td>

                                        </tr>

                                    @endforeach

                                @else

                                <tr>

                                    <td>
                                        {{ inputField('', 'contacts[0][name]', '') }}
                                    </td>
                                    <td>
                                        {{ inputField('', 'contacts[0][email]', '') }}
                                    </td>
                                    <td>
                                        {{ inputField('', 'contacts[0][phone]', '') }}
                                    </td>

                                </tr>


                                @endif
                            
                            </tbody>
                        
                        </table>

                    </div>

                </div>
                
            </div>

        </div>
        <!-- .col-lg-6 -->

    </div>
    <!-- .row -->

</form>

@endsection


@section('footer_scripts')

@include('components.select2-scripts')

<script src="{{ asset('assets/pages/elements.js') }}"></script>
<!--<script src="{{ asset('js/contact-table-form.js') }}"></script>-->

<script>
    $( '#addContact' ).on('click', function (e) {
    
    e.preventDefault();

    var tr = '<tr>' +
            '<td>' +
                '<div class="md-input-wrapper">' +
                    '<input type="text" name="contacts[' + $('#contactForm tbody tr').size() + '][name]" class="md-valid md-form-control" value="">' +
                    // '<label></label>'
                '<span class="md-line"></span></div>' +
            '</td>' +
            
            '<td>' +
                '<div class="md-input-wrapper">' +
                    '<input type="text" name="contacts[' + $('#contactForm tbody tr').size() + '][email]" class="md-valid md-form-control" value="">' +
                    // '<label></label>'
                    '<span class="md-line"></span></div>' +
            '</td>' +
            
            '<td>' +
                '<div class="md-input-wrapper">' +
                    '<input type="text" name="contacts[' + $('#contactForm tbody tr').size() + '][phone]" class="md-valid md-form-control" value="">' +
                    // '<label></label>'
                    '<span class="md-line"></span></div>' +
            '</td>' +
        '</tr>';
// console.log(tr);
    $('#contactForm tbody').append(tr);

});
</script>

@endsection