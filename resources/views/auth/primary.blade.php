<div class="card">
    
    <div class="card-header d-flex align-items-center">
    
        <h4>Primary Informations</h4>
    
    </div>

    <div class="card-block">

        {{ inputField('Full name', 'name', old('name', $user->name), $errors->first('name')) }}

        {{ inputField('Email', 'email', old('email', $user->email), $errors->first('email'), ['type'=>'email'] ) }}

{{--        {{ inputField('Password', 'password', old('password'), $errors->first('password'), ['type'=>'password'] ) }}--}}

{{--        {{ inputField('Confirm Password', 'password_confirmation', old('password_confirmation'), $errors->first('password_confirmation'), ['type'=>'password'] ) }}--}}
        
        {{ inputField('Father name', 'father_name', old('father_name', $user->father_name), $errors->first('father_name')) }}
        
        {{ inputField('CNIC #', 'cnic', old('cnic', $user->cnic), $errors->first('cnic')) }}
        
        {{ inputField('Date of Birth', 'dob', old('dob', $user->dob), $errors->first('dob'), [ 'class'=>'date']) }}

        {{ selectField('Gender', 'gender', ['Male' => 'Male', 'Female' => 'Female', 'Other' => 'Other'], old('gender', $user->gender), $errors->first('gender')) }}

        {{ inputField('Phone', 'phone', old('phone', $user->phone), $errors->first('phone')) }}

        {{ textField('Address', 'address', old('address', $user->address), $errors->first('address')) }}
        
    </div>
</div>