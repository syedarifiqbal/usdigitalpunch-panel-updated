<div class="card">

    <div class="card-header d-flex align-items-center"><h4>Employee Details</h4></div>

    <div class="card-block">

        <div class="row">

            <div class="col-lg-6">

                {{ selectField('Gender', 'roles_id[]', \App\Models\Role::all()->pluck('name', 'id'), old('roles_id', $user->roles()->pluck('role_id')->toArray()), $errors->first('roles_id'), ['multiple'=>true, 'first_blank' => false]) }}

                {{ inputField('Bonus Rate', 'bonus', old('bonus', $user->bonus), $errors->first('bonus')) }}

                @if( ! $user->id )

                {{ inputField('Password', 'password', '', $errors->first('password'), ['type' => 'password']) }}

                {{ inputField('Password Confirmation', 'password_confirmation', '', $errors->first('password_confirmation'), ['type' => 'password']) }}

                @endif

            </div>
            <!-- .col-lg-6 -->
            

            <div class="col-lg-6">
                
                {{ inputField('Approval Bonus', 'approval_bonus', old('approval_bonus', $user->approval_bonus), $errors->first('approval_bonus')) }}

                {{ inputField('Revision Bonus', 'revision_bonus', old('revision_bonus', $user->revision_bonus), $errors->first('revision_bonus')) }}

                {{ inputField('Edit Bonus', 'edit_bonus', old('edit_bonus', $user->edit_bonus), $errors->first('edit_bonus')) }}

                {{ inputField('Salewrap Commission', 'sale_wrap_commision', old('sale_wrap_commision', $user->sale_wrap_commision), $errors->first('sale_wrap_commision')) }}
                
            </div>
            <!-- .col-lg-6 -->

            <div class="col-lg-12">
                
                <div class="form-group">
                    <input type="submit" value="Save" class="btn btn-primary">
                </div>
                
            </div>

        </div>
        <!-- .row -->
        
    </div>

</div>