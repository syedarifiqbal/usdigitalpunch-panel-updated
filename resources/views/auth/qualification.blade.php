<div class="card">
    
    <div class="card-header d-flex align-items-center">
    
        <h4>Qualification Information</h4>
    
    </div>

    <div class="card-block">    
        
        {{ inputField('Last Degree/Education', 'education', old('education', $user->education), $errors->first('education')) }}

        {{ inputField('Institute', 'institute', old('institute', $user->institute), $errors->first('institute')) }}

        {{ inputField('Passing Year', 'passing_year', old('passing_year', $user->passing_year), $errors->first('passing_year')) }}

    </div>
</div>