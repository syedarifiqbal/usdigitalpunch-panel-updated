<div class="card">

    <div class="card-header d-flex align-items-center"> <h4>Contact for emergency cases</h4> </div>

    <div class="card-block">

        {{ inputField('Person Name', 'contact_person', old('contact_person', $user->contact_person), $errors->first('contact_person')) }}

        {{ inputField('Relationship', 'contact_person_relation', old('contact_person_relation', $user->contact_person_relation), $errors->first('contact_person_relation')) }}

        {{ inputField('Phone number', 'contact_person_phone', old('contact_person_phone', $user->contact_person_phone), $errors->first('contact_person_phone')) }}

        {{ inputField('Address', 'contact_person_address', old('contact_person_address', $user->contact_person_address), $errors->first('contact_person_address')) }}

    </div>
</div>