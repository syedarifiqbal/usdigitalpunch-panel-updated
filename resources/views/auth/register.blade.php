@extends('layouts.app')

@section('content')

      
<form method="POST" action="{{ route('register') }}">

    @csrf
    
    <section class="forms">
    
            <!-- Page Header-->
            <header><h1 class="h3 display">Register new User</h1></header>

            <div class="row">

                <!-- .col-lg-6 -->
                <div class="col-lg-6">
                    @include('auth.primary')
                </div>
                <!-- .col-lg-6 -->

                <!-- .col-lg-6 -->
                <div class="col-lg-6">
                    @include('auth.emergency')
                    @include('auth.qualification')
                </div>
                <!-- .col-lg-6 -->

                <!-- .col-lg-6 -->
                <div class="col-lg-12">
                    @include('auth.bonus_salary')
                </div>
                <!-- .col-lg-6 -->
                

            </div>
            <!-- .row -->

    </section>

</form>

@endsection


@section('footer_scripts')

<script src="{{ asset('vendor/bootstrap-datepicker/js/bootstrap-datepicker.min.js') }}"></script>

<script>

$('.date').datepicker({
    clearBtn: true,
    todayHighlight: true
});

</script>

@endsection