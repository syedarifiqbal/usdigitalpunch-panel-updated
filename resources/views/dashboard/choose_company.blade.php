@extends('layouts.master')

@section('content')

    <div class="card">

        <div class="card-header">

            <h5 class="card-header-text">Choose Company</h5>
            
        </div>

        <form  method="POST" action="{{ route('choose.company') }}" class="card-block">
        
            @csrf

            {{ selectField('Choose Company', 'company_id', $companies, old('company_id'), $errors->first('company_id')) }}

            <div class="form-group">
                <input type="submit" value="Save" class="btn btn-primary">
            </div>

        </form>

    </div>


@endsection


@section('footer_scripts')

<script src="{{ asset('assets/plugins/moment/moment.js') }}"></script>
<script src="{{ asset('assets/plugins/bootstrap-datepicker/dist/js/bootstrap-datepicker.min.js') }}"></script>
<script src="{{ asset('assets/plugins/bootstrap-datepicker/js/bootstrap-datetimepicker.min.js') }}"></script>

<script src="{{ asset('assets/plugins/datepicker/js/moment-with-locales.min.js') }}"></script>
<script src="{{ asset('assets/plugins/bootstrap-material-datetimepicker/js/bootstrap-material-datetimepicker.js') }}"></script>
<script src="{{ asset('assets/pages/elements.js') }}"></script>

@include('components.select2-scripts')

<script>

$(function () {

    $('.date').bootstrapMaterialDatePicker({
        time: false,
        clearButton: true,
        format: 'DD/MM/YYYY'
    });

})

</script>

@endsection