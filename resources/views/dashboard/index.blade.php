@extends('layouts.master')

@section('content')

    @can('view-order-summary')
        @include('components.pending_tasks_summary')
    @endcan

    @can('view-user-progress-task')
        @include('components.user_assigned_tasks')
    @endcan

    {{--@can('view-today-tasks-list')
        @include('components.today_tasks')
    @endcan--}}

    @if(!in_array('Super Admin', request('roles')) || in_array('Admin', request('roles')))
        @include('components.pending_tasks', ['tasks' => auth()->user()->tasks()->with('client')->where('status', 'pending')->get()])
    @else
        
    @endif

    @can('pending-orders')
    <?php
    // $date = new \DateTime();
    // $date->modify('-12 hours');
    // $formatted_date = $date->format('Y-m-d H:i:s');
    // $ptasks = App\Models\Task::with('client')->where('status', 'pending')->where('created_at', '<=', $formatted_date)->get();
    $ptasks = App\Models\Task::with('client')->where('status', 'pending')->get();
        ?>
        @include('components.pending_tasks', ['tasks' => $ptasks])
    @endcan

    @if($sales_wrap_orders)
        @include('components.sales_wrap_pending_tasks_list', ['tasks' => $sales_wrap_orders])
    @endif

@endsection

@section('header_styles')
<link rel="stylesheet" href="{{ asset('vendor/bootstrap-datepicker/css/bootstrap-datepicker.min.css') }}">
@endsection

@section('footer_scripts')

<!-- Sparkline charts -->
<script src="{{ asset('assets/plugins/jquery-sparkline/dist/jquery.sparkline.js') }}"></script>

<!-- Rickshaw Chart js -->
<!-- <script src="{{ asset('assets/plugins/d3/d3.js') }}"></script>
<script src="{{ asset('assets/plugins/rickshaw/rickshaw.js') }}"></script> -->

<!-- Counter js  -->
<script src="{{ asset('assets/plugins/waypoints/jquery.waypoints.min.js') }}"></script>

<script src="{{ asset('assets/plugins/countdown/js/jquery.counterup.js') }}"></script>

<!-- <script type="text/javascript" src="{{ asset('assets/pages/dashboard.js') }}"></script> -->

<script type="text/javascript" src="{{ asset('assets/pages/elements.js') }}"></script>

<!-- <script src="{{ asset('vendor/bootstrap-datepicker/js/bootstrap-datepicker.min.js') }}"></script> -->

<script>

$('.date').datepicker({
    clearBtn: true,
    todayHighlight: true
});

</script>
@endsection