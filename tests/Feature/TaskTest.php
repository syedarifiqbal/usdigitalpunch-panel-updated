<?php

namespace Tests\Feature;

use Tests\TestCase;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Http\UploadedFile;
use Illuminate\Foundation\Testing\WithoutMiddleware;
use Illuminate\Support\Facades\Storage;

// use Illuminate\Foundation\Testing\DatabaseMigrations;

class TaskTest extends TestCase
{
    use RefreshDatabase;
    
    /**
     * A basic test example.
     *
     * @return void
     */
    
    /** @test */
    public function a_authanticated_user_can_insert_task()
    {
        $user = $this->signedIn('add-task');

        $designer = create('App\User');

        $task = make('App\Models\Task', ['designer_id' => $designer->id]);

        $taskData = $task->toArray();
        $taskData['date'] = $task->date->format('d/m/Y');
        $taskData['delivery_date'] = $task->delivery_date->format('d/m/Y');
        $taskData['uploads'] = [
            UploadedFile::fake()->image('test1.jpg'),
            UploadedFile::fake()->image('test2.jpg')
        ];

        $response = $this->withSession(['company_id' => '1'])
                            ->withoutExceptionHandling()
                            ->post(route('tasks.store'), $taskData);
                            
        $response->assertRedirect(route('tasks'));

        foreach(\App\Models\Task::first()->attachments as $file)
        {
            unlink(storage_path('app/'.$file->path));
        }

    }
    
    /** @test */
    public function a_unauthanticated_user_can_not_insert_task()
    {
        $this->signedIn();

        $response = $this->post(route('tasks.store'), []);
                            
        $response->assertStatus(403);

    }
    
    /** @test */
    public function a_task_should_generate_user_bonus_based_on_order_type()
    {
        $user = $this->signedIn(['add-task','send-task']);

        $designer = create('App\User');
        $designer->bonus = 50;
        $designer->revision_bonus = 12;
        $designer->edit_bonus = 11;
        $designer->approval_bonus = 10;
        $designer->save();

        $task = make('App\Models\Task', ['designer_id' => $designer->id]);

        $taskData = $task->toArray();
        $taskData['date'] = $task->date->format('d/m/Y');
        $taskData['delivery_date'] = $task->delivery_date->format('d/m/Y');
        $taskData['uploads'] = [
            UploadedFile::fake()->image('test1.jpg'),
            UploadedFile::fake()->image('test2.jpg')
        ];

        // dd(\App\Models\Task::first());
 
        $response = $this->withSession(['company_id' => '1'])
                            // ->withoutExceptionHandling()
                            ->post(route('tasks.store'), $taskData);
                            
        $response->assertRedirect(route('tasks'));
        
        // Get first Task and submit
        $firstTask = \App\Models\Task::first();
        $firstTask->status = 'rfs';
        $firstTask->order_type = 'New';
        $firstTask->save();

        $sendData = $firstTask->toArray();
        $sendData['files'] = $firstTask->attachments->pluck('id')->toArray();
        $sendData['date'] = $task->date->format('d/m/Y');
        $sendData['delivery_date'] = $task->delivery_date->format('d/m/Y');
        
        // dd(\App\Models\Task::first());

        $this
            ->withSession(['company_id' => '1'])
            ->withoutExceptionHandling()
            ->post(route('tasks.review.store', ['task', $firstTask->id]), $sendData);
        
        $bonus = \App\Models\Bonus::where('user_id', $designer->id)
            ->where('task_id', $firstTask->id)->first();
            
        dd(\App\Models\Bonus::all(), $firstTask->id);
        
        foreach($firstTask->attachments as $file)
        {
            unlink(storage_path('app/'.$file->path));
        }

    }
}
