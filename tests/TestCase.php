<?php

namespace Tests;

use Illuminate\Foundation\Testing\TestCase as BaseTestCase;

abstract class TestCase extends BaseTestCase
{
    use CreatesApplication;

    public function signedIn($permissions = null, $user = null)
    {
        $role = create('App\Models\Role');

        $user = $user ?: create('App\User');

        $roleUser = create('App\Models\RoleUser', ['role_id' => $role->id, 'user_id' => $user->id]);
        
        $this->registerGate($permissions, $role);

        $this->actingAs($user);

        return $user;
    }

    protected function registerGate($permissions, $role)
    {
        if(!$permissions){ return; }

        $permissions = is_array($permissions)? $permissions:[$permissions];

        foreach ($permissions as $permission) 
        {
            $perm = create('App\Models\Permission', ['name' => $permission]);
            
            $role_permissions = create('App\Models\PermissionRole', ['role_id' => $role->id, 'permission_id' => $perm->id]);
            
            \Gate::define($perm->name, function ($user) use ($role) {
                return $user->hasRole($role->name);
            });
        }

    }
}
