<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

// dd(App::environment());

Auth::routes();

Route::get('/test', function(){
    // return config('database.connections.mysql.database');
});

Route::get('/switch-account', function(){
    // 6 nabeel
    // 2 faheem
    // auth()->loginUsingId(6);
    return redirect('/');
    // return config('database.connections.mysql.database');
});

Route::group(['middleware'=> 'auth'], function (){

    Route::name('choose.company')->get('/choose-company', 'HomeController@choose_company');

    Route::name('choose.company')->post('/choose-company', 'HomeController@choose_company');
});

Route::group(['middleware'=> ['auth', 'check_company']], function ()
{
    Route::get('/home', 'HomeController@index')->name('home');

    Route::get('/', 'HomeController@index');

    Route::group(['prefix'=> 'users'], function(){

        Route::name('users')->get('/', 'UserController@index');

        Route::name('profile')->get('/profile/{from?}/{to?}', 'UserController@profile');

        Route::name('post.profile')->post('/profile', 'UserController@profile_store');

        Route::name('edit.users')->get('/{user}/edit', 'UserController@edit');

        Route::name('users.change.password')->get('/{user}/change-password', 'ChangePasswordController@form');

        Route::name('users.change.password.post')->post('/{user}/change-password', 'ChangePasswordController@store');

        Route::name('udpate.users')->put('/{user}', 'UserController@update');

        Route::name('delete.users')->get('/{user}/delete', 'UserController@distroy');

        Route::name('roles')->get('/roles', 'RolesController@index');

        Route::name('roles.create')->get('/roles/create', 'RolesController@create');

        Route::name('roles.store')->post('/roles', 'RolesController@store');

        Route::name('roles.edit')->get('/roles/{role}/edit', 'RolesController@edit');

        Route::name('roles.update')->put('/roles/{role}', 'RolesController@update');

        Route::name('roles.delete')->delete('/roles/{role}', 'RolesController@destroy');

        Route::group(['prefix'=> 'permissions'], function(){

            Route::name('permissions')->get('/', 'PermissionsController@index');

            Route::name('permissions.store')->post('/', 'PermissionsController@store');

            Route::name('permissions.create')->get('/create', 'PermissionsController@create');

            Route::name('permissions.edit')->get('/{permission}/edit', 'PermissionsController@edit');

            Route::name('permissions.update')->put('/{permission}', 'PermissionsController@update');

            Route::name('permissions.delete')->get('/{permission}/delete', 'PermissionsController@destroy');

        });

    });

    Route::group(['prefix'=> 'tasks'], function(){

        Route::name('tasks')->get('/', 'TasksController@index');

        // Route::name('tasks.rfs')->get('/rfs', 'TasksController@rfs');

        Route::name('tasks.create')->get('/create', 'TasksController@create');

        Route::name('tasks.store')->post('/', 'TasksController@store');

        Route::name('tasks.show')->get('/{task}/show', 'TasksController@show');

        Route::name('tasks.revise')->get('/{task}/revise', 'TasksController@revise');

        Route::name('tasks.revise.store')->post('/{task}/revise', 'TasksController@reviseStore');

        Route::name('tasks.changeOrderType')->get('/{task}/change-order-type', 'TasksController@changeOrderType');

        Route::name('tasks.changeOrderType.store')->post('/{task}/change-order-type', 'TasksController@changeOrderTypeStore');

        Route::name('tasks.addMoreFiles')->get('/{task}/add-files', 'TasksController@addMoreFilesForm');

        Route::name('tasks.file.delete')->delete('/{task}/{file}/delete', 'TasksController@deleteSubmittedFile');

        Route::name('tasks.review')->get('/{task}/review', 'TasksController@review');

        Route::name('tasks.review.store')->post('/{task}/review', 'TasksController@reviewStore');

        Route::name('tasks.assign')->get('/{task}/assign', 'TasksController@assign');

        Route::name('tasks.store.assign')->post('/{task}/assign', 'TasksController@storeAssign');

        Route::name('tasks.edit')->get('/{task}/edit', 'TasksController@edit');

        Route::name('tasks.delete')->get('/{task}/delete', 'TasksController@destroy');

        Route::name('tasks.submit')->get('/{task}/{submit?}', 'TasksController@edit');

        Route::name('tasks.update')->put('/{task}/{submit?}', 'TasksController@update');

    });

    Route::group(['prefix'=> 'clients'], function(){

        Route::name('clients')->get('/', 'ClientsController@index');

        Route::name('clients.create')->get('/create', 'ClientsController@create');

        Route::name('clients.store')->post('/', 'ClientsController@store');

        Route::name('clients.show')->get('/{client}/show', 'ClientsController@show');

        Route::name('clients.edit')->get('/{client}/edit', 'ClientsController@edit');

        Route::name('clients.update')->put('/{client}', 'ClientsController@update');

        Route::name('clients.delete')->get('/{client}/delete', 'ClientsController@destroy');

    });

    Route::group(['prefix'=> 'reports', 'namespace' => 'Reports'], function(){

        Route::name('bonus-test')->get('/bonus-test', 'BonusController@temp_bonus');
        Route::name('bonus')->get('/bonus', 'BonusController@index');
        Route::name('bonus.pdf')->get('/pdf/bonus', 'BonusController@pdf');
        Route::name('reports.today_task')->get('/today-task', 'ReportController@today_tasks');
        Route::name('reports.orders.invoice.excel')->get('/orders-invoices/excel/{client}/{from_date?}/{end_date?}/{design_type?}', 'InvoiceController@excel');
        Route::name('reports.orders.invoice.pdf')->get('/orders-invoices/pdf/{client}/{from_date?}/{end_date?}/{design_type?}', 'InvoiceController@pdf');
        Route::name('reports.orders.invoice')->get('/orders-invoices/{start_date?}/{end_date?}/{design_type?}', 'InvoiceController@form');
        Route::name('reports.orders.sales')->get('/sales-bonus/{start_date?}/{end_date?}', 'SalesBonusController@index');
        Route::name('reports.orders.sales')->post('/sales-bonus/{start_date?}/{end_date?}', 'SalesBonusController@pdf');
        Route::name('reports.sales.customers.orders')->get('/sales-customers', 'SalesCustomersController@index');
        Route::name('reports.sales.customers.orders')->post('/sales-customers', 'SalesCustomersController@pdf');

    });

    Route::name('settings')->get('/settings', 'SettingsController@index');

    Route::name('settings.post')->post('/settings/{tab}', 'SettingsController@store');

    Route::group(['prefix'=> 'select'], function()
    {
        Route::name('select.country')->post('/countries', 'Api\Select2Controller@countries');

        Route::name('select.client')->post('/clients', 'Api\Select2Controller@clients');

        Route::name('select.user')->post('/users', 'Api\Select2Controller@users');
    });

    Route::get('/notifications', 'NotificationsController@index')->name('notifications');

});

Route::group(['prefix'=>'datatables'], function ()
{
    Route::name('dt.users')->get('/', 'Datatables\UserTableController@getData');

    Route::name('dt.roles')->get('/roles', 'Datatables\RolesTableController@index');

    Route::name('dt.clients')->get('/clients', 'Datatables\ClientTableController@getData');

    Route::name('dt.tasks')->get('/tasks', 'Datatables\TasksTableController@getData');

});

Route::group(['middleware' => ['web', 'auth']], function () {
    Route::get('/search', 'SearchController@index')->name('route.search');
});

Route::group(['prefix' => 'laravel-filemanager', 'middleware' => ['web', 'auth']], function () {
    \UniSharp\LaravelFilemanager\Lfm::routes();
});

Route::group(['prefix' => 'seeds-generator', 'namespace' => 'Generators', 'middleware' => ['web', 'auth']], function () {
    Route::get('roles-permissions','RoleandseedControllers@index');
});


Route::get('/clear', function(){
    Artisan::call('cache:clear');
    Artisan::call('config:cache');
    Artisan::call('view:clear');
    dd(Artisan::call('storage:link'));
});

Route::get('/my-link', function(){
    dd(Artisan::call('storage:link'));
});

Route::get('/make-controller', function(){
    dd(Artisan::call('make:controller', ['name' => 'Sales/Auth/MyAuthController']));
});