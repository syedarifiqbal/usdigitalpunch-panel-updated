<?php



Route::group(['namespace' => 'Sales', 'prefix' => 'sales'],function () {

    Route::get('login', 'Auth\LoginController@login')->name('sales.auth.login');
    Route::post('login', 'Auth\LoginController@loginSales')->name('sales.auth.loginAdmin');

    Route::group(['middleware' => 'auth:sales'], function(){

        Route::get('/', 'SalesController@index')->name('sales.dashboard');
        Route::get('dashboard', 'SalesController@index')->name('sales.dashboard');
        Route::get('register', 'SalesController@create')->name('sales.register');
        Route::post('register', 'SalesController@store')->name('sales.register.store');
        Route::post('logout', 'Auth\LoginController@logout')->name('sales.auth.logout');

        // Leads
        Route::group(['prefix' => 'leads'],function () {
            Route::get('/', 'LeadsController@index')->name('leads.index');
            Route::get('/calling-lists', 'LeadsController@callingList')->name('leads.calling.list');
            Route::get('/create', 'LeadsController@create')->name('leads.create');
            Route::post('/', 'LeadsController@store')->name('leads.store');
            Route::get('/{lead}/edit', 'LeadsController@edit')->name('leads.edit');
            Route::put('/{lead}/', 'LeadsController@update')->name('leads.update');
            Route::delete('/{lead}/delete', 'LeadsController@destroy')->name('leads.delete');
            Route::post('/{lead}/move-to-customer', 'LeadsController@moveToCustomer')->name('leads.move.to.customer');
            Route::get('/{lead}/show', 'LeadsController@show')->name('leads.show');

            // Calls
            Route::get('/{lead}/calls', 'CallsController@index')->name('leads.calls.index');
            Route::get('/{lead}/calls/create', 'CallsController@create')->name('leads.calls.create');
            Route::post('/{lead}/calls', 'CallsController@store')->name('leads.calls.store');
            Route::get('/{lead}/calls/{call}/edit', 'CallsController@edit')->name('leads.calls.edit');
            Route::put('/{lead}/calls{call}/', 'CallsController@update')->name('leads.calls.update');
            Route::delete('/{lead}/calls/{call}/delete', 'CallsController@destroy')->name('leads.calls.delete');
        });
        // Customers
        Route::group(['prefix' => 'customers'],function () {
            Route::get('/', 'CustomersController@index')->name('sales.customers.index');
        });

        Route::group(['prefix'=> 'select'], function()
        {
            Route::name('select.leads')->post('/leads', 'LeadsController@select2');
        });

        Route::get('/activities', 'ActivitiesController@index')->name('activities.index');
    });
});

