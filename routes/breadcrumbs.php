<?php

// Home
Breadcrumbs::register('home', function ($breadcrumbs) {
    $breadcrumbs->push('<i class="icofont icofont-home"></i>', url('/'));
//    $breadcrumbs->push(\App\Models\Company::find(session('company_id', 1))->name, '#');
});

// Home > Users
Breadcrumbs::register('users', function ($breadcrumbs) {
    $breadcrumbs->parent('home');
    $breadcrumbs->push('Users', route('users'));
});

// Home > Users > New
Breadcrumbs::register('create.user', function ($breadcrumbs) {
    $breadcrumbs->parent('users');
    $breadcrumbs->push('Register', url('/register'));
});

// Home > Users > Roles
Breadcrumbs::register('roles', function ($breadcrumbs) {
    $breadcrumbs->parent('users');
    $breadcrumbs->push('Roles', route('roles'));
});

// Home > Users > Profile > Username
Breadcrumbs::register('profile', function ($breadcrumbs) {
    $breadcrumbs->parent('users');
    $breadcrumbs->push('Profile', route('profile'));
    $breadcrumbs->push(auth()->user()->name, route('profile'));
});

// Home > Tasks > width type
Breadcrumbs::register('tasks.with.design.type', function ($breadcrumbs) {
    $breadcrumbs->parent('home');
    $breadcrumbs->push('Tasks', route('tasks'));
    $breadcrumbs->push(request('design_type', 'Digitizing'), route('tasks'));
});

// Home > Tasks
Breadcrumbs::register('tasks', function ($breadcrumbs) {
    $breadcrumbs->parent('home');
    $breadcrumbs->push(\App\Models\Company::find(session('company_id', 1))->name, '#');
    $breadcrumbs->push('Tasks', route('tasks'));
});

// Home > Tasks > Create
Breadcrumbs::register('tasks.create', function ($breadcrumbs) {
    $breadcrumbs->parent('tasks');
    $breadcrumbs->push('Create', route('tasks.create'));
});

// Home > Tasks > Edit
Breadcrumbs::register('tasks.edit', function ($breadcrumbs, $task) {
    $breadcrumbs->parent('tasks');
    $breadcrumbs->push('Edit', route('tasks.edit', ['task'=>$task->id]));
    $breadcrumbs->push($task->name, route('tasks.edit', ['task'=>$task->id]));
});

// Home > Tasks > Submit > Task Name
Breadcrumbs::register('tasks.submit', function ($breadcrumbs, $task) {
    $breadcrumbs->parent('tasks');
    $breadcrumbs->push('Submit', route('tasks.submit', ['task'=>$task->id, 'submit' => 'submit']));
    $breadcrumbs->push($task->name, route('tasks.submit', ['task'=>$task->id, 'submit' => 'submit']));
});

// Home > Tasks > Task Name
Breadcrumbs::register('tasks.show', function ($breadcrumbs, $task) {
    $breadcrumbs->parent('tasks');
    $breadcrumbs->push($task->name, route('tasks.show', ['task'=>$task->id]));
});

// Home > Tasks > Task Name > Review
Breadcrumbs::register('tasks.review', function ($breadcrumbs, $task) {
    $breadcrumbs->parent('tasks');
    $breadcrumbs->push($task->name, route('tasks.show', ['task'=>$task->id]));
    $breadcrumbs->push("Review", route('tasks.show', ['task'=>$task->id]));
});

// Home > Clients
Breadcrumbs::register('clients', function ($breadcrumbs) {
    $breadcrumbs->parent('home');
    $breadcrumbs->push('Clients', route('clients'));
});

// Home > Clients > Create
Breadcrumbs::register('clients.create', function ($breadcrumbs) {
    $breadcrumbs->parent('clients');
    $breadcrumbs->push('Create', route('clients.create'));
});

// Home > Clients > Edit > Client Name
Breadcrumbs::register('clients.edit', function ($breadcrumbs, $client) {
    $breadcrumbs->parent('clients');
    $breadcrumbs->push('Edit', route('clients.edit', [ 'client' => $client->id ]));
    $breadcrumbs->push( $client->name, route('clients.edit', [ 'client' => $client->id ]));
});

// Home > Clients > Client Name
Breadcrumbs::register('clients.show', function ($breadcrumbs, $client) {
    $breadcrumbs->parent('clients');
    $breadcrumbs->push($client->name, route('clients.show', ['client'=>$client->id]));
});

// Home > Reports
Breadcrumbs::register('reports', function ($breadcrumbs) {
    $breadcrumbs->parent('home');
    $breadcrumbs->push('Reports', route('home'));
});

// Home > Reports > Invoice report
Breadcrumbs::register('reports.invoice', function ($breadcrumbs) {
    $breadcrumbs->parent('reports');
    $breadcrumbs->push('Invoice', route('reports.orders.invoice'));
});

// Home > Settings
Breadcrumbs::register('settings', function ($breadcrumbs) {
    $breadcrumbs->parent('home');
    $breadcrumbs->push('Settings', route('settings'));
});

// Home > Users > Roles > new
Breadcrumbs::register('new.roles', function ($breadcrumbs) {
    $breadcrumbs->parent('roles');
    $breadcrumbs->push('New', route('create.users'));
});