<?php

namespace App\Notifications;

use Illuminate\Bus\Queueable;
use Illuminate\Notifications\Notification;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Notifications\Messages\MailMessage;
use Illuminate\Notifications\Messages\BroadcastMessage;

class BonusGenerated extends Notification
{
    use Queueable;

    public $task;
    public $description;

    /**
     * Create a new notification instance.
     *
     * @return void
     */
    public function __construct($task, $description)
    {
        $this->task = $task;
        $this->description = $description;
    }
    
    public function via($notifiable)
    {
        return ['database', 'broadcast'];
    }
    
    public function toArray($notifiable)
    {
        // sprintf('Design %s assigned to you by %s', $this->task->name, auth()->user()->name)
        $data = [
            'task' => $this->task,
            'url' => route('tasks.show', ['task' => $this->task->id]),
            'description' => $this->description,
        ];

        return $data;
    }

}
