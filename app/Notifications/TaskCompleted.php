<?php

namespace App\Notifications;

use Illuminate\Bus\Queueable;
use Illuminate\Notifications\Notification;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Notifications\Messages\MailMessage;

class TaskCompleted extends Notification
{
    use Queueable;

    public $task;

    public $files;

    public function __construct($task, $files)
    {
        $this->task = $task;
        $this->files = $files;
    }
    
    public function via($notifiable)
    {
        return ['mail'];
    }

    public function toMail($notifiable)
    {
        // dd(get_class_methods(new MailMessage));

        $mail = new MailMessage();
        
        $revision_or_edit = $this->task->order_type == 'new'? : $this->task->order_type;
        
        $mail
            ->from(setting('email_from_' . session('company_id', 1), 'no-reply@ghousiaenterprises.com') )
            ->subject("Your {$this->task->design_type} $revision_or_edit Order is Ready/{$this->task->name}/OR_{$this->task->id}")
            ->view(
//                'emails.send_task', ['task' => $this->task, 'template' => setting('delivery_email_template_' . session('company_id', 1))]
                'emails.customer_email_'.session('company_id', 1), ['task' => $this->task]
            );
        $cc = setting('email_cc_' . session('company_id', 1), 'no-reply@ghousiaenterprises.com');
        if ($cc)
            $mail->cc($cc);

        if (!$this->files)
            return $mail;

        foreach($this->task->attachments()->whereIn('id', $this->files)->get() as $file)
        {
            // dd( url(\Storage::url($file->path)));
            $mail->attach( url(\Storage::url($file->path)) );
            // $mail->attach(storage_path( 'app/' . $file->path));
        }

        return $mail;
    }

    /**
     * Get the array representation of the notification.
     *
     * @param  mixed  $notifiable
     * @return array
     */
    public function toArray($notifiable)
    {
        return [
            //
        ];
    }
}
