<?php

// use Illuminate\Http\Request;
use App\Models\LogActivity;
use Illuminate\Support\Facades\Gate;

define('FLASH_INFO', 'info');
define('FLASH_SUCCESS', 'success');
define('FLASH_WARNING', 'warning');
define('FLASH_DANGER', 'danger');
define('FLASH_DARK', 'dart');
define('FLASH_NOTICE', '');

define('FLASH_NORMAL', 'normal');
define('FLASH_STICKY', 'sticky');
define('FLASH_NONBLOCK', 'nonblock');
define('FLASH_TABBED', 'tabbed');

define('STATUS_PENDING', 'Pending');
define('STATUS_RFS', 'Rfs');
define('STATUS_COMPLETE', 'Complete');
define('STATUS_IN_PROGRESS', 'in progress');


function flash($message, $level = FLASH_INFO, $type = FLASH_NORMAL)
{
    session()->flash('flash_message', $message);
    session()->flash('flash_message_level', $level);
}

/**
 * @param $description
 * @param null $logable
 * @param null $logable_id
 * @return LogActivity
 * @throws Exception
 */
function activity_log($description, $logable = null, $logable_id = null)
{
    if(!auth()->check())
        throw new Exception("User not Logged in.");

    $data = [
        'description' => $description,
        'url' => Request::fullUrl(),
        'method' => Request::method(),
        'ip' => Request::ip(),
        'agent' => Request::header('user-agent'),
        'user_id' => auth()->user()->id,
    ];
    
    $log = new LogActivity($data);
    // Auth::logedin
    auth()->user()->logs()->save($log);
    return $log;
}

function selectedCompany($value = false)
{
    if ($value) {
        return session('company_id') == $value;
    }
    return session('company_id');
}

/**
 * @param $permission
 * @param string $name
 * @return \Illuminate\Http\RedirectResponse|null
 */
function allowedOrRedirect($permission, $name='home')
{
    if(Gate::denies($permission))
    {
        // flash('You are not permitted to view this page', FLASH_DANGER);
        // return redirect()->route($name);
    }
    abort_if(Gate::denies($permission), 403, 'You are not permitted for this action.');
}

function params($params)
{
    if(!is_array($params))
        return $params;

    return array_merge(request()->all(), $params);
}

function readNotification()
{
    if( $ref = request('ref'))
    {
        event(new \App\Events\NotificationHasRead(auth()->id()));
    }
}

function activeMenu($menu, $name, $condition = true)
{
    if ($menu === $name && $condition)
    {
        return ' active';
    }
    return '';
}

function prepareCountriesJob($taskCollection)
{
    $data = [];
    foreach ($taskCollection as $task)
    {
        // dd($taskCollection->toArray());
        if (!isset($data[$task->client->country->name]))
        {
            $data[$task->client->country->name] = [
                'digitizing_new' => 0, 'digitizing_revision' => 0, 'digitizing_edit' => 0,
                'vector_new' => 0, 'vector_revision' => 0, 'vector_edit' => 0,
                'quotation_new' => 0, 'quotation_revision' => 0, 'quotation_edit' => 0,
                'digitizing_pending' => 0, 'vector_pending' => 0, 'quotation_pending' => 0,
                'digitizing_rfs' => 0, 'vector_rfs' => 0, 'quotation_rfs' => 0,
                'digitizing_unsigned' => 0, 'vector_unsigned' => 0, 'quotation_unsigned' => 0
            ];
        }
        $data[$task->client->country->name][strtolower($task->design_type.'_'.$task->order_type)] += 1;
        $data[$task->client->country->name]['flag'] = $task->client->country->flag;
        if ($task->status == 'pending')
        {
            $data[$task->client->country->name][strtolower($task->design_type.'_pending')] += 1;
        }
        if ($task->status == 'RFS')
        {
            $data[$task->client->country->name][strtolower($task->design_type.'_rfs')] += 1;
        }
        if (!$task->designer_id)
        {
            $data[$task->client->country->name][strtolower($task->design_type.'_unsigned')] += 1;
        }
    }
    return $data;
}