<?php

function inputField($label, $name, $defalutValue, $error = false, $attr = [])
{ 
    $defaluts = ['type'=> 'text', 'class'=>'', 'id'=>''];

    foreach ($attr as $key => $value) {
        $defaluts[$key] = $value;
    }

    $class = $defaluts['class'] . ' md-form-control';
    $class .= $defalutValue? ' md-valid ': '';

    $hasError = $error? ' md-input-danger' : '';
    $id = $defaluts["id"]? 'id="'.$defaluts['id'].'"':'';
?>

    <div class="md-input-wrapper<?php echo $hasError; ?>">
        
        <input type="<?php echo $defaluts['type']; ?>" <?php echo $id; ?> name="<?php echo $name; ?>" class="<?php echo $class ?>" value="<?php echo $defalutValue; ?>"/>

        <label><?php echo $label ?></label>
        
        <?php if ($error): ?>
        
            <span class="invalid-feedback">
        
                <strong><?php echo $error; ?></strong>
        
            </span>
        
        <?php endif; ?>

    </div>

<?php }

function selectField($label, $name, $options, $defalutValue, $error = false, $attr = [])
{ 
    $defaluts = ['class'=> 'md-valid', 'id'=>'', 'first_blank' => true, 'multiple' => false, 'endpoint' => false, 'select2Defaults'];

    foreach ($attr as $key => $value) {
        $defaluts[$key] = $value;
    }

    $endpoing =  ($defaluts['endpoint'])? 'data-api-endpoint="' . $defaluts['endpoint'] . '"': '';

    $class = ($defaluts['endpoint'])? $defaluts['class'] . ' selectApi ':'';
    
    $class .= $defaluts['class'] . ' md-form-control ';

    $hasError = $error? ' md-input-danger' : '';
    $id = $defaluts["id"]? 'id="'.$defaluts['id'].'"':'';
?>


<div class="md-input-wrapper<?php echo $hasError; ?>">

    <!-- <select class="js-data-example-ajax select2-hidden-accessible selectApi md-valid" tabindex="-1" aria-hidden="true">
    </select> -->

    <select class="<?php echo $class; ?>" <?php echo $endpoing; ?> name="<?php echo $name; ?>" <?php echo $defaluts['multiple']? 'multiple':''; ?>>

        <?php if( $endpoing ): ?>

            <?php if( $defalutValue ): ?>
                <option value="<?php echo $defalutValue ?>"><?php echo $defaluts['select2Defaults']; ?></option>
            <?php endif; ?>
        
        <?php else: ?>

            <?php if( $defaluts['first_blank'] ): ?>
                <option value=''>Select</option>
            <?php endif; ?>
            <?php foreach($options as $key => $value): ?>
                <?php if( $defaluts['multiple'] ): ?>
                    <option value="<?php echo $key ?>"<?php echo in_array($key, $defalutValue)? ' selected':''; ?>><?php echo $value; ?></option>            
                <?php else:?>
                    <option value="<?php echo $key ?>"<?php echo $defalutValue == $key? ' selected':''; ?>><?php echo $value; ?></option>            
                <?php endif;?>
            <?php endforeach;?>

        <?php endif; ?>

    </select>
    
    <label><?php echo $label ?></label>

    <?php if ($error): ?>
    
        <span class="invalid-feedback">
    
            <strong><?php echo $error; ?></strong>
    
        </span>
    
    <?php endif; ?>
</div>

<?php }





function textField($label, $name, $defalutValue, $error = false, $attr = [])
{ 
    $defaluts = ['class'=> $defalutValue? 'md-valid':'', 'id'=>'', 'first_blank' => true, 'rows' => 4];

    foreach ($attr as $key => $value) {
        $defaluts[$key] = $value;
    }

    $class = $defaluts['class'] . ' md-form-control';
    $hasError = $error? ' md-input-danger' : '';
    $id = $defaluts["id"]? 'id="'.$defaluts['id'].'"':'';
    $rows = $defaluts["rows"]? 'rows="'.$defaluts['rows'].'"':'';
?>


<div class="md-input-wrapper<?php echo $hasError; ?>">
    
    <textarea class="<?php echo $class; ?>" name="<?php echo $name; ?>" <?php echo $id; ?>><?php echo $defalutValue; ?></textarea>
    
    <label><?php echo $label ?></label>

    <?php if ($error): ?>
    
        <span class="invalid-feedback">
    
            <strong><?php echo $error; ?></strong>
    
        </span>
    
    <?php endif; ?>
</div>

<?php }



function checkboxField($label, $name, $value, $defaultValue, $error = false, $attr = [])
{ 
    $defaults = ['class'=> $defaultValue? 'md-valid':'', 'id'=>'', 'display_errors' => true];

    foreach ($attr as $k => $v) { $defaults[$k] = $v; }

    $class = $defaults['class'] . ' md-form-control';
    $hasError = $error? ' md-input-danger' : '';
    $id = $defaults["id"]? 'id="'.$defaults['id'].'"':'';

    if( is_array($defaultValue) )
    {
        $checked = in_array($value, $defaultValue)? 'checked':'';
    }
    else{
        $checked = ($value == $defaultValue)? 'checked':'';
        // dd($checked);
    }

    $id = $defaults["id"]? 'id="'.$defaults['id'].'"':'';
?>

    <div class="rkmd-checkbox checkbox-rotate checkbox-ripple">
        <label class="input-checkbox checkbox-primary">
            <input type="checkbox" id="checkbox" name="<?php echo $name; ?>" <?php echo $checked ?> value="<?php echo $value ?>">
            <span class="checkbox"></span>
        </label>
        <div class="captions"><?php echo $label; ?></div>
    </div>

    <?php if ($error && $defaults['display_errors']): ?>
    
        <span class="invalid-feedback">
    
            <strong><?php echo $error; ?></strong>
    
        </span>
    
    <?php endif; ?>

<?php } ?>