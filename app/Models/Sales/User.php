<?php

namespace App\Models\Sales;

use App\Models\Client;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Notifications\Notifiable;


class User extends Authenticatable
{
    use Notifiable;

    protected $guard = 'sales';

    protected $fillable = ['name', 'email', 'password', 'father_name', 'gender', 'cnic', 'dob', 'phone', 'address', 'education', 'institute', 'passing_year', 'contact_person', 'contact_person_relation', 'contact_person_phone', 'contact_person_address', 'bonus', 'additional_bonus', 'revision_bonus', 'edit_bonus', 'approval_bonus', 'sale_wrap_commision'];
    protected $hidden = ['password', 'remember_token'];
    protected $dates = ['dob'];

    /**
     * Get all of the users's Logs.
     */
    public function logs()
    {
        return $this->morphMany('App\Models\LogActivity', 'logable');
    }

    public function leads()
    {
        return $this->hasMany(Lead::class, 'created_by');
    }

    public function clients()
    {
        return $this->hasMany(Client::class, 'referred_by');
    }

    /**
     * Get all of the users's Roles.
     */
    public function roles()
    {
        return $this->belongsToMany('App\Models\Role');
    }

    /**
     * Get all of the users's Jobs/Tasks.
     */
    public function tasks()
    {
        return $this->hasMany('App\Models\Task', 'designer_id');
    }

    public function bonuses()
    {
        return $this->hasMany(Models\Bonus::class);
    }

    public static function findByPermission($permission)
    {
        return static::with('roles')->whereHas('roles', function($query) use ($permission) {

            $query->whereHas('permissions', function($query) use ($permission) {

                $query->where(is_numeric($permission)? 'id':'name', '=', $permission);

            });

        })->get();
    }

    public function hasRole($role)
    {

        if (is_string($role)) {
            return $this->roles->contains('name', $role);
        }

        return !!$role->intersect($this->roles)->count();

        // for breaf over view or intersec()
        // foreach( $role as $r )
        // {
        //     if( $this->hasRole($r->name) )
        //         return true;
        // }
    }

}
