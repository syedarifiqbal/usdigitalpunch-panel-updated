<?php

namespace App\Models\Sales;

use Illuminate\Database\Eloquent\Model;

class Call extends Model
{
    protected $fillable = [ 'subject', 'duration', 'feedback', 'details' ];

    public function lead()
    {
        return $this->belongsTo(Lead::class);
    }

    public function owner()
    {
        return $this->belongsTo(\App\User::class, 'created_by');
    }
}
