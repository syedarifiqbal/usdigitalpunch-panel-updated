<?php

namespace App\Models\Sales;

use App\Models\Country;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Lead extends Model
{
    use SoftDeletes;

    protected $fillable = [ 'name', 'company', 'phone', 'mobile', 'email', 'state', 'city', 'address', 'description', 'last_feedback', 'last_call_at', 'country_id'];

    protected $casts = [
        'last_call_at' => 'datetime'
    ];

    protected $dates = [ 'deleted_at' ];

    public function country()
    {
        return $this->belongsTo(Country::class);
    }

    public function owner()
    {
        return $this->belongsTo(\App\User::class, 'created_by');
    }

    public function calls()
    {
        return $this->hasMany(Call::class);
    }

    public function latest_call()
    {
        return $this->hasOne(Call::class)->latest();
    }
}
