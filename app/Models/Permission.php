<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Permission extends Model
{
    protected $fillable = ['name', 'label', 'module'];
    
    public function roles()
    {
        return $this->belongsToMany(Role::class);
    }

    public static function findByModule()
    {
        $permissions = self::all();

        $temp = [];

        foreach($permissions as $p)  { $temp[$p->module][] = $p; }

        return $temp;

    }

}
