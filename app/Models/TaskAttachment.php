<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class TaskAttachment extends Model
{
    protected $fillable = [ 'task_id', 'filename', 'extension', 'path', 'size', 'for_delivery' ];

    public $timestamps = false;
    
    public function task()
    {
        return $this->belongsTo(Task::class);
    }
}
