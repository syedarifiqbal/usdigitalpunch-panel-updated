<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Task extends Model
{
    protected $fillable = [ 'date', 'delivery_date', 'name', 'po_number', 'currency', 'price', 'required_format', 'unit', 'company_id', 'width', 'height', 'status', 'approved_by', 'placement', 'price', 'design_type', 'priority', 'parent_id', 'order_type', 'is_approved', 'designer_id', 'client_id', 'remarks', 'delivery_width', 'delivery_height', 'stitches', 'delivery_remarks' ];
    
    protected $casts = [
        'required_format' => 'array',
        'is_approved' => 'boolean'
    ];

    protected $dates = [
        // 'created_at',
        // 'updated_at',
        // 'deleted_at',
        'date',
        'delivery_date',
    ];

    public function logs()
    {
        return $this->morphMany('App\Models\LogActivity', 'logable');
    }

    public function attachments()
    {
        return $this->hasMany(TaskAttachment::class, 'task_id');
    }

    public function childrens()
    {
        return $this->hasMany(Task::class, 'parent_id');
    }

    public function parent()
    {
        return $this->belongsTo(Task::class, 'parent_id');
    }

    public function client()
    {
        return $this->belongsTo(Client::class);
    }

    public function designer()
    {
        return $this->belongsTo(\App\User::class, 'designer_id');
    }

    public function checker()
    {
        return $this->belongsTo(\App\User::class, 'approved_by');
    }

    public function company()
    {
        return $this->belongsTo(Company::class, 'company_id');
    }

    public function bonus()
    {
        return $this->hasMany(Bonus::class, 'task_id');
    }

    public function addAttachments($request, $forDelivery = 0)
    {
        if( $request->uploads )
        {
            foreach ($request->uploads as $index => $file) 
            {
                $filename = strtolower(str_replace(' ', '_', $file->getClientOriginalName()));
                $filename = preg_replace("/[^a-zA-Z0-9._]/", "", $filename);

                if( $request->file('uploads.'.$index)->isValid() )
                {
                    if(app()->environment() == 'testing' || app()->environment() == 'local')
                    {
                        $path = $file->storeAs('attachments', $filename);
                    }else{
                        // $path = $file->storeAs('attachments', $filename, 's3');
                        $path = $file->storeAs('attachments', $filename);
                    }
                    $this->attachments()->create([
                        'filename' => $file->getClientOriginalName(), 
                        'extension' => $file->guessExtension()??'',
                        'path'=> $path, 
                        'size' => $file->getClientSize(), 
                        'for_delivery' => $forDelivery
                    ]);
                }
            }
        }
    }

    /**
     * Set the Task's date.
     *
     * @param  string  $value
     * @return void
     */
    public function setDateAttribute($value)
    {
        $this->attributes['date'] = \Carbon\Carbon::createFromFormat('d/m/Y', $value);
    }

    public function setDeliveryDateAttribute($value)
    {
        $this->attributes['delivery_date'] = \Carbon\Carbon::createFromFormat('d/m/Y', $value);
    }

    public function isLate()
    {
        return $this->created_at < now()->modify('-12 hours');
    }

    public function getCurrency()
    {
        $symbol = '$';
        switch ($this->currency)
        {
            case 'USD':
                $symbol = '$';
                break;
            case 'CA':
                $symbol = '$';
                break;
            case 'NZD':
                $symbol = '$';
                break;
            case 'GBP':
                $symbol = '£';
                break;
            case 'EUR':
                $symbol = '€';
                break;
        }
        return $symbol;
    }

    // public function getDateAttribute()
    // {
    //     return "$this->data->format('d/m/Y')";
    // }
}
