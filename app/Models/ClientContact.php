<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class ClientContact extends Model
{
    protected $table = 'client_contacts';

    protected $fillable = ['name', 'phone', 'email'];

    public $timestamps = false;

    public function client()
    {
        return $this->blongsTo(Client::class, 'id');
    }
}
