<?php

namespace App\Models;

use Carbon;
use Illuminate\Database\Eloquent\Model;

class LogActivity extends Model
{
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'description', 'url', 'method', 'ip', 'agent', 'user_id',
    ];

    protected $casts = [
        'created_at' => 'datetime'
    ];

    /**
     * Get all of the owning logs models.
     */
    public function logable()
    {
        return $this->morphTo();
    }

    /**
     * Get the log's date.
     *
     * @param  null
     * @return string
     */
    public function getDate()
    {
        return Carbon::parse($this->created_at, 'Europe/London')->timezone('Asia/Karachi')->format('d');
    }

    /**
     * Get the log's Month.
     *
     * @param  null
     * @return string
     */
    public function getMonth()
    {
        return Carbon::parse($this->created_at, 'Europe/London')->timezone('Asia/Karachi')->format('M');
        // return (new Carbon())->format('M');
    }

    /**
     * Get the log's Time.
     *
     * @param  null
     * @return string
     */
    public function getTime()
    {
        return Carbon::parse($this->created_at)->timezone('Asia/Karachi')->format('h:i:s');
        return (new Carbon($this->created_at))->format('h:i:s');
    }
}
