<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Notifications\Notifiable;


class Client extends Model
{
    use Notifiable, SoftDeletes;

    protected $fillable = [ 'name', 'nick_name', 'company', 'email', 'country_id', 'is_active', 'from_lead', 'phone', 'address', 'instructions', 'secret_instructions', 'referred_by', 'company_id'];
    
    protected $casts = [
        'is_active' => 'boolean'
    ];

    public function saveContacts($contacts)
    {
        $this->contacts()->delete();
        $this->contacts()->createMany($contacts);
    }
    
    public function logs()
    {
        return $this->morphMany(LogActivity::class, 'logable');
    }

    public function orders()
    {
        return $this->hasMany(Task::class);
    }

    public function latestOrder()
    {
        return $this->hasOne(\App\Models\Task::class)->latest();
    }

    public function country()
    {
        return $this->belongsTo(Country::class);
    }

    public function owner()
    {
        return $this->belongsTo(\App\User::class, 'referred_by');
    }

    public function contacts()
    {
        return $this->hasMany(ClientContact::class, 'client_id');
    }

}
