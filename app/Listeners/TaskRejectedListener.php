<?php

namespace App\Listeners;

use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use \App\Notifications\BonusGenerated;

class TaskRejectedListener
{
    public function __construct()
    {
        
    }

    public function handle($event)
    {
        $task = $event->task;
        /*if( strtolower($task->order_type) == 'edit' ){ return; }*/
        $bonus = new \App\Models\Bonus;
        $bonus->user_id = $task->designer->id;
        $bonus->bonus_type = 'Edit';
        $bonus->tip = 0; 
        $bonus->rs = -(strtolower($task->order_type) === 'edit'? $task->designer->revision_bonus: $task->designer->bonus);
        $task->bonus()->save($bonus);
        $task->designer->notify(new BonusGenerated($task, sprintf('Bonus deducted against job rejection (%s)', $task->name)));
    }

}
