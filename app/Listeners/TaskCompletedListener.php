<?php

namespace App\Listeners;

use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use \App\Notifications\BonusGenerated;
class TaskCompletedListener
{
    public function __construct()
    {
    }

    public function handle($event)
    {
        $this->addBonus($event->data);

//        dd($event->data);
        $this->addCheckingBonusIfLegible($event->data);
    }

    protected function addBonus($data)
    {
        $task = $data->task;

        // if( $task->status == STATUS_COMPLETE ){ return; }

        $bonus = new \App\Models\Bonus;

        $bonus->user_id = $task->designer->id;
        
        $bonus->bonus_type = $task->order_type;

        $bonus->tip = $data->additional_bonus;
        
        switch( strtolower($task->order_type) )
        {
            case 'revision':
                $bonus->rs = $task->designer->revision_bonus;
            break;
            
            case 'edit':
                $bonus->rs = $task->designer->edit_bonus;
            break;
            
            default:
                $bonus->rs = $task->designer->bonus;
        }

        $task->bonus()->save($bonus);

        $task->designer->notify(new BonusGenerated($task, sprintf('Bonus Generated for %s', $task->name)));
    }

    protected function addCheckingBonusIfLegible($data)
    {
        $task = $data->task;

        // if( $task->status == STATUS_COMPLETE ){ return; }

        if( strtolower($task->design_type) != 'digitizing' && !$data->checker ) { return; }

        $checker = \App\User::find($data->checker);

        if (! $checker ) return;

        $bonus = new \App\Models\Bonus;

        $bonus->user_id = $data->checker;

        $bonus->rs = $checker->approval_bonus;
        
        $bonus->bonus_type = 'Checking';

        $task->bonus()->save($bonus);

        $checker->notify(new BonusGenerated($task, sprintf('Checking Bonus has Generated for %s', $task->name)));        
    }
}
