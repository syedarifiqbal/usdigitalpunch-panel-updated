<?php

namespace App\Listeners;

use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;

class NotificationHasReadListener
{
    public $readAll;

    /**
     * Create the event listener.
     *
     * @return void
     */
    public function __construct()
    {
        $this->readAll = true;
    }

    /**
     * Handle the event.
     *
     * @param  object  $event
     * @return void
     */
    public function handle($event)
    {
        auth()->user()->notifications->markAsRead();
    }
}
