<?php

namespace App\Listeners;

use App\Events\TaskAssigned;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;

class EventListener
{
    public $all_read;
    
    /**
     * Create the event listener.
     *
     * @return void
     */
    public function __construct()
    {
        $this->all_read = true;
    }

    /**
     * Handle the event.
     *
     * @param  TaskAssigned  $event
     * @return void
     */
    public function notify(TaskAssigned $event)
    {
        $user = \App\User::find($event->task->designer->id);
        $user->notify(new \App\Notifications\TaskAssigned($event->task));
    }
}
