<?php

namespace App\Providers;

use Illuminate\Support\Facades\Event;
use Illuminate\Foundation\Support\Providers\EventServiceProvider as ServiceProvider;

class EventServiceProvider extends ServiceProvider
{
    /**
     * The event listener mappings for the application.
     *
     * @var array
     */
    protected $listen = [
        'App\Events\TaskAssigned' => [
            'App\Listeners\EventListener@notify',
        ],

        'App\Events\NotificationHasRead' => [
            'App\Listeners\NotificationHasReadListener',
        ],

        'App\Events\TaskCompleted' => [
            'App\Listeners\TaskCompletedListener',
        ],

        'App\Events\TaskRejected' => [
            'App\Listeners\TaskRejectedListener',
        ],

    ];

    /**
     * Register any events for your application.
     *
     * @return void
     */
    public function boot()
    {
        parent::boot();

        //
    }
}
