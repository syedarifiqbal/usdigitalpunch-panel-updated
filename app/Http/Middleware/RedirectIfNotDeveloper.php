<?php

namespace App\Http\Middleware;

use Closure;
use Illuminate\Support\Facades\Auth;

class RedirectIfNotDeveloper
{
    public function handle($request, Closure $next)
    {
        if (!Auth::user()->is_developer) {
            flash('You are not permitted to view this page', FLASH_DANGER);            
            return redirect('/');
        }

        return $next($request);
    }
}
