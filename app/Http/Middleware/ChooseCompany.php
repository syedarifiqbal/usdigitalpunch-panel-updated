<?php

namespace App\Http\Middleware;

use Closure;
use Auth;
use Gate;

class ChooseCompany
{
    public function handle($request, Closure $next)
    {
        // if (app()->environment() === 'testing') {
        //     return $next($request);
        // }

        // dd($request->route()->getName());

        if (Auth::check() && Gate::allows('add-user') && !selectedCompany()) 
        {
            return redirect()->route('choose.company');
        }

        // session()->put('settings', $settings);

        return $next($request);
    }
}
