<?php

namespace App\Http\Controllers;

use App\Models\Task;
use Illuminate\Http\Request;

class SearchController extends BaseController
{
    protected $menu = 'tasks';

    public function index(Request $request)
    {
        $id = $request->query('term');

        $task = Task::find($id);

        if (!$task)
        {
            flash('There is no job exists with this order number', FLASH_WARNING);
            return redirect()->route('home');
        }

        $data['pageTitle'] = 'Orders';
        $data['menu'] = $this->menu;
        $data['submenu'] = 'view-task';
        $data['breadcrumb'] = 'tasks.show';
        $data['breadcrumbData'] = $task;
        $data['task'] = $task;
        return view('tasks.show', $data);
    }
}
