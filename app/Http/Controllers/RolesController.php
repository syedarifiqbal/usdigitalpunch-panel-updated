<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use \App\Models\Role;
use \App\Models\Permission;

class RolesController extends BaseController
{
    protected $menu = 'users';

//    function __construct()
//    {
//        $this->middleware('developer');
//    }

    public function index()
    {
        allowedOrRedirect('view-role', 'roles');

        $data['roles'] = Role::all();

        $data['menu'] = $this->menu;

        $data['submenu'] = 'view-role';

        return view('roles.index', $data);
    }

    public function create()
    {
        $data['role'] = new Role;

        $data['menu'] = $this->menu;

        $data['submenu'] = 'add-role';

        $data['permissions'] = Permission::findByModule();

        $data['hasPerms'] = collect([]);

        return view('roles.create', $data);
    }

    public function store(Request $request)
    {
        $request->validate(['name' => 'required']);

        $role = new Role($request->all());

        $role->save();

        flash('Role Saved successfully', FLASH_SUCCESS);

        return redirect()->route('roles');
    }

    public function show(Role $role)
    {
        //
    }

    public function edit(Role $role)
    {
        allowedOrRedirect('edit-role', 'roles');

        $data['role'] = $role;

        $data['menu'] = $this->menu;

        $data['permissions'] = Permission::findByModule();

        $data['hasPerms'] = $role->permissions->pluck('id');

        return view('roles.create', $data);
    }

    public function update(Request $request, Role $role)
    {
        allowedOrRedirect('edit-role', 'roles');

        $request->validate([
            'name' => 'required',
            'permissions' => 'required|array'
        ]);

        \DB::transaction(function() use($request, $role){
            
            $role->fill($request->all());
    
            $role->save();
    
            $role->permissions()->sync($request->input('permissions'));

        });

        flash('Role Saved successfully', FLASH_SUCCESS);

        return redirect()->route('roles');
    }

    public function destroy(Role $role)
    {
        allowedOrRedirect('delete-role', 'roles');

        $role->delete();

        flash('Role Deleted successfully', FLASH_SUCCESS);

        return redirect()->route('roles');
    }
}
