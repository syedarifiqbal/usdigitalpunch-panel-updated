<?php

namespace App\Http\Controllers;

use App\Models\Country;
use App\Models\Task;
use Illuminate\Http\Request;

class BaseController extends Controller
{
    public $roles;

    public function __construct()
    {
        $this->middleware(function ($request, $next) {
            $this->roles = auth()->user()->roles->pluck('name')->toArray();
            $request->request->add(['roles' => $this->roles]);
            return $next($request);
        });
    }
}
