<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use \App\Models\Permission;

class PermissionsController extends BaseController
{
    protected $menu = 'users';

    function __construct()
    {
        parent::__construct();
        $this->middleware('developer');
    }

    public function index()
    {
        allowedOrRedirect('view-task');

        $data['menu'] = $this->menu;

        $data['submenu'] = 'view-permission';

        $data['permissions'] = Permission::orderBy('module')->orderBy('label')->orderBy('id')->get();

        return view('permissions.index', $data);
    }

    public function create()
    {
        allowedOrRedirect('add-task');

        $data['menu'] = $this->menu;

        $data['submenu'] = 'add-permission';

        $data['permission'] = new Permission;

        return view('permissions.create', $data);
    }

    public function store(Request $request)
    {
        allowedOrRedirect('add-task');

        $request->validate(['name' => 'required']);

        $permission = new Permission($request->all());

        $permission->save();

        flash('Permissions Saved successfully', FLASH_SUCCESS);

        return redirect()->route('permissions');
    }

    public function show(Permission $permission)
    {
        allowedOrRedirect('view-task');

        $data['menu'] = $this->menu;
    }

    public function edit(Permission $permission)
    {
        allowedOrRedirect('edit-task');

        $data['menu'] = $this->menu;

        $data['permission'] = $permission;

        return view('permissions.create', $data);
    }

    public function update(Request $request, Permission $permission)
    {
        allowedOrRedirect('edit-task');

        $request->validate(['name' => 'required']);

        $permission->fill($request->all());

        $permission->save();

        flash('Permissions Saved successfully', FLASH_SUCCESS);

        return redirect()->route('permissions');
    }

    public function destroy(Permission $permission)
    {
        allowedOrRedirect('delete-task');

        $permission->delete();

        flash('Permissions Deleted successfully', FLASH_SUCCESS);

        return redirect()->route('permissions');
    }
}
