<?php

namespace App\Http\Controllers\Generators;

use App\Http\Controllers\BaseController;
use App\Models\Permission;
use App\Models\PermissionRole;
use App\Models\Role;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class RoleandseedControllers extends BaseController
{
    protected $contents;

    public function __construct()
    {
        parent::__construct();
        $this->contents = file_get_contents(database_path('seeds/RoleTableSeeder.php'));
    }

    public function index()
    {
$contents = "<?php\n
use Illuminate\Database\Seeder;\n
class RoleTableSeeder extends Seeder
{
    public function __construct()
    {
        \$this->roles = [\n";
        foreach (Role::all() as $role):
            $contents .= "\t\t\t['name' => '{$role->name}'],\n";
        endforeach;
        $contents .= "\t\t];

        \$this->permissions = [\n";

        foreach (Permission::orderBy('module')->orderBy('name')->get() as $permission):
            $contents .= "\t\t\t['module' => '{$permission->module}', 'label'=>'{$permission->label}', 'name' => '{$permission->name}'],\n";
        endforeach;
        $contents .= "\t\t];
    }

    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        App\Models\Role::truncate();
        App\Models\Permission::truncate();
        App\Models\PermissionRole::truncate();
        
        foreach (\$this->roles as \$role) {
            App\Models\Role::create(\$role);
        }

        foreach (\$this->permissions as \$permission) {
            App\Models\Permission::create(\$permission);
        };

        \$r_id = App\Models\Role::where('name', 'Super Admin')->first();
        
        if (\$r_id)
        {
            foreach (App\Models\Permission::all() as \$perm) {
                App\Models\PermissionRole::create(['role_id' => \$r_id->id, 'permission_id' => \$perm->id]);
            };
        }
    }
}";

        if(file_put_contents(database_path('seeds/RoleTableSeeder.php'), $contents))
        {
            return 'done';
        };

//        echo '<pre>';
//        echo e($contents);
//        echo '</pre>';
    }
}
