<?php

namespace App\Http\Controllers;

use App\User;
use Illuminate\Http\Request;

class ChangePasswordController extends BaseController
{
    protected $menu = 'users';

    function form(User $user)
    {
        allowedOrRedirect('edit-user');
        $data['menu'] = $this->menu;
        $data['user'] = $user;
        return view('users.change_password', $data);
    }

    function store(Request $request, User $user)
    {
        $request->validate(['password' => 'required|string|min:6|confirmed']);

        $user->password = bcrypt(\request('password'));
        $user->save();

        activity_log("New Task Created {$user->name}", $user);
        flash('Order Created Successfully', FLASH_SUCCESS);
        return redirect('users');
    }
}
