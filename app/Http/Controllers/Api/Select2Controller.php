<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\BaseController;
use App\Models\Sales\Lead;
use Illuminate\Http\Request;

class Select2Controller extends BaseController
{

    function countries()
    {
        $record = \App\Models\Country::where('name', 'LIKE', '%' . request('q') . '%')
            ->select(['name AS name', 'id AS id', 'flag'])->paginate(10);
        return $record;
    }

    function clients()
    {
        $record = \App\Models\Client::where(function($q){
                $q->orWhere('name', 'LIKE', '%' . request('q') . '%')
                ->orWhere('nick_name', 'LIKE', '%' . request('q') . '%');
            })
//            ->where('is_active', 1)
            ->where('company_id', session('company_id'))
            ->selectRaw('CONCAT(nick_name, " (", email, ")") AS name, id AS id')->paginate(10);
        return $record;
    }

    function users()
    {
        $record = \App\User::where('name', 'LIKE', '%' . request('q') . '%')
            ->select(['name AS name', 'id AS id'])->paginate(10);
        return $record;
    }

}
