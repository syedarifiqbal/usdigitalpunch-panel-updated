<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class SettingsController extends BaseController
{

    public function index()
    {
        $data['breadcrumb'] = 'settings';

        if( !(in_array('Admin', $this->roles) || in_array('Super Admin', $this->roles)) )
        {
            if (request('tab') != 'ui')
                return redirect()->route('settings', ['tab'=>'ui']);
        }

        $data['pageTitle'] = 'Settings';

        return view('users.settings', $data);
    }

    public function store(Request $request, $tab)
    {
        $roles = [];

//        switch( $tab )
//        {
//            case 'general':
//                $roles['phone'] = 'required';
//                $roles['address'] = 'required';
//                $roles['allowed_extension'] = 'required';
//                break;
//            case 'email':
//                $roles['email_from_' . session('company_id', 1)] = 'required|email';
//                break;
//            case 'ui':
//                $roles['table_page_length'] = 'numeric';
//                break;
//            case 'smtp':
//                $roles['smtp_host'] = 'required';
//                $roles['smtp_username'] = 'required';
//                $roles['smtp_password'] = 'required';
//                break;
//        }

//        $request->validate($roles);

        $data = $request->except('_token');

        collect($data)->each(function( $v, $k ) use(&$data){

            if(empty($v))
            {
                Setting()->forget($k);
                unset($data[$k]);
            }

        });

        setting()->setExtraColumns(['company_id' => session('company_id', '1')]);

        setting($data)->save();

        flash('Settings Saved Successfully', FLASH_SUCCESS);

        return redirect()->route('settings');
    }
}
