<?php

namespace App\Http\Controllers\Auth;

use App\Http\Controllers\BaseController;
use App\User;
use Carbon\Carbon;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Auth;
use Illuminate\Auth\Events\Registered;
use Illuminate\Support\Facades\Validator;
use Illuminate\Foundation\Auth\RegistersUsers;

class RegisterController extends BaseController
{
    protected $menu = 'users';

    /*
    |--------------------------------------------------------------------------
    | Register Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles the registration of new users as well as their
    | validation and creation. By default this controller uses a trait to
    | provide this functionality without requiring any additional code.
    |
     */

    use RegistersUsers;

    /**
     * Where to redirect users after registration.
     *
     * @var string
     */
    protected $redirectTo = '/users';

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
        // Redis::set('name', 'arif');

        // dd(Redis::get('name'));
        // $this->middleware('guest');
    }

    /**
     * Show the application registration form.
     *
     * @return \Illuminate\Http\Response
     */
    public function showRegistrationForm()
    {
        allowedOrRedirect('add-user');

        $data['breadcrumb'] = 'create.user';

        $data['menu'] = $this->menu;

        $data['submenu'] = 'add-user';

        $data['pageTitle'] = 'User Registration';

        $data['user'] = new User;

        return view('users.create', $data);
    }

    /**
     * Get a validator for an incoming registration request.
     *
     * @param  array  $data
     * @return \Illuminate\Contracts\Validation\Validator
     */
    protected function validator(array $data)
    {
        // dd($data['roles_id']);
        return Validator::make($data, [
            'name' => 'required|string|max:255',
            'email' => 'required|string|email|max:255|unique:users',
            'password' => 'required|string|min:6|confirmed',
            'father_name' => 'required',
            'gender' => 'required',
            'cnic' => 'required',
            'dob' => 'required',
            'phone' => 'required',
            'address' => 'required',
            'contact_person' => 'required',
            'contact_person_relation' => 'required',
            'contact_person_phone' => 'required',
            'contact_person_address' => 'required',
            'roles_id' => 'required|array|min:1'            
        ]);
    }

    /**
     * Create a new user instance after a valid registration.
     *
     * @param  array  $data
     * @return \App\User
     */
    protected function create(array $data)
    {
        allowedOrRedirect('add-user');

        flash('User Created Successfully', FLASH_SUCCESS);

        return User::create([
            'name' => $data['name'],
            'email' => $data['email'],
            'password' => Hash::make($data['password']),
            'father_name' => $data['father_name'],
            'gender' => $data['gender'],
            'cnic' => $data['cnic'],
            'dob' => Carbon::createFromFormat('d/m/Y', $data['dob']),
            'phone' => $data['phone'],
            'address' => $data['address'],
            'education' => $data['education'],
            'institute' => $data['institute'],
            'passing_year' => $data['passing_year'],
            'contact_person' => $data['contact_person'],
            'contact_person_relation' => $data['contact_person_relation'],
            'contact_person_phone' => $data['contact_person_phone'],
            'contact_person_address' => $data['contact_person_address'],
            'bonus' => $data['bonus']??0,
            'additional_bonus' => $data['additional_bonus']??0,
            'revision_bonus' => $data['revision_bonus']??0,
            'edit_bonus' => $data['revision_bonus']??0,
            'sale_wrap_commision' => $data['sale_wrap_commision']??0,
        ]);
    }

    /**
     * Handle a registration request for the application.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function register(Request $request)
    {
        // allowedOrRedirect("add-user");

        $this->validator($request->all())->validate();

        // dd($request->all);

        event(new Registered($user = $this->create($request->all())));
        // Auth::loginUsingId(1);
        return $this->registered($request, $user)
        ?: redirect($this->redirectPath());
    }

    /**
     * The user has been registered Add log and redirect to home.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  mixed  $user
     * @return mixed
     */
    protected function registered(Request $request, $user)
    {
        activity_log('Registered new User', $user);
    }
}
