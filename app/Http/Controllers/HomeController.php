<?php

namespace App\Http\Controllers;

use App\Models\Country;
use App\Models\Task;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Gate;

class HomeController extends BaseController
{
    protected  $menu = 'dashboard';

    public function __construct()
    {
        parent::__construct();
        $this->middleware('auth');
    }

    public function index()
    {
        //        \DB::listen(function($q){
        //            dump($q->sql);
        //        });
        $data['menu'] = $this->menu;

        // get current users tasks if has permissions
        if (Gate::allows('view-user-progress-task'))
        {
            $currentUsersTask = \App\User::whereHas('tasks', function ($query) {
                $query->where('status', '!=', 'Complete');
            })->withCount(['tasks' => function ($query) {
                $query->where('status', '!=', 'Complete');
            }])->get();

            $data['currentUsersTask'] = $currentUsersTask;
        }

        // get and prepare pending order summary if has permission
        if (Gate::allows('view-order-summary'))
        {
            $tasks = (Task::with('client.country')->where('status', '!=', 'Complete')->get());
            $data['pending_task_summary'] = prepareCountriesJob($tasks);
        }
        
        // return $data['pending_task_summary'];

        // get and today's tasks list if has permission
        // if (Gate::allows('view-today-tasks-list'))
        // {
        //     $data['today_tasks'] = Task::with('client.country')->whereRaw('Date(created_at) = CURDATE()')->get();
        // }

        $data['sales_wrap_orders'] = null;

        if(auth()->user()->sale_wrap_commision > 0)
        {
            $sales_wrap_orders = Task::whereHas('client', function($q){
                $q->where('referred_by', auth()->id());
            })->where('status', '!=', 'Complete')->get();
            $data['sales_wrap_orders'] = $sales_wrap_orders;
        }

        return view('dashboard.index', $data);
    }

    public function choose_company(Request $request)
    {
        if($request->isMethod('post'))
        {
            session()->put('company_id', $request->input('company_id'));

            return redirect()->route('home');
        }

        $data['companies'] = \App\Models\Company::all()->pluck('name', 'id');

        return view('dashboard.choose_company', $data);
    }
}
