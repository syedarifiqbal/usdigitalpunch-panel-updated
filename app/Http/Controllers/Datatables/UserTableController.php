<?php

namespace App\Http\Controllers\Datatables;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use DB;
use Gate;
use App\User;
use Illuminate\Pagination\Paginator;

class UserTableController extends Controller
{
    
    private $request;

    /**
     * Inject Request variable when initiated by system
     */
    function __construct(Request $request)
    {
        $this->request = $request;
    }
    
    /**
     * Get User data for user data table
     * @param boolean is_active
     */
    function getData()
    {
        $currentPage = ($this->request->input('start')/$this->request->input('length'))+1;

        Paginator::currentPageResolver(function () use ($currentPage) { return $currentPage; });
    
        // Search value send from datatables
        $term = $this->request->input('search')['value'];

        // column defination what we need to select
        $columns = ['id', 'name', 'email', 'is_active', 'phone', 'address'];

        // query to elequent model
        $query = User::select($columns)
            ->where('is_active', (request('status', 'active') == 'disable') ? 0:1)
            ->where(function ($query) use($term) {
                $query->orwhere('name', 'like', "%$term%")
                      ->orwhere('email', 'like', "%$term%")
                      ->orwhere('address', 'like', "%$term%")
                      ->orwhere('phone', 'like', "%$term%");
            });

        if ($this->request->input('order')) {
            $colIndex       = $this->request->input('order')[0]['column'];
            $colDirection   = $this->request->input('order')[0]['dir'];
            $query->orderBy($columns[$colIndex], $colDirection);
        } else {
            $query->orderBy('id', 'desc');
        }
        $query->with(['roles' => function ($q) {$q->select('id', 'name');}]);
        // return $query->get()->first()->roles->map(function ($roles) {
        //     return $roles->name;
        // });
        // return $query->get();
        return $this->prepareData($query->paginate($this->request->input('length')));
    }

    public function prepareData($paginate)
    {
        $data = [
            "draw"              => intval($this->request->input("draw")),
            "recordsTotal"      => User::where('is_active', (request('status', 'active') == 'disable') ? 0:1)->count(),
            "recordsFiltered"   => $paginate->total(),
            "data"              => []
        ];
        
        foreach($paginate->items() as $row)
        {
            $roles = $row->roles->map(function ($roles)
            {
                return $roles->name;
            });

            $actions = '<div class="dropdown show">' .
                    '<a class="btn btn-outline-danger btn-sm dropdown-toggle" href="#" role="button" id="dropdownMenuLink" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                        <i class="fa fa-ellipsis-h "></i>
                    </a>
                    <div class="dropdown-menu" aria-labelledby="dropdownMenuLink">';
            if (Gate::allows("edit-user")):
                $name_with_edit_link = sprintf('<a href="%s"><i class="fa fa-pencil"></i> %s </a>', route("edit.users", [$row->id]), $row->name);
                $actions .= sprintf('<a href="%s" class="dropdown-item"><i class="fa fa-pencil"></i> %s </a>', route("edit.users", [$row->id]), "Edit");
                $actions .= sprintf('<a href="%s" class="dropdown-item"><i class="fa fa-key"></i> %s </a>', route("users.change.password", [$row->id]), "Change Password");
            else:
                $name_with_edit_link = $row->name;
            endif;
            if (Gate::allows("delete-user")):
                $actions .= sprintf('<a href="%s" class="dropdown-item delete-row" data-pk="%s"><i class="fa fa-trash"></i> %s </a>', route("delete.users", [$row->id]), $row->id, "Delete");
            endif;


            $actions .= '</div>' .
                '</div>';
            
            $temp = [
                $name_with_edit_link,
                $row->email,
                $row->phone,
                $roles->implode(','),
                $row->address,
                $row->is_active? 'Active':'Disable',
                $actions
            ];

            array_push($data['data'], $temp);
        }

        return $data;
    }

}
