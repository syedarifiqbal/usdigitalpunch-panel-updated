<?php

namespace App\Http\Controllers\Datatables;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use DB;
use Gate;
use \App\Models\Client;
use Illuminate\Pagination\Paginator;

class ClientTableController extends Controller
{
    
    private $request;

    /**
     * Inject Request variable when initiated by system
     */
    function __construct(Request $request)
    {
        $this->request = $request;
    }
    
    /**
     * Get User data for user data table
     * @param boolean is_active
     */
    function getData()
    {
        $currentPage = ($this->request->input('start')/$this->request->input('length'))+1;

        Paginator::currentPageResolver(function () use ($currentPage) { return $currentPage; });
    
        // Search value send from datatables
        $term = $this->request->input('search')['value'];

        // column defination what we need to select
        $columns = ['id', 'name', 'email', 'is_active', 'phone', 'address'];

        // query to elequent model
        $query = Client::select($columns)
            ->where('company_id', session('company_id'))
            // ->where('is_active', (request('status', 'active') == 'disable') ? 0:1)
            ->where(function ($query) use($term) {
                $query->orwhere('name', 'like', "%$term%")
                      ->orwhere('nick_name', 'like', "%$term%")
                      ->orwhere('email', 'like', "%$term%")
                      ->orwhere('address', 'like', "%$term%")
                      ->orwhere('phone', 'like', "%$term%");
            });

        if ($this->request->input('order')) {
            $colIndex       = $this->request->input('order')[0]['column'];
            $colDirection   = $this->request->input('order')[0]['dir'];
            $query->orderBy($columns[$colIndex], $colDirection);
        } else {
            $query->orderBy('id', 'desc');
        }
        $query->with('contacts');
        // return $query->get()->first()->roles->map(function ($roles) {
        //     return $roles->name;
        // });
        // return $query->get();
        return $this->prepareData($query->paginate($this->request->input('length')));
    }

    public function prepareData($paginate)
    {
        
        $data = [
            "draw"              => intval($this->request->input("draw")),
            // "recordsTotal"      => Client::where('is_active', (request('status', 'active') == 'disable') ? 0:1)->count(),
            "recordsTotal"      => Client::count(),
            "recordsFiltered"   => $paginate->total(),
            "data"              => []
        ];
        
        foreach($paginate->items() as $row)
        {
            $contacts = $row->contacts->map(function ($contacts)
            {
                return $contacts->email . '(' . $contacts->name . ')';
            });

            $temp = [
                sprintf('<a href="%s"><i class="mdi mdi-pencil"></i> %s </a>', route("clients.edit", [$row->id]), $row->name),
                $row->email,
                $row->phone,
                $contacts->implode(','),
                $row->address,
                $row->is_active? 'Active':'Disable',
                $this->makeActionLinks($row),
            ];

            array_push($data['data'], $temp);
        }

        return $data;
    }

    protected function makeActionLinks($row)
    {
        $actions = '<div class="btn-group">
                        <button type="button" class="btn btn-danger" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                            <i class="mdi mdi-menu"></i>
                        </button>
                        <div class="dropdown-menu">';
     
        if (Gate::allows("edit-client")):
        
            $actions .= sprintf('<a href="%s" class="dropdown-item"><i class="mdi mdi-%s"></i> Edit</a>', route("clients.edit", [$row->id]), "pencil");
        
        else:
        
            $name_with_edit_link = $row->name;
        
        endif;

        if (Gate::allows("delete-client")):

            $actions .= sprintf('<a href="%s" class="dropdown-item" data-pk="%s"><i class="mdi mdi-delete"></i> Delete</a>', route("clients.delete", [$row->id]), $row->id);
            
        endif;

        $actions .= '</div>
                    </div>';

        return $actions;
    }

}
