<?php

namespace App\Http\Controllers\Datatables;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use DB;
use Illuminate\Support\Facades\Gate;
use \App\Models\Task;
use Illuminate\Pagination\Paginator;

class TasksTableController extends Controller
{
    
    private $request;

    /**
     * Inject Request variable when initiated by system
     */
    function __construct(Request $request)
    {
        $this->request = $request;
    }

    /**
     * Get User data for user data table
     * @param boolean is_active
     * @return array
     */
    function getData()
    {
        $currentPage = ($this->request->input('start')/$this->request->input('length'))+1;

        Paginator::currentPageResolver(function () use ($currentPage) { return $currentPage; });
    
        // Search value send from datatables
        $term = $this->request->input('search')['value'];

        // column defination what we need to select
        $columns = ['id', 'date', 'name', 'parent_id', 'design_type', 'client_id', 'designer_id', 'order_type', 'po_number', 'priority', 'status'];

        // query to eloquent model
        $query = Task::select($columns)
            ->where('company_id', session('company_id', 1))
            ->where(function($q) use($term)
            {
                $q->where('name', 'like', "%$term%")
                ->orwhere('date', 'like', "%$term%")
                ->orwhere('design_type', 'like', "%$term%")
                ->orwhere('po_number', 'like', "%$term%")
                ->orwhere('priority', 'like', "%$term%")
                ->orwhere('status', 'like', "%$term%");
            });

        if(in_array(request('design_type'), ['digitizing', 'vector', 'quote']))
        {
            $query->where('design_type', request('design_type'));
        }

        if(in_array(request('status'), ['pending', 'complete', 'rfs', 'progress']))
        {
            $query->where('status', request('status'));
        }

        if(request('unsigned_tasks'))
        {
            if (request('unsigned_tasks') == 'true') 
            {
                $query->where('designer_id',  null);
            }

            if (request('unsigned_tasks') == 'false')
            {
                $query->where('designer_id', '>', 0);
            }
        }

        if ($this->request->input('order')) 
        {
            $colIndex       = $this->request->input('order')[0]['column'];
            $colDirection   = $this->request->input('order')[0]['dir'];
            $query->orderBy($columns[$colIndex], $colDirection);
        } else {
            $query->orderBy('id', 'desc');
        }
        
        $query->with([
            'client' => function ($q) {$q->select('id', 'name', 'nick_name', 'is_active'); },
            'designer' => function ($q) {$q->select('id', 'name'); },
        ]);
        
        $this->totalRecords = $query->count();

        return $this->prepareData($query->paginate($this->request->input('length')));
    }

    public function prepareData($paginate)
    {   
        $data = [
            "draw"              => intval($this->request->input("draw")),
            "recordsTotal"      => $this->totalRecords,
            "recordsFiltered"   => $paginate->total(),
            "data"              => []
        ];
        
        foreach($paginate->items() as $row)
        {
            $assignTask = optional($row->designer)->name;

            if (Gate::allows('can-assign-task')):
                $assignTask = $assignTask ?
                    $assignTask . '<a class="btn btn-mini btn-icon btn-primary waves-effect waves-light" href="' . route('tasks.assign', ['task' => $row->id]) . '" title="Re-asign to other user"><i class="icofont icofont-refresh"></i></a>'
                    : '<a class="btn btn-mini btn-primary waves-effect waves-light" href="' . route('tasks.assign', ['task' => $row->id]) . '">Assign to designer</a>';
            endif;
            
            $bgColor = '';

            if($row->priority == 'Urgent') 
                $bgColor = "badge badge-warning"; 

            if($row->priority == 'Most Urgent' || !$row->client->is_active)
                $bgColor = 'badge badge-danger';
                
            $temp = [
                $row->id,
                $row->date->format('d/m/Y'),
                sprintf('<a href="%s" title="%s"><i class="icofont icofont-eye"></i> %s </a>', route("tasks.show", [$row->id]), $row->name, $row->name),
                $row->client->nick_name,
                $row->parent_id,
                $row->order_type,
                $row->po_number,
                '<span class="'. $bgColor .'">' . ($row->client->is_active? $row->priority: 'On Hold') . '</span>',
                $assignTask,
                $row->status,
                $this->makeActionLinks($row),
            ];

            array_push($data['data'], $temp);
        }

        return $data;
    }

    protected function makeActionLinks($row)
    {
        $actions = '<div class="btn-group">
                        <button type="button" class="btn btn-danger" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                            <i class="mdi mdi-menu"></i>
                        </button>
                        <div class="dropdown-menu">';
     
        if (Gate::allows("edit-task")):
            $actions .= sprintf('<a href="%s" class="dropdown-item"><i class="mdi mdi-%s"></i> Edit</a>', route("tasks.edit", [$row->id]), "pencil");
        else:
            $name_with_edit_link = $row->name;
        endif;
     
        if (strtolower($row->status) == 'pending'):
            $actions .= sprintf('<a href="%s" class="dropdown-item"><i class="mdi mdi-%s"></i> Submit Order</a>', route("tasks.submit", [$row->id, 'submit' => 'submit']), "check-all");
        endif;

        if (Gate::allows("send-task") && strtolower($row->status) == 'rfs'):
            $actions .= sprintf('<a href="%s" class="dropdown-item"><i class="mdi mdi-send"></i> Send to Customer</a>', route("tasks.review", [$row->id]), $row->id);
        endif;

        if (strtolower($row->status) == 'complete'):
            if (Gate::allows("can-make-edit-or-revision")):
            $actions .= sprintf('<a href="%s" class="dropdown-item"><i class="mdi mdi-replay"></i> Revision/Edit</a>', route("tasks.revise", [$row->id]), $row->id);
            endif;
            if (Gate::allows("send-task")):
            $actions .= sprintf('<a href="%s" class="dropdown-item"><i class="mdi mdi-send"></i> Resend to Customer</a>', route("tasks.review", [$row->id]), $row->id);
            endif;
        endif;

        if (strtolower($row->status) == 'complete'):
            if (Gate::allows("can-change-order-type")):
            $actions .= sprintf('<a href="%s" class="dropdown-item"><i class="mdi mdi-replay"></i> Change order type</a>', route("tasks.changeOrderType", [$row->id]), $row->id);
            endif;
        endif;

        if (Gate::allows("delete-task")):
            $actions .= '<div class="dropdown-divider"></div>';
            $actions .= sprintf('<a href="%s" class="dropdown-item" data-pk="%s"><i class="mdi mdi-delete"></i> Delete</a>', route("tasks.delete", [$row->id]), $row->id);
        endif;

        $actions .= '</div>
                    </div>';

        return $actions;
    }

}
