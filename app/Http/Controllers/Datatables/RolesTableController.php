<?php

namespace App\Http\Controllers\Datatables;

use DB;
use Gate;
use App\Models\Role;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Pagination\Paginator;

class RolesTableController extends Controller
{
    
    private $request;

    /**
     * Inject Request variable when initiated by system
     */
    function __construct(Request $request)
    {
        $this->request = $request;
    }
    
    /**
     * Get User data for user data table
     * @param boolean is_active
     */
    function getData()
    {
        $currentPage = ($this->request->input('start')/$this->request->input('length'))+1;

        Paginator::currentPageResolver(function () use ($currentPage) { return $currentPage; });
    
        // Search value send from datatables
        $term = $this->request->input('search')['value'];

        // column defination what we need to select
        $columns = ['id', 'name', 'email', 'is_active', 'phone', 'address'];

        // query to elequent model
        $query = Role::select($columns)
                    ->where('name', 'like', "%$term%")
                    ->orwhere('description', 'like', "%$term%");

        if ($this->request->input('order')) {
            $colIndex       = $this->request->input('order')[0]['column'];
            $colDirection   = $this->request->input('order')[0]['dir'];
            $query->orderBy($columns[$colIndex], $colDirection);
        } else {
            $query->orderBy('id', 'desc');
        }
        return $this->prepareData($query->paginate($this->request->input('length')));
    }

    public function prepareData($paginate)
    {
        $data = [
            "draw"              => intval($this->request->input("draw")),
            "recordsTotal"      => Role::count(),
            "recordsFiltered"   => $paginate->total(),
            "data"              => []
        ];
        
        foreach($paginate->items() as $row)
        {
            $actions = '<div class="btn-group">' .
                    '<button data-toggle="dropdown" class="dropdown-toggle btn btn-icon-toggle btn-danger btn-sm">
                        <i class="fa fa-ellipsis-h "></i>
                    </button>
                    <ul class="dropdown-menu">';
            if (Gate::allows("edit-user")):
                $name_with_edit_link = sprintf('<a href="%s"><i class="fa fa-pencil"></i> %s </a>', route("edit.users", [$row->id]), $row->name);
                $actions .= sprintf('<li><a href="%s"><i class="fa fa-pencil"></i> %s </a></li>', route("edit.users", [$row->id]), "Edit");
            else:
                $name_with_edit_link = $row->name;
            endif;
            if (Gate::allows("edit-user")):
                $actions .= sprintf('<li><a href="%s" class="delete-row" data-pk="%s"><i class="fa fa-trash"></i> %s </a></li>', route("delete.users"), $row->id, "Delete");
            endif;


            $actions .= '</ul>' .
                '</div>';
            
            array_push($data['data'], [
                    $row->id,
                    $name_with_edit_link,
                    $row->description, 
                    $actions
                ]);
        }

        return $data;
    }

}
