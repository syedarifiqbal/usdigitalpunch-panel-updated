<?php

namespace App\Http\Controllers\Sales;

use App\Models\Client;
use App\Models\Company;
use App\Models\Sales\Lead;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Http\Response;

class LeadsController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return Response
     */
    public function index()
    {
        $leads = auth()->user()->leads()->withCount('calls')->get();
        return view('sales.leads.index', ['leads' => $leads]);
    }

    /**
     * Display a listing of the resource.
     *
     * @return Response
     */
    public function callingList()
    {
        $leads = Lead::doesntHave('calls')->get();
        return view('sales.leads.index', ['leads' => $leads, 'isCallingList' => true]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return Response
     */
    public function create()
    {
        return view('sales.leads.create', ['lead' => new Lead()]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @return Response
     * @throws \Exception
     */
    public function store(Request $request)
    {
        $this->validateFields($request);
        $lead = new Lead($request->all());
        auth()->user()->leads()->save($lead);
        $lead->save();
        activity_log("Created new Lead ({$lead->name})", $lead);
        return redirect()->route('leads.index')->with('message', 'Leads Created Successfully');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return Response
     */
    public function show(Lead $lead)
    {
        $lead->load('calls.owner');
        return view('sales.leads.show', compact('lead'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  Lead $lead
     * @return Response
     */
    public function edit(Lead $lead)
    {
        return view('sales.leads.create', ['lead' => $lead]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @param  Lead $lead
     * @return Response
     * @throws \Exception
     */
    public function update(Request $request, Lead $lead)
    {
        $this->validateFields($request);
        $lead->fill($request->all());
        $lead->save();
        activity_log("{$lead->name} Updated", $lead);
        return redirect()->route('leads.index')->with('message', 'Leads Updated Successfully');
    }

    protected function validateFields(Request $request)
    {
        $request->validate([
            'company' => 'required',
            'email' => 'required',
            'state' => 'required',
            'city' => 'required',
            'country_id' => 'required|exists:countries,id',
        ]);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  Lead $lead
     * @return Response
     * @throws \Exception
     */
    public function destroy(Lead $lead)
    {
        $lead->delete();
        activity_log("{$lead->name} Deleted!", $lead);
        return redirect()->route('leads.index')->with('message', 'Leads Deleted Successfully');
    }

    /**
     * Move Lead to Clients List and softDelete from Leads.
     *
     * @param  Lead $lead
     * @return Response
     * @throws \Exception
     */
    public function moveToCustomer(Lead $lead)
    {
        $client = new Client();
        $client->name = $lead->name;
        $client->nick_name = $lead->name;
        $client->company = $lead->company;
        $client->email = $lead->email;
        $client->country_id = $lead->country_id;
        $client->phone = $lead->phone;
        $client->address = $lead->address;
        $client->instructions = $lead->description;
        $client->secret_instructions = '';
        $client->company_id = Company::first()->id;
        $client->is_active = 1;
        $client->from_lead = 1;

        auth()->user()->clients()->save($client);

        $lead->delete();

        activity_log("{$lead->name} moved to clients!", $lead);
        return redirect()->route('leads.index')->with('message', 'Leads moved to clients successfully');
    }

    function select2()
    {
        $record = Lead::where('name', 'LIKE', '%' . request('q') . '%')
            ->select(['name AS name', 'id AS id'])->paginate(10);
        return $record;
    }
}
