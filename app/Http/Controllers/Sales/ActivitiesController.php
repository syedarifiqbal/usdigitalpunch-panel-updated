<?php

namespace App\Http\Controllers\Sales;

use App\Models\Sales\Call;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class ActivitiesController extends Controller
{
    public function index()
    {
        $calls = Call::with('owner')->get();
        return view('sales.activities.index', compact('calls'));
    }
}
