<?php

namespace App\Http\Controllers\Sales;

use App\Models\Sales\Call;
use App\Models\Sales\Lead;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class CallsController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $leads = Lead::doesntHave('calls')->get();
        return view('sales.calls.index', ['leads' => $leads]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @param Lead $lead
     * @return \Illuminate\Http\Response
     */
    public function create(Lead $lead)
    {
        return view('sales.calls.create', ['call' => new Call(), 'lead' => $lead]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @param  Lead $lead
     * @return \Illuminate\Http\Response
     * @throws \Exception
     */
    public function store(Request $request, Lead $lead)
    {
        $this->validateFields($request);
        $call = new Call($request->all());
        $call->lead()->associate($lead);
        auth()->user()->leads()->save($call);
        activity_log("Created new Lead ({$call->subject})", $call);
        return redirect()->route('leads.calls.index', ['lead' => $lead->id])->with('message', 'Call Created Successfully');
    }

    /**
     * Display the specified resource.
     *
     * @param  Call $call
     * @return \Illuminate\Http\Response
     */
    public function show(Call $call)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit(Call $call)
    {
        return view('sales.calls.index', ['call' => $call]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Call $call)
    {
        $this->validateFields($request);
        $call->fill($request->all());
        $call->save();
    }

    protected function validateFields(Request $request)
    {
        $request->validate([
            'subject' => 'required',
            'lead_id' => 'required|exists:leads,id',
            'duration' => 'required',
            'feedback' => 'required',
            'details' => 'required',
        ]);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  Call $call
     * @return \Illuminate\Http\Response
     */
    public function destroy(Call $call)
    {
        //
    }
}
