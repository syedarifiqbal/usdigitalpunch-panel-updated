<?php

namespace App\Http\Controllers;

use App\Models\TaskAttachment;
use Illuminate\Http\Request;
use App\Models\Task;
use Illuminate\Support\Facades\Storage;

class TasksController extends BaseController
{
    protected $menu = 'tasks';

    public function index()
    {
        allowedOrRedirect('view-task');

        $data['pageTitle'] = 'Orders';

        $data['breadcrumb'] = 'tasks.with.design.type';

        $data['menu'] = $this->menu;

        $data['submenu'] = 'view-task';

        // $f = \App\Models\TaskAttachment::orderBy('id', 'desc')->first();

        // dd($_ENV, config('filesystems.disks.s3.url'), $f->path);

        // return '<img src="' . \Storage::disk('s3')->url($f->path) . '">';

        return view('tasks.index', $data);
    }

    public function create()
    {
        allowedOrRedirect('add-task');
        $data['menu'] = $this->menu;
        $data['submenu'] = 'add-task';
        $data['pageTitle'] = 'Orders';
        $data['breadcrumb'] = 'tasks.create';
        $data['placements'] = $this->getAvailablePlacements();
        $data['allowedFormats'] = $this->getAllowedFormats();
        $data['record'] = new Task;
        return view('tasks.create', $data);
    }

    public function store(Request $request)
    {
        allowedOrRedirect('add-task');
        $request->validate($this->getValidationRoles(false, $request));
        \DB::transaction(function() use($request){
            $task = new Task($request->all());
            $task->price = $task->price??0;
            $task->company_id = session('company_id');
            $task->save();
            $task->addAttachments($request);
            activity_log("New Task Created {$task->name}", $task);
        });
        flash('Order Created Successfully', FLASH_SUCCESS);
        return redirect()->route('tasks');
    }

    public function show(Task $task)
    {
        if (optional($task->designer)->id != auth()->id())
            allowedOrRedirect('view-task');
        // $data['roleUsers'] = \App\Models\Permission::where('name', 'add-user')->first()->roles->pluck('users')->first()->toArray();
        readNotification();
        $data['pageTitle'] = 'Orders';
        $data['menu'] = $this->menu;
        $data['submenu'] = 'view-task';
        $data['breadcrumb'] = 'tasks.show';
        $data['breadcrumbData'] = $task;
        $data['task'] = $task;
        return view('tasks.show', $data);
    }

    public function edit(Task $task, $submit = false)
    {
        if (!$submit)
            allowedOrRedirect('edit-task');
        $data['pageTitle'] = 'Orders';
        $data['menu'] = $this->menu;
        $data['submenu'] = 'view-task';
        $task->load(['attachments', 'client']);
        $data['breadcrumb'] = $submit? 'tasks.submit':'tasks.edit';
        $data['breadcrumbData'] = $task;
        $data['placements'] = $this->getAvailablePlacements();
        $data['allowedFormats'] = $this->getAllowedFormats();
        $data['record'] = $task;
        $data['task'] = $task;
        
        return view($submit? "tasks.submit":"tasks.create", $data);
    }

    public function update(Request $request, Task $task, $submit = false)
    {
        // check if submitter is not designer than check the permission
        if($task->designer_id != auth()->id())
        {
            allowedOrRedirect('edit-task');
        }

        // validate the request
        $request->validate($this->getValidationRoles($submit, $request));

        \DB::transaction(function() use($request, $task, $submit){

            $task->fill($request->all());

            $task->price = $task->price ?? 0;

            if($submit && $task->status != 'Complete'){ $task->status = 'RFS'; }

            $task->save();

            $task->addAttachments($request, !!$submit);

            activity_log($submit ? "Task {$task->name} Submitted": "Task {$task->name} Modified", $task);

        });

        if($submit)
        {
            \Notification::send(\App\User::findByPermission('send-to-customer'), new \App\Notifications\TaskSubmitted($task));
        }

        flash('Order Updated Successfully', FLASH_SUCCESS);

        return redirect()->route('tasks');
    }

    public function destroy(Task $task)
    {
        allowedOrRedirect('delete-task');
        try {
            foreach ($task->attachments as $file)
            {
                Storage::delete($file->path);
            }
            // delete edit deduction from parent job
            if(strtolower($task->order_type) === 'edit')
            {
                $previousBonus = $task->parent->bonus()->where('rs', '<', 0)->first();
                if($previousBonus) { $previousBonus->delete(); }
            }
            $task->bonus()->delete();
            $task->attachments()->delete();
            $task->delete();
            flash('Order Deleted Successfully', FLASH_SUCCESS);
        } catch (\Exception $e) {
            flash('There are some error while deleting order.', FLASH_DANGER);
        }
        return redirect()->route('tasks');
    }

    public function review(Task $task)
    {
        allowedOrRedirect('send-task');
        $data['menu'] = $this->menu;
        $data['pageTitle'] = 'Orders';
        $data['breadcrumb'] = 'tasks.review';
        $data['breadcrumbData'] = $task;
        $data['placements'] = $this->getAvailablePlacements();
        $data['allowedFormats'] = $this->getAllowedFormats();
        $data['task'] = $task;
        $data['checkers'] = \App\User::findByPermission('approve-task')->pluck('name', 'id');
        return view('tasks.review', $data);
    }

    public function reviewStore(Request $request, Task $task)
    {
        allowedOrRedirect('send-task');
        $roles = [
            'price' => 'required|numeric',
            // 'files' => 'required|array'
        ];
        // check the exisiting user id in case checker is included.
        if( request('checker') ) { $roles['checker'] = 'exists:users,id'; }
        // Flag Weather or not event should fire
        $shouldEventOccur = true;
        // Disable Event firing in case order is resending to client.
        if( $task->status == STATUS_COMPLETE ) { $shouldEventOccur = false; }
        // Validate the request params
        $request->validate($roles);
        // Fill the Task with request Params

        $task->fill(request()->all());
        // Setting Order Status to Complete
        $task->status = STATUS_COMPLETE;
        // Save the order details.
        $task->save();

        $task->load('designer');
        // Log Activity
        try {
            activity_log("Task {$task->name} Send to {$task->client->name} and marked as completed", $task);
        } catch (\Exception $e) {
            logger($e->getMessage());
        }
        // Notify the client (sending email to client)
        $task->client->notify(new \App\Notifications\TaskCompleted($task, request('files')));
        // Check and fire event if it should be.
        if($shouldEventOccur)
        {
            $data = new \stdClass;
            $data->task = $task;
            $data->currency = request('currency');
            $data->checker = request('checker');
            $data->additional_bonus = request('additional_bonus', 0);
            $data->files = request('files');
            event(new \App\Events\TaskCompleted($data));
        }
        flash('Task Send to customer Successfully', FLASH_SUCCESS);
        return redirect()->route('tasks');
    }

    public function assign(Task $task)
    {
        allowedOrRedirect('can-assign-task');
        $data['pageTitle'] = 'Orders';
        $data['menu'] = $this->menu;
        $data['breadcrumb'] = 'tasks.edit';
        $data['breadcrumbData'] = $task;
        $data['placements'] = $this->getAvailablePlacements();
        $data['allowedFormats'] = $this->getAllowedFormats();
        $data['task'] = $task;
        return view('tasks.assign', $data);
    }

    public function storeAssign(Request $request, Task $task)
    {
        allowedOrRedirect('can-assign-task');
        $request->validate(['designer' => 'required|exists:users,id']);
        $task->designer_id = request('designer');
        $task->save();
        $task->load('designer');
        activity_log("Task {$task->name} Assigned to {$task->designer->name}", $task);
        $task->designer->notify(new \App\Notifications\TaskAssigned($task));
        // event(new \App\Events\TaskAssigned($task->designer, $task));
        flash('Task Assigned Successfully', FLASH_SUCCESS);
        return redirect()->route('tasks');
    }

    public function revise(Task $task)
    {
        if($task->status != STATUS_COMPLETE)
        {
            flash("You cannot revise task unless it completed", FLASH_WARNING);
            return redirect()->route('tasks');
        }
        $data['menu'] = $this->menu;
        $data['pageTitle'] = 'Orders';
        $data['breadcrumb'] = 'tasks.edit';
        $data['breadcrumbData'] = $task;
        $data['placements'] = $this->getAvailablePlacements();
        $data['allowedFormats'] = $this->getAllowedFormats();
        $data['task'] = $task;
        return view('tasks.revise', $data);
    }

    public function reviseStore(Request $request, Task $task)
    {
        $request->validate(['rejection_level' => 'required']);
        $reject = new Task();
        $data = $task->toArray();
        $data['date'] = now()->format('d/m/Y');
        $data['delivery_date'] = $task->delivery_date->format('d/m/Y');
        $reject->fill($data);
        $reject->name .= ' / ' . $task->id;
        $reject->parent_id = $task->id;
        $reject->order_type = request('rejection_level');
        $reject->status = 'Pending';
        $reject->remarks = request('remarks');
        $reject->designer_id = null;
        $reject->created_at = now()->toDateTimeString();
        $reject->save();

        $reject->addAttachments($request);

        activity_log("{$task->order_type} of {$task->name} created", $reject);
        flash("{$task->order_type} Created Successfully", FLASH_SUCCESS);

        /*dd($task->toArray(), $reject->toArray());*/

        if ($reject->order_type === 'Edit')
        {
            event(new \App\Events\TaskRejected($task));
        }
        return redirect()->route('tasks');
    }

    public function changeOrderType(Task $task)
    {
        if(!\Gate::allows("can-make-edit-or-revision"))
        {
            flash("You cannot change task type on completed Task", FLASH_WARNING);
            return redirect()->route('tasks');
        }
        
        $data['menu'] = $this->menu;
        $data['pageTitle'] = 'Orders';
        $data['breadcrumb'] = 'tasks.edit';
        $data['breadcrumbData'] = $task;
        $data['task'] = $task;
        return view('tasks.change_order_type', $data);
    }

    public function changeOrderTypeStore(Request $request, Task $task)
    {
        $request->validate(['order_type' => 'required']);
        if(!\Gate::allows("can-make-edit-or-revision"))
        {
            flash("You cannot change task type on completed Task", FLASH_WARNING);
            return redirect()->route('tasks');
        }
        
        $deductedBonus = null;
        // check if order is edit then remove parent order's edit deduction
        if(strtolower($task->order_type) == 'edit')
            $deductedBonus = $task->parent->bonus()->where('rs', '<', 0)->first();
        // if there is deduction then delete it
        if($deductedBonus)
            $deductedBonus->delete();
        
        // check if order is revision then deduct bonus against parent's designer
        if(strtolower($task->order_type) == 'revision'){
            event(new \App\Events\TaskRejected($task->parent));
        }
        
        $design_type = $task->order_type;
        $task->order_type = $request->order_type;
        $task->save();
        try {
            activity_log("{$task->name} is changed order type from {$design_type} of {$task->order_type} created", $task);
            flash("{$task->order_type} Created Successfully", FLASH_SUCCESS);
        } catch (\Exception $e) {
            dd($e);
        }
        return redirect()->route('tasks');
    }

    public function deleteSubmittedFile(Task $task, TaskAttachment $file)
    {
        // return 'we are here';
        Storage::disk('s3')->delete($file->path);
        try {
            $file->delete();
            return ['status' => true, 'message'=> 'File Deleted Successfully!'];
        } catch (\Exception $e) {
            dd($e->getMessage());
        }
        return ['status' => false, 'message'=> 'There may some problem.'];
    }

    protected function getValidationRoles($submit, $request)
    {
        if($submit) {
            return [
                'delivery_width' => 'required',
                'delivery_height' => 'required',
                'stitches' => 'required',
                // 'uploads' => 'required',
            //    'uploads.*' => 'mimes:' . str_replace(['.','|'], ['',','], setting('allowed_extension', 'jpeg,bmp,png')),
            ];
        }

        $roles = [
            'name' => 'required',
            'date' => 'required|date_format:"d/m/Y"',
            'delivery_date' => 'required|date_format:"d/m/Y"',
            'required_format' => 'required|array',
            'client_id' => 'required',
            'design_type' => 'required',
            'placement' => 'required',
            'unit' => 'required'
        ];

        $extensions = explode('|', str_replace('.', '', setting('allowed_extension', 'jpeg,bmp,png')));

        if(is_array($request->uploads))
        {
            foreach($request->uploads as $file)
            {
                if(!in_array(strtolower($file->getClientOriginalExtension()), $extensions))
                {
                    $roles['uploads.*'] = 'mimes:' . str_replace(['.','|'], ['',','], setting('allowed_extension', 'jpeg,bmp,png'));
                    return $roles;
                }
            }
        }

        return $roles;
    }

    protected function getAvailablePlacements()
    {
        return [
            'Left Chest' => 'Left Chest',
            'Jacket Back' => 'Jacket Back',
            'Hat Front' => 'Hat Front',
            'Hat Back' => 'Hat Back',
            'Apran' => 'Apran',
            'Towal' => 'Towal',
            'Cap Bennies' => 'Cap Bennies',
            'Back Yoke' => 'Back Yoke',
        ];
    }

    protected function getAllowedFormats()
    {
        return [
            '.dst' => '.dst',
            '.emb' => '.emb',
            '.jef' => '.jef',
            '.cnd' => '.cnd',
            '.exp' => '.exp',
            '.pes' => '.pes',
            '.jan' => '.jan',
            '.dsz' => '.dsz',
            '.pxf' => '.pxf',
            '.ofm' => '.ofm',
            '.psf' => '.psf',
            '.ai'  => '.ai',
            '.eps' => '.eps',
            '.cdr' => '.cdr',
            '.pdf' => '.pdf',
        ];
    }
}
