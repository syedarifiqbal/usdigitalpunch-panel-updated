<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class NotificationsController extends BaseController
{
    public function index()
    {
        return auth()->user()->notifications()->latest()->limit(5)->get();
    }

}
