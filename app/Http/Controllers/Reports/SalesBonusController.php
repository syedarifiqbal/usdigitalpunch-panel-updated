<?php

namespace App\Http\Controllers\Reports;

use App\Http\Controllers\BaseController;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use PDF;
use \App\Models\Task;
use \App\Models\Bonus;
use \App\User;
use \Carbon\Carbon;

class SalesBonusController extends BaseController
{
    protected $menu = 'reports';

    public function index()
    {
        $data['menu'] = $this->menu;
        $data['submenu'] = 'sales-bonus';
        return view('reports.sales_bonus_form', $data);
    }
    
    public function pdf(Request $request)
    {
        $request->validate(['user_id' => 'required:exists.users']);
        $user = User::find(request('user_id'));

        $orders = Task::whereBetween('date', [request('from'), request('to')])
            ->where('order_type', 'new')
            ->whereHas('client', function($q){
            $q->where('referred_by', request('user_id'));
        })->get();

        $data['bonuses'] = $orders;
        $data['user'] = $user;
        $data['from'] = $request->from;
        $data['to'] = $request->to;

        return view('reports.sales_bonus', $data);

        $pdf = PDF::loadView('reports.sales_bonus', $data);
    
        return $pdf->stream();
    }

    public function temp_bonus(Request $request)
    {
        $records = \DB::select("SELECT users.name, t_count.order_type, t_count.order_count FROM `users` JOIN (
                    SELECT t.designer_id, COUNT(t.id) order_count, t.order_type FROM tasks AS t WHERE DATE_FORMAT(t.created_at, '%Y-%m-%d') BETWEEN ? AND ? GROUP BY t.designer_id, t.order_type
                ) AS t_count ON users.id = t_count.designer_id
        ", ['2019-01-01', '2019-01-31']);
        return view('reports.temp_bonus', compact('records'));
    }
}
