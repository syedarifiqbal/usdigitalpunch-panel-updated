<?php

namespace App\Http\Controllers\Reports;

use Illuminate\Http\Request;
use \App\Models\Role;
use \App\Models\Permission;
use \App\Http\Controllers\BaseController;
use \App\Models\Task;

class ReportController extends BaseController
{
    protected $menu = 'report';

    public function today_tasks()
    {
        allowedOrRedirect('view-today-tasks-list');

        $data['today_tasks'] = Task::with('client.country')->whereRaw('Date(created_at) = CURDATE()')->get();

        $data['menu'] = $this->menu;

        $data['submenu'] = 'today-task';

        return view('reports.today_tasks', $data);
    }
    
}
