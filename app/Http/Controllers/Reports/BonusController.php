<?php

namespace App\Http\Controllers\Reports;

use App\Http\Controllers\BaseController;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use PDF;
use \App\Models\Task;
use \App\Models\Bonus;
use \App\User;
use \Carbon\Carbon;

class BonusController extends BaseController
{
    protected $menu = 'reports';

    public function index()
    {
        $data['menu'] = $this->menu;

        $data['submenu'] = 'view-bonus';

        return view('reports.bonus_filters', $data);
    }

    public function pdf(Request $request)
    {
        $request->validate(['user_id' => 'required:exists.users']);

        $user = User::find(request('user_id'));

        $from = Carbon::createFromFormat('d/m/Y', request('from_date', date('d/m/Y', strtotime('first day of this month'))));
        $to = Carbon::createFromFormat('d/m/Y', request('to_date', date('d/m/Y', strtotime('last day of this month'))));

        /*$bonuses = \DB::table('bonuses as b')
            ->join('tasks as t', 't.id' , '=', 'b.task_id')
            ->select('b.*', 't.date', 't.name', 't.design_type', 't.order_type')
            ->where('b.user_id', $user->id)
            ->whereBetween('t.date', [$from, $to])
            ->get();*/

        $bonuses = $user->bonuses()
            ->with(['task' => function($q){
                $q->select('id', 'date', 'name', 'design_type', 'order_type');
            }])->whereHas('task', function($query) use($from, $to){
                $query->select('id');
                $query->whereBetween('date', [$from->format('Y-m-d'), $to->format('Y-m-d')]);
                // if (request('type', 'All') !== 'All'){}
                    // $query->where('order_type', request('type'));
            })->get();


        // return view('old.welcome');
        $data['bonuses'] = $bonuses;
        $data['user'] = $user;
        $data['from'] = $from;
        $data['to'] = $to;
        $data['type'] = request('type');

        // dd($user);

        return view('reports.bonus', $data);

        $pdf = PDF::loadView('reports.bonus', $data);

        return $pdf->stream();
    }

    public function temp_bonus(Request $request)
    {
        /*$records = \DB::select("SELECT users.name, t_count.order_type, t_count.order_count FROM `users` JOIN (
                    SELECT t.designer_id, COUNT(t.id) order_count, t.order_type FROM tasks AS t WHERE DATE_FORMAT(t.created_at, '%Y-%m-%d') BETWEEN ? AND ? GROUP BY t.designer_id, t.order_type
                ) AS t_count ON users.id = t_count.designer_id
        ", ['2019-01-01', '2019-01-31']);*/


        $records = \DB::select("SELECT
            users.name,
            t_count.task_id,
            t_count.order_type,
            t_count.order_count,
            tip.tip
        FROM
            `users`
        JOIN(
            SELECT t.id AS task_id,
                t.designer_id,
                COUNT(t.id) order_count,
                t.order_type
            FROM
                tasks AS t
            WHERE
                DATE_FORMAT(t.created_at, '%Y-%m-%d') BETWEEN ? AND ?
            GROUP BY
                t.designer_id,
                t.order_type
        ) AS t_count
        ON
            users.id = t_count.designer_id
        LEFT JOIN(
            SELECT SUM(b.tip) AS tip,
                b.user_id
            FROM
                bonuses AS b
            WHERE
                tip > 0 AND b.task_id IN(
                SELECT
                    tt.id
                FROM
                    tasks as tt
                WHERE
                    DATE_FORMAT(tt.created_at, '%Y-%m-%d') BETWEEN ? AND ?
            )
        GROUP BY
            b.user_id
        ) AS tip
        ON
            tip.user_id = users.id AND t_count.order_type = ?", ['2019-01-01', '2019-01-31', '2019-01-01', '2019-01-31', 'new']);

        // return $records;
        return view('reports.temp_bonus', compact('records'));
    }
}
