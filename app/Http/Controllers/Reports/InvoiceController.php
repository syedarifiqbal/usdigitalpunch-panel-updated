<?php

namespace App\Http\Controllers\Reports;

use App\Http\Controllers\BaseController;
use App\Models\Client;
use App\Models\Task;
use Illuminate\Http\Request;
use PhpOffice\PhpSpreadsheet\Exception;
use PhpOffice\PhpSpreadsheet\IOFactory;
use PhpOffice\PhpSpreadsheet\Spreadsheet;
use PhpOffice\PhpSpreadsheet\Style\Alignment;
use PhpOffice\PhpSpreadsheet\Style\Border;
use PhpOffice\PhpSpreadsheet\Style\NumberFormat;
use PhpOffice\PhpSpreadsheet\Worksheet\MemoryDrawing;
use PhpOffice\PhpSpreadsheet\Writer\Xlsx;


class InvoiceController extends BaseController
{
    public function form(Request $request, $from_date = '', $end_date = '', $design_type = '')
    {
        $customers = $this->getCustomers();

        $totalRevision = Task::where('order_type', 'revision')->where('company_id', session('company_id'));
        $totalEdit = Task::where('order_type', 'edit')->where('company_id', session('company_id'));

        if ($from_date && $end_date){
            $totalRevision->whereBetween('created_at', [$from_date, $end_date]);
            $totalEdit->whereBetween('created_at', [$from_date, $end_date]);
        }

        if (request('design_type', 'all') != 'all'){
            $totalRevision->where('design_type', request('design_type'));
            $totalEdit->where('design_type', request('design_type'));
        }

        $data = [
                'customers' => $customers,
                'breadcrumb' => 'reports.invoice',
                'from_date' => $from_date,
                'end_date' => $end_date,
                'design_type' => $design_type,
                'totalRevision' => $totalRevision->count(),
                'totalEdit' => $totalEdit->count(),
            ];

        return view('reports.invoice_form', $data);
    }

    public function excel(Request $request, Client $client, $from_date = '', $end_date = '')
    {
        if ($from_date && $end_date)
            $orders = $client->orders()->where('company_id', session('company_id'))->whereBetween('date', [$from_date, $end_date])->get();
        else
            $orders = $client->orders()->where('company_id', session('company_id'))->get();

        $spreadsheet = new Spreadsheet();
        $spreadsheet->getProperties()
            ->setCreator("System Generated. (www.usdigitalpunch.com)")
            ->setLastModifiedBy("(www.usdigitalpunch.com)")
            ->setTitle("Us Digital Punch")
            ->setSubject("Invoice")
            ->setDescription("Item Invoice for " . '')
            ->setKeywords("Invoice")
            ->setCategory("Excel");
        $formats = [
            "USD" => '_("$"* #,##0.00_);_("$"* \(#,##0.00\);_("$"* "-"??_);_(@_)',
            "CA" => '_("$"* #,##0.00_);_("$"* \(#,##0.00\);_("$"* "-"??_);_(@_)',
            "NZD" => '_("$"* #,##0.00_);_("$"* \(#,##0.00\);_("$"* "-"??_);_(@_)',
            "EUR" => '_("€"* #,##0.00_);_("€"* \(#,##0.00\);_("€"* "-"??_);_(@_)',
            "GBP" => '_("£"* #,##0.00_);_("£"* \(#,##0.00\);_("£"* "-"??_);_(@_)',
        ];
        try {
            $spreadsheet->getDefaultStyle()->getAlignment()->setVertical(Alignment::VERTICAL_CENTER);
            $spreadsheet->getDefaultStyle()->getFont()->setName("Century");
            $spreadsheet->getDefaultStyle()->getFont()->setSize("11");
            $activeSheet = $spreadsheet->getActiveSheet();
            $activeSheet ->setShowGridlines(false);
            $activeSheet->getHeaderFooter()->setOddHeader("&C&B&16 " .
                $activeSheet->getTitle())
                ->setOddFooter("&CPage &P of &N");

            $objDrawing = new MemoryDrawing();
            $objDrawing->setName('Logo');
            $objDrawing->setDescription('Logo');
            $objDrawing->setImageResource(imagecreatefrompng(public_path('assets/images/invoice-'. session('company_id') .'-logo.png')));
            $objDrawing->setRenderingFunction(MemoryDrawing::RENDERING_PNG);
            $objDrawing->setMimeType(MemoryDrawing::MIMETYPE_DEFAULT);
            $objDrawing->setHeight(90);
            $objDrawing->setWorksheet($activeSheet);

            $objDrawing->setCoordinates('B2');

            $colWidths = array('A,j'=>2.36, 'B'=>10, 'H'=>12.91, 'I'=>17.91);
            foreach ($colWidths as $name => $w) {
                $splitedName = explode(',',str_replace(' ','',$name));
                if( count($splitedName) > 1){
                    foreach ($splitedName as $multicolumn) {
                        $activeSheet
                            ->getColumnDimension($multicolumn)
                            ->setWidth($w);
                    }
                }else{
                    $activeSheet
                        ->getColumnDimension($name)
                        ->setWidth($w);
                }
            }

            $activeSheet
//                ->setCellValue('H2', 'Address: 119 Chestnut Ave New Windsor New York 12553 USA')
                ->setCellValue('H2', 'Address: ' . setting('address_' . session('company_id')))
                ->mergeCells('H2:I3')
//                ->setCellValue('H4', 'Tele: 1-917-464-3882')
                ->setCellValue('H4', 'Tele: ' . setting('phone_' . session('company_id')))
                ->setCellValue('H5', 'Skype: ' . setting('skype_' . session('company_id')))
                ->setCellValue('H8', 'Invoice / Statement')
                ->mergeCells('H8:I8')
                ->setCellValue("B10", "To: ". $client->name)
                ->setCellValue("B11", "Country: ". $client->country->name)
                ->setCellValue("B12", "Company: ". $client->name)
                ->setCellValue("B13", "Address: ". $client->address)
                // ->setCellValue("B13", "")
                ->setCellValue("B14", "Email: ". $client->email)
                ->setCellValue('H12', 'Date:')
                ->setCellValue('H14', 'Invoice No:')
                ->setCellValue("B17", 'Date')
                ->setCellValue("C17", "Description")
                ->mergeCells("C17:F17")
                ->setCellValue("G17","Price")
                ->setCellValue("H17","Discount")
                ->setCellValue("I17","Amount")
                ->setCellValue('I12','=today()')
            ;

            $activeSheet->getStyle('B13')->getAlignment()->setWrapText(true);
            $activeSheet->getStyle('H2')->getAlignment()->setWrapText(true);
            $activeSheet->mergeCells('B13:F13');

            $col = "A";
            $row = 17;
            $currency = null;
            foreach($orders AS $order){
                $row++;
                $activeSheet->setCellValue("B".$row, $order->date->format('d/m/Y'));
                $activeSheet->setCellValue("C".$row, $order->name . ' ('.$order->order_type . ' ' . $order->design_type.')');
                $activeSheet->setCellValue("G".$row, $order->price);

                $f = $formats["USD"];
                if (array_key_exists($order->currency, $formats))
                {
                    $f = $formats[$order->currency];
                }

                $activeSheet->getStyle("G".$row)->getNumberFormat()->setFormatCode($f);
                $activeSheet->setCellValue("H".$row,0);
                $activeSheet->setCellValue("I".$row, sprintf("=SUM(%s,%s)",('G'.$row),('-H'.$row)));
                // $activeSheet->setCellValue("J".$row,$record->currency);
                $currency = $order->currency;

            }
            $row++;
            if($row<30) $row=30;

            // Setting cell format
            $activeSheet->getStyle('I12')
                ->getNumberFormat()
                ->setFormatCode(NumberFormat::FORMAT_DATE_DDMMYYYY);
            // Making Fonts Bold
            $boldCells = array("B10","H8","H12","H14","B17:I17", "H53:I53", "H54:I54","B".$row.":I".$row);

            foreach ($boldCells as $cell) {
                $activeSheet
                    ->getStyle($cell)->getFont()->setBold(true);
            }
            $borderCells = array("H7:I9",
                "H11:I15",
                "B9:F15",
                "B17:I17",
                "B17:B".$row,
                "C17:F".$row,
                "G17:G".$row,
                "H17:H".$row,
                "I17:I".$row,
                "H".($row+1).":I".($row+3),
                "H".($row+2).":I".($row+2));

            foreach ($borderCells as $cell) {
                $activeSheet
                    ->getStyle($cell)
                    ->applyFromArray($this->borderArray());
            };
            // Writing total value
            $activeSheet
                ->setCellValue("H".($row+1),'Subtotal')
                ->setCellValue("I".($row+1),sprintf("=SUM(G18:G%s)",$row))
                ->setCellValue("H".($row+2),'Discount')
                ->setCellValue("I".($row+2),sprintf("=SUM(H18:H%s)",$row))
                ->setCellValue("H".($row+3),'Total Owing')
                ->setCellValue("I".($row+3),sprintf("=%s-%s",("I".($row+1)),("I".($row+2)) ))
                ->setCellValue("B".($row+5),'Please pay on this invoice statement')
                ->mergeCells("B".($row+5).":"."I".($row+5));

            // Setting Alignments
            $activeSheet->getStyle("B17:I17")->getAlignment()->setHorizontal(Alignment::HORIZONTAL_CENTER);
            $activeSheet->getStyle("H8")->getAlignment()->setHorizontal(Alignment::HORIZONTAL_CENTER);
            $activeSheet->getStyle("B".($row+5))->getAlignment()->setHorizontal(Alignment::HORIZONTAL_CENTER);

            $symbol = 'GBP'; /*$order->currency? NumberFormat::FORMAT_CURRENCY_USD_SIMPLE: NumberFormat::FORMAT_CURRENCY_EUR_SIMPLE;*/

            /*dd($formats[$symbol]);*/
            /*$symbol2 = $currency == 'USD'? '_("$"* #,##0.00_);_("$"* \(#,##0.00\);_("$"* "-"??_);_(@_)': '_("£"* #,##0.00_);_("£"* \(#,##0.00\);_("£"* "-"??_);_(@_)';*/
            /*$activeSheet->getStyle("G18:I".($row+5))->getNumberFormat()->setFormatCode('_(""* #,##0.00_);_(""* \(#,##0.00\);_(""* "-"??_);_(@_)');*/
            /*$activeSheet->getStyle("G18:I".($row+5))->getNumberFormat()->setFormatCode($symbol);*/
            /*$activeSheet->getStyle("G18:I".($row+5))->getNumberFormat()->setFormatCode($formats[$symbol]);*/

            $activeSheet->getPageSetup()->setFitToPage(true)->setFitToWidth(1)->setFitToHeight(0);
            $activeSheet->getPageMargins()->setLeft(0.5)->setRight(0.5);
            $activeSheet->getPageSetup()->setPrintArea("A1:J".($row+6));
            $activeSheet->getPageSetup()->setHorizontalCentered(true);
        } catch (Exception $e) {
        }
//        $activeSheet->setCellValue('A1', 'Hello World !');

        $writer = new Xlsx($spreadsheet);
        try {

            header('Content-Type: application/vnd.ms-excel');
            header('Content-Disposition: attachment;filename="invoice.xls"');
            header('Cache-Control: max-age=0');

            $writer = IOFactory::createWriter($spreadsheet, 'Xls');
            $writer->save('php://output');

            header('Content-Type: application/vnd.openxmlformats-officedocument.spreadsheetml.sheet');
            header('Content-Disposition: attachment;filename="myfile.xlsx"');
            header('Cache-Control: max-age=0');

            $writer = IOFactory::createWriter($spreadsheet, 'Xlsx');
            $writer->save('php://output');

        } catch (Exception $e) {
            dd($e->getMessage());
        }
    }

    public function pdf(Request $request, Client $client, $from_date = '', $end_date = '')
    {
        if ($from_date && $end_date)
            $orders = $client->orders()->where('company_id', session('company_id'))->whereBetween('date', [$from_date, $end_date])->get();
        else
            $orders = $client->orders()->where('company_id', session('company_id'))->get();

        $data = [
            'orders' => $orders,
            'client' => $client,
            'from_date' => $from_date,
            'end_date' => $end_date,
        ];

        $pdf = \PDF::loadView('reports.invoice_pdf', $data);
//return $data;
        return $pdf->stream();

    }

    private function getCustomers()
    {
        $customers = Client::where('company_id', session('company_id'))->has('orders')->withCount(['Orders' => function($q){
            $q->where('order_type', 'new');

            if (request('design_type', 'all') != 'all')
                $q->where('design_type', request('design_type'));

            if (request('start_date') && request('end_date'))
                $q->whereBetween('created_at', [request('start_date'), request('end_date')]);

        }])->with(['latestOrder' => function($q){
            if (request('design_type', 'all') != 'all')
                $q->where('design_type', request('design_type'));

            if (request('start_date') && request('end_date'))
                $q->whereBetween('created_at', [request('start_date'), request('end_date')]);
        }])->get();

        return $customers;
    }

    protected function borderArray(){
        return $styleArray = [
            'borders' => [
                'outline' => [
                    'borderStyle' => Border::BORDER_MEDIUM,
                    'color' => ['rgb' => '999999'],
                ],
            ],
        ];
    }

}
