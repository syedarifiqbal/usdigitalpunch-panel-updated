<?php

namespace App\Http\Controllers;

use App\Models\Task;
use App\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class UserController extends BaseController
{
    protected $menu = 'users';

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        allowedOrRedirect('view-user');

        setting()->setExtraColumns(['company_id' => auth()->user()->company_id]);

//        setting(['email_from' => 'hdarif2@gmail.com'])->save();

        $data['menu'] = $this->menu;

        $data['submenu'] = 'view-user';

        $data['breadcrumb'] = 'users';

        $data['pageTitle'] = 'Users';

        return view('users.index', $data);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        allowedOrRedirect('view-user');

        $data['menu'] = $this->menu;

        $data['submenu'] = 'view-user';

    }

    /**
     * Display user's profile.
     *
     * @return \Illuminate\Http\Response
     */
    public function profile($from = '', $to ='')
    {
        $data['breadcrumb'] = 'profile';
        $data['menu'] = $this->menu;
        $data['submenu'] = 'view-profile';
        $data['tasks'] = auth()->user()->tasks()->orderBy('created_at', 'desc')->with('client')->simplePaginate(setting('table_page_length', 10));

        $from = request('from', date('Y-m-01'));
        $to = request('to', date('Y-m-t'));

        
        // if(auth()->id() == 1)
        // {
        //     auth()->loginUsingId(3);
        // }

        if(auth()->user()->sale_wrap_commision == 0)
        {
            $bonuses = auth()->user()->bonuses()
                ->with(['task' => function($q){
                    $q->select('id', 'date', 'name', 'design_type', 'order_type');
                }])->whereHas('task', function($query) use($from, $to){
                    $query->select('id');
                    if ($from && $to){
                        $query->whereBetween('date', [$from, $to]);
                    }
                })->get();
            $data['bonuses'] = $bonuses;
        }
        else{
            $tasks = Task::whereBetween('date', [$from, $to])
                ->where('order_type', 'new')
                        ->whereHas('client', function($query) use($from, $to){
                            $query->where('referred_by', auth()->id());
                        })->get();
            $data['bonusTasks'] = $tasks;
        }
        return view('users.profile', $data);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  User  $user
     * @return \Illuminate\Http\Response
     */
    public function edit(User $user)
    {
        allowedOrRedirect('edit-user');
        $data['menu'] = $this->menu;
        $data['user'] = $user;
        return view('users.create', $data);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  User $user
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, User $user)
    {
        allowedOrRedirect('edit-user');

        $request->validate([
            'name' => 'required|string|max:255',
            'email' => 'required|string|email|max:255|unique:users,id,'.$user->id,
            'father_name' => 'required',
            'gender' => 'required',
            'cnic' => 'required',
            'dob' => 'required',
            'phone' => 'required',
            'address' => 'required',
            'contact_person' => 'required',
            'contact_person_relation' => 'required',
            'contact_person_phone' => 'required',
            'contact_person_address' => 'required',
            'roles_id' => 'required|array|min:1'
        ]);
        $user->fill($request->all());
        $user->save();
        $user->roles()->sync($request->roles_id);
        return redirect()->route('users');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        allowedOrRedirect('delete-user');

    }
}
