<?php

namespace App\Http\Controllers;


use Illuminate\Http\Request;
use App\Models\Client;
use App\Models\ClientContact;


class ClientsController extends BaseController
{
    protected $menu = 'clients';

    public function index()
    {
        allowedOrRedirect('view-client');

        $data['pageTitle'] = 'Clients';

        $data['menu'] = $this->menu;

        $data['submenu'] = 'view-client';

        $data['breadcrumb'] = 'clients';

        $data['record'] = new Client;

        return view('clients.index', $data);
    }

    public function create()
    {
        allowedOrRedirect('add-client');

        $data['menu'] = $this->menu;

        $data['submenu'] = 'add-client';

        $data['pageTitle'] = 'Clients';

        $data['breadcrumb'] = 'clients.create';

        $data['record'] = new Client;

        return view('clients.create', $data);
    }

    public function store(Request $request)
    {
        allowedOrRedirect('add-client');

        $request->validate($this->getValidationRoles($request), [
            'contacts.*.email' => 'Contact email is required',
            'contacts.*.name' => 'Contact name is required', 
        ]);

        $client = new Client($request->all());

        $client->company_id = session('company_id');

        $client->is_active = true;

        $client->save();
        
        $client->saveContacts(request('contacts'));
        
        activity_log("Added new Client Successfully", $client);

        flash('Client Created Successfully', FLASH_SUCCESS);
        
        return redirect()->route('clients');
    }

    public function show($id)
    {
        allowedOrRedirect('view-client');

        $data['menu'] = $this->menu;

    }

    public function edit(Client $client)
    {
        allowedOrRedirect('edit-client');

        $client->load(['contacts', 'country', 'owner']);

        $data['menu'] = $this->menu;

        $data['pageTitle'] = 'Clients';

        $data['breadcrumb'] = 'clients.edit';

        $data['breadcrumbData'] = $client;

        $data['record'] = $client;

        return view('clients.create', $data);
    }

    public function update(Request $request, Client $client)
    {
        allowedOrRedirect('edit-client');

        $request->validate($this->getValidationRoles($request), [
            'contacts.*.email' => 'Contact email is required',
            'contacts.*.name' => 'Contact name is required', 
        ]);

        $client->fill($request->all());

        $client->save();
        
        $client->saveContacts(request('contacts'));
        
        activity_log("Added new Client Successfully", $client);

        flash('Client Updated Successfully', FLASH_SUCCESS);
        
        return redirect()->route('clients');
    }

    /**
     * @param Client $client
     * @return \Illuminate\Http\RedirectResponse
     * @throws \Exception
     */
    public function destroy(Client $client)
    {
        allowedOrRedirect('delete-client');

        $client->delete();

        flash('Client Deleted Successfully', FLASH_SUCCESS);
        
        return redirect()->route('clients');
    }

    protected function getValidationRoles($request)
    {

        $roles = [
            'name' => 'required',
            'country_id' => 'required|exists:countries,id',
            'referred_by' => 'required|exists:users,id',
            'email' => 'required|email',
            'address' => 'required',
        ];

        foreach($request->input('contacts') as $key => $contact)
        {
            if( $contact['email'] )
            {
                $roles["contacts.$key.name"] = 'required';
                $roles["contacts.$key.email"] = 'email|required';
            }
        }

        return $roles;
    }

}
