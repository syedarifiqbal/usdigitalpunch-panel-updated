<?php

namespace App\Events;

use Illuminate\Broadcasting\Channel;
use Illuminate\Queue\SerializesModels;
use Illuminate\Broadcasting\PrivateChannel;
use Illuminate\Broadcasting\PresenceChannel;
use Illuminate\Foundation\Events\Dispatchable;
use Illuminate\Broadcasting\InteractsWithSockets;
use Illuminate\Contracts\Broadcasting\ShouldBroadcast;

class TaskAssigned implements ShouldBroadcast
{
    use Dispatchable, InteractsWithSockets, SerializesModels;

    public $task;
    public $id;
    public $username;
    public $url;
    public $description;

    /**
     * Create a new event instance.
     *
     * @return void
     */
    public function __construct($user, $task)
    {
        $this->task = $task;
        $this->id = $user->id;
        $this->username = $user->name;
        $this->url = route('tasks.show', ['task' => $task->id]);
        $this->description = sprintf('Design %s assigned to you by %s', $task->name, auth()->user()->name);
    }

    /**
     * Get the channels the event should broadcast on.
     *
     * @return \Illuminate\Broadcasting\Channel|array
     */
    public function broadcastOn()
    {
        return new Channel("notification.{$this->id}");
    }
}
