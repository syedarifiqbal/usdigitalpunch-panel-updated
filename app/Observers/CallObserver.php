<?php

namespace App\Observers;

use App\Models\Sales\Call;
use App\Models\Sales\Lead;
use App\User;

class CallObserver
{
    /**
     * Handle to the User "created" event.
     *
     * @param  \App\Models\Sales\Call $call
     * @return void
     */
    public function created(Call $call)
    {
        $lead = Lead::find($call->lead->id);
        $lead->last_call_at = \Carbon\Carbon::now();
        $lead->last_feedback = $call->feedback;
        $lead->save();
    }

    /**
     * Handle the User "updated" event.
     *
     * @param  \App\Models\Sales\Call $call
     * @return void
     */
    public function updated(Call $call)
    {
        $latestCall = Call::where('lead_id', $call->lead_id)->latest()->first();
        if( $latestCall->id != $call->id ) return;

        $lead = Lead::find($call->lead->id);
        $lead->last_feedback = $call->feedback;
        $lead->save();
    }

    /**
     * Handle the User "deleted" event.
     *
     * @param  \App\Models\Sales\Call $call
     * @return void
     */
    public function deleted(Call $call)
    {
        //
    }
}