<?php

use Illuminate\Database\Seeder;

class RoleTableSeeder_Back extends Seeder
{
    public function __construct()
    {
        $this->roles = [

			['name' => 'Super Admin'],
			['name' => 'Admin'],
			['name' => 'designer'],

        ];

        $this->permissions = [

			[ 'module' => 'Users', 'label'=>'add', 'name' => 'add-user'],
			[ 'module' => 'Users', 'label'=>'edit', 'name' => 'edit-user'],
			[ 'module' => 'Users', 'label'=>'delete', 'name' => 'delete-user'],
			[ 'module' => 'Users', 'label'=>'view', 'name' => 'view-user'],

			[ 'module' => 'Tasks', 'label'=>'add', 'name' => 'add-task'],
			[ 'module' => 'Tasks', 'label'=>'edit', 'name' => 'edit-task'],
			[ 'module' => 'Tasks', 'label'=>'delete', 'name' => 'delete-task'],
			[ 'module' => 'Tasks', 'label'=>'view', 'name' => 'view-task'],
			[ 'module' => 'Tasks', 'label'=>'send', 'name' => 'send-task'],

			[ 'module' => 'Roles', 'label'=>'add', 'name' => 'add-role'],
			[ 'module' => 'Roles', 'label'=>'edit', 'name' => 'edit-role'],
			[ 'module' => 'Roles', 'label'=>'delete', 'name' => 'delete-role'],
			[ 'module' => 'Roles', 'label'=>'view', 'name' => 'view-role'],
			
        ];
        
        $this->role_permissions = [];

        foreach($this->permissions as $index => $permission)
        {
            $this->role_permissions[] = ['role_id' => 1, 'permission_id' => ($index+1)];
        }
    
    }

    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
//        foreach ($this->roles as $role) {
//            App\Models\Role::create($role);
//        }
//
//        foreach ($this->permissions as $permission) {
//            App\Models\Permission::create($permission);
//        };
//
//        foreach ($this->role_permissions as $rp) {
//            App\Models\PermissionRole::create($rp);
//        };
    }
}
