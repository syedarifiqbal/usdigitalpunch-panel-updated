<?php

use App\User;
use App\Models\RoleUser;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;
use Illuminate\Database\Eloquent\Model;

class DatabaseSeeder extends Seeder
{

    protected $toTruncate = ['countries', 'clients', 'companies', 'permission_role', 'permissions', 'users'];

    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        // Model::unguard();
        DB::statement("SET FOREIGN_KEY_CHECKS = 0;");
        foreach ($this->toTruncate as $table) {
            DB::table($table)->truncate();
        }
        DB::statement("SET FOREIGN_KEY_CHECKS = 1;");

        $this->call(CountryTableSeeder::class);
        $this->call(CompanyTableSeeder::class);
        $this->call(UsersTableSeeder::class);
        $this->call(RoleTableSeeder::class);
        $this->call(CustomersTableSeeder::class);

        // $user = factory(User::class)->create(['email'=>'arifiqbal@outlook.com', 'password'=>bcrypt('arifiqbal'), 'is_developer'=>true, 'is_admin'=>1])->first();

        RoleUser::create([ 'user_id'=>1, 'role_id'=>1 ]);

    }

}
