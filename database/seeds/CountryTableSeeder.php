<?php

use Illuminate\Database\Seeder;

class CountryTableSeeder extends Seeder
{
    public function __construct()
    {   
        $this->countries = json_decode(file_get_contents('https://restcountries.eu/rest/v2/all'));
    }

    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        foreach ($this->countries as $country) {

            // dd($country->currencies);
            App\Models\Country::create([
                'name' => $country->name,
                'code2' => $country->alpha2Code,
                'code3' => $country->alpha3Code,
                'location' => json_encode($country->latlng),
                'timezones' => $country->timezones[0],
                'currencies' => json_encode($country->currencies),
                'flag' => $country->flag,
            ]);
        }
    }
}
