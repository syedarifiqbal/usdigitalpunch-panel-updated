<?php

use Illuminate\Database\Seeder;

class UsersTableSeeder extends Seeder
{

    public function __construct()
    {
        $this->users = json_decode(file_get_contents('http://panel.usdigitalpunch.com/user_json.php?admin=arif'));

        $this->roles = [
            [ 'name' => 'super-admin'],
            [ 'name' => 'admin'],
            [ 'name' => 'sales'],
            [ 'name' => 'designer'],
        ];

        $this->permissions = [
            [ 'name' => 'create-user'],
            [ 'name' => 'delete-user'],
            [ 'name' => 'edit-user'],
            [ 'name' => 'read-user'],
        ];
    }

    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $company_id = optional(App\Models\Company::where('name', 'LIKE', 'Us Digital Punch')->first())->id;
        $gender = ['Male', 'Female', 'Other'];
        // dd('users ', count($this->users));
        foreach ($this->users as $user) {
            App\User::create([
                'name' => $user->first_name . ' ' . $user->last_name,
                'father_name' => $user->father_name,
                'gender' => $gender[(int)$user->gender],
                'cnic' => $user->cnic,
                'dob' => $user->dob,
                'phone' => $user->mobile,
                'address' => $user->address,
                'education' => $user->last_degree,
                'institute' => $user->institute,
                'passing_year' => $user->passing_year,
                'contact_person' => $user->person,
                'contact_person_relation' => $user->relation,
                'contact_person_phone' => $user->person_mobile,
                'contact_person_address' => $user->person_address,
                'bonus' => $user->commission,
                'additional_bonus' => $user->additional_commission,
                'revision_bonus' => $user->revision_commission,
                'edit_bonus' => $user->edit_commission,
                'sale_wrap_commision' => $user->sale_wrap_commission,
                'approval_bonus' => $user->approval_commission,
                'email' => $user->email,
                'password' => $user->password,
                'company_id' => $company_id,
                'is_active' => $user->is_active,
            ]);
        }

        $user_id = App\User::first()->id;

        $roles_id = [];
        $users_id = [];
        // foreach($this->roles as $role){
        //     $roles_id[] = factory('App\Models\Role')->create()->id;
        //     $users_id[] = $user_id;
        // }
        // foreach($this->roles as $role){
        //     $roles_id[] = factory('App\Models\Role')->create()->id;
        //     $users_id[] = factory('App\User')->create()->id;
        // }
    }

}
