<?php

use Illuminate\Database\Seeder;

class RoleTableSeeder extends Seeder
{
//     public function __construct()
//     {
//         $this->roles = [
// 			['name' => 'Super Admin'],
// 			['name' => 'Admin'],
// 			['name' => 'designer'],
// 		];

//         $this->permissions = [
// 			['module' => 'Clients', 'label'=>'Add', 'name' => 'add-client'],
// 			['module' => 'Clients', 'label'=>'Delete', 'name' => 'delete-client'],
// 			['module' => 'Clients', 'label'=>'edit', 'name' => 'edit-client'],
// 			['module' => 'Clients', 'label'=>'View', 'name' => 'view-client'],
// 			['module' => 'Reports', 'label'=>'Date-wise User Bonus', 'name' => 'view-bonus'],
// 			['module' => 'Roles', 'label'=>'add', 'name' => 'add-role'],
// 			['module' => 'Roles', 'label'=>'delete', 'name' => 'delete-role'],
// 			['module' => 'Roles', 'label'=>'edit', 'name' => 'edit-role'],
// 			['module' => 'Roles', 'label'=>'view', 'name' => 'view-role'],
// 			['module' => 'Tasks', 'label'=>'add', 'name' => 'add-task'],
// 			['module' => 'Tasks', 'label'=>'Can Assign to User', 'name' => 'can-assign-task'],
// 			['module' => 'Tasks', 'label'=>'delete', 'name' => 'delete-task'],
// 			['module' => 'Tasks', 'label'=>'edit', 'name' => 'edit-task'],
// 			['module' => 'Tasks', 'label'=>'send', 'name' => 'send-task'],
// 			['module' => 'Tasks', 'label'=>'view', 'name' => 'view-task'],
// 			['module' => 'Users', 'label'=>'add', 'name' => 'add-user'],
// 			['module' => 'Users', 'label'=>'delete', 'name' => 'delete-user'],
// 			['module' => 'Users', 'label'=>'edit', 'name' => 'edit-user'],
// 			['module' => 'Users', 'label'=>'view', 'name' => 'view-user'],
// 		];
//     }

//     /**
//      * Run the database seeds.
//      *
//      * @return void
//      */
//     public function run()
//     {
// //        App\Models\Role::truncate();
// //        App\Models\Permission::truncate();
// //        App\Models\PermissionRole::truncate();
        
//         foreach ($this->roles as $role) {
//             App\Models\Role::create($role);
//         }

//         foreach ($this->permissions as $permission) {
//             App\Models\Permission::create($permission);
//         };

//         $r_id = App\Models\Role::where('name', 'Super Admin')->first();
        
//         if ($r_id)
//         {
//             foreach (App\Models\Permission::all() as $perm) {
//                 App\Models\PermissionRole::create(['role_id' => $r_id->id, 'permission_id' => $perm->id]);
//             };
//         }
//     }
}