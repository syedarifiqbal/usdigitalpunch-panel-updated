<?php

use Illuminate\Database\Seeder;

class CustomersTableSeeder extends Seeder
{
    public function __construct()
    {   
        $this->clients = json_decode(file_get_contents('http://panel.usdigitalpunch.com/customer_json.php?admin=arif'));
        // dd($this->clients);
    }

    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $company_id = optional(App\Models\Company::where('name', 'LIKE', 'Us Digital Punch')->first())->id;
        
        $x = [];
        $x2 = [];
        foreach ($this->clients as $client) 
        {
            $offset = 17;

            if( $client->refered_by >= 23 )
            {
                $offset = 21;
            }
            
            if( $client->refered_by >= 36 )
            {
                $offset = 22;
            }
            $x[$client->refered_by] = 1;
            $x2[$client->refered_by - $offset] = 1;
            App\Models\Client::create([
                'name' => $client->name,
                'nick_name' => $client->nick_name,
                'email' => $client->email,
                'country_id' => $client->country,
                'is_active' => $client->active,
                'phone' => $client->cell,
                'price' => $client->price,
                'address' => $client->address,
                'instructions' => $client->instructions,
                'secret_instructions' => $client->secret_instructions,
                'referred_by' => $client->refered_by - $offset,
                'company_id' => $company_id,
            ]);
        }
        // dd($x, $x2);
        
        // name
        // nick_name
        // email
        // country_id
        // is_active
        // phone
        // price
        // address
        // instructions
        // secret_instructions
        // referred_by
        // company_id
        // deleted_at
        // created_at
        // updated_at
    }
}
