<?php

use Illuminate\Database\Seeder;

class CompanyTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        factory(App\Models\Company::class)->create(['name' => 'Us Digital Punch', 'email' => 'info@usdigitalpunch.com']);
        factory(App\Models\Company::class)->create(['name' => 'Aster Digital Punch', 'email' => 'info@asterdigitalpunch.com']);
    }
}
