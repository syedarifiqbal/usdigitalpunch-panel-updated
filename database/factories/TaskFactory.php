<?php

use Faker\Generator as Faker;

$factory->define(App\Models\Task::class, function (Faker $faker) {

    $design_types = ['Digitizing', 'Vector', 'Quote'];
    $order_types = ['New', 'Revision', 'Edit'];
    $status = ['Pending', 'Complete', 'RFS'];

    return [ 
        'date' => $faker->date('d/m/Y'), 
        'delivery_date' => $faker->date('d/m/Y'), 
        'name' => $faker->name, 
        'po_number' => $faker->buildingNumber, 
        'currency' => $faker->currencyCode, 
        'price' => $faker->randomNumber(2), 
        'required_format' => ['.pdf', '.ext'], 
        'unit' => 'in', 
        'company_id' => function(){
            return factory(App\Models\Company::class)->create()->id;
        }, 
        'width' => $faker->randomNumber(2), 
        'height' => $faker->randomNumber(2), 
        'status' => $status[rand(0, 2)],
        'approved_by' => 0, 
        'placement' => $faker->word, 
        'design_type' => $design_types[rand(0, 2)], 
        'priority' => 'normal', 
        'parent_id' => null, 
        'order_type' => $order_types[rand(0, 2)], 
        'is_approved' => 0, 
        'designer_id' => function () {
            return factory(App\User::class)->create()->id;
        }, 
        'client_id' => function () {
            return factory(App\Models\Client::class)->create()->id;
        }, 
        'remarks' => null, 
        'delivery_width' => null, 
        'delivery_height' => null, 
        'stitches' => 0, 
        'delivery_remarks' => null 
    ];

});
