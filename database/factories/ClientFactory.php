<?php

use Faker\Generator as Faker;

$factory->define(App\Models\Client::class, function (Faker $faker) {
    return [ 
        'name' => $faker->name, 
        'nick_name' => $faker->name, 
        'email' => $faker->safeEmail, 
        'country_id' => 1, 
        'is_active' => 1, 
        'phone' => $faker->phoneNumber, 
        'address' => $faker->address, 
        'instructions' => $faker->sentence, 
        'secret_instructions' => $faker->sentence, 
        'referred_by' => 1, 
        'company_id' => function(){
            return factory(App\Models\Company::class)->create()->id;
        }, 
        'country_id' => function(){
            return factory(App\Models\Country::class)->create()->id;
        }
    ];
});
