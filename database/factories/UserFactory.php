<?php

use Faker\Generator as Faker;

/*
|--------------------------------------------------------------------------
| Model Factories
|--------------------------------------------------------------------------
|
| This directory should contain each of the model factory definitions for
| your application. Factories provide a convenient way to generate new
| model instances for testing / seeding your application's database.
|
 */

$factory->define(App\User::class, function (Faker $faker) {
    return [
        'name' => $faker->name,
        'father_name' => $faker->name,
        'gender' => 'male',
        'email' => $faker->unique()->safeEmail,
        'password' => '$2y$10$TKh8H1.PfQx37YgCzwiKb.KjNyWgaHb9cbcoQgdIVFlYg7B77UdFm', // secret
        'cnic' => '4240152594617',
        'dob' => '1990-01-20',
        'phone' => '03452650236',
        'address' => $faker->sentence,
        'contact_person' => $faker->name,
        'contact_person_relation' => $faker->word,
        'contact_person_phone' => '03452650236',
        'contact_person_address' => $faker->sentence,
        'remember_token' => str_random(10),
    ];
});

$factory->define(App\Models\Role::class, function (Faker $faker) {
    return [
        'name' => $faker->name,
        'label' => $faker->firstName
    ];
});

$factory->define(App\Models\Permission::class, function (Faker $faker) {
    return [
        'name' => $faker->name,
        'label' => $faker->firstName,
        'module' => ''
    ];

});

$factory->define(App\Models\PermissionRole::class, function (Faker $faker) {
    return [
        'permission_id' => 1,
        'role_id' => 1
    ];
});

$factory->define(App\Models\RoleUser::class, function (Faker $faker) {
    return [
        'user_id' => 1,
        'role_id' => 1
    ];
});

$factory->define(App\Models\Company::class, function (Faker $faker) 
{
    // $country_id = App\Models\Country::inRandomOrder()->first()->id;

    return [
        'name' => '',
        'email' => $faker->unique()->safeEmail,
        'phone' => $faker->phoneNumber,
        'address' => $faker->address,
        'country_id' => 1,
    ];

});

$factory->define(App\Models\Country::class, function (Faker $faker) 
{
    return [
        'name' => $faker->name,
        'code2' => $faker->randomNumber(2),
        'code3' => $faker->randomNumber(3),
        'location' => $faker->name,
        'timezones' => $faker->name,
        'currencies' => $faker->currencyCode,
        'flag' => ''
    ];

});