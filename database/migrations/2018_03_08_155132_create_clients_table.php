<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateClientsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('clients', function (Blueprint $table) {
            $table->increments('id');
            $table->string('name');
            $table->string('nick_name');
            $table->string('email');
            $table->unsignedInteger('country_id');
            $table->boolean('is_active')->default(true);
            $table->string('phone')->nullable();
            $table->float('price')->default(0);
            $table->string('address', 255)->nullable();
            $table->string('instructions', 255)->nullable();
            $table->string('secret_instructions', 255)->nullable();
            $table->unsignedInteger('referred_by');
            $table->unsignedInteger('company_id');
            $table->softDeletes();
            $table->timestamps();

            $table->foreign('country_id')->references('id')->on('countries');
            $table->foreign('company_id')->references('id')->on('companies');
            $table->foreign('referred_by')->references('id')->on('users');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('clients');
    }
}
