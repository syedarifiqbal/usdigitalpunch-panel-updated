<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTasksTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('tasks', function (Blueprint $table) {
            $table->increments('id');
            $table->date('date');
            $table->date('delivery_date');
            $table->string('name');
            $table->string('po_number');
            $table->string('required_format');
            $table->string('unit', 10);
            $table->smallInteger('width');
            $table->smallInteger('height');
            $table->string('status')->default('pending');
            $table->string('placement');
            $table->float('price')->default(0);
            $table->string('currency', 5)->nullable();
            $table->string('design_type');
            $table->string('priority');
            $table->string('parent_id')->nullable();
            $table->string('order_type')->default('new');
            $table->boolean('is_approved')->default(0);
            $table->unsignedInteger('designer_id')->nullable();
            $table->unsignedInteger('client_id');
            $table->unsignedInteger('company_id');
            $table->unsignedInteger('approved_by')->nullable();
            $table->text('remarks')->nullable();
            $table->smallInteger('delivery_width')->nullable();
            $table->smallInteger('delivery_height')->nullable();
            $table->string('stitches')->nullable();
            $table->text('delivery_remarks')->nullable();
            $table->timestamps();

            $table->foreign('client_id')->references('id')->on('clients');
            // $table->foreign('client_id')->references('id')->on('customers');

        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('tasks');
    }
}
