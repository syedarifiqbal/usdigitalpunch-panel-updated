<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateUsersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('users', function (Blueprint $table) {
            $table->increments('id');
            $table->string('name');
            $table->string('father_name')->nullable();
            $table->string('gender')->nullable();
            $table->string('cnic')->nullable();
            $table->date('dob')->nullable();
            $table->string('phone')->nullable();
            $table->string('address')->nullable();
            $table->string('education')->nullable();
            $table->string('institute')->nullable();
            $table->string('passing_year')->nullable();
            $table->string('contact_person')->nullable();
            $table->string('contact_person_relation')->nullable();
            $table->string('contact_person_phone')->nullable();
            $table->string('contact_person_address')->nullable();
            $table->decimal('bonus')->default(0);
            $table->decimal('additional_bonus')->default(0);
            $table->decimal('revision_bonus')->default(0);
            $table->decimal('edit_bonus')->default(0);
            $table->decimal('sale_wrap_commision')->default(0);
            $table->decimal('approval_bonus')->default(0);
            $table->string('email')->unique();
            $table->string('password');
            $table->unsignedInteger('company_id')->nullable();
            $table->boolean('is_active')->default(1);
            $table->boolean('is_admin')->default(0);
            $table->boolean('is_developer')->default(0);
            $table->rememberToken();
            $table->softDeletes();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('users');
    }
}
