'use strict';

$( '#addContact' ).on('click', function (e) {
    
    e.preventDefault();

    var tr = '<tr>' +
            '<td>' +
                '<div class="md-input-wrapper">' +
                    '<input type="text" name="contacts[' + $('#contactForm tbody tr').size() + '][name]" class="md-valid md-form-control" value="">' +
                    // '<label></label>'
                '<span class="md-line"></span></div>' +
            '</td>' +
            
            '<td>' +
                '<div class="md-input-wrapper">' +
                    '<input type="text" name="contacts[' + $('#contactForm tbody tr').size() + '][email]" class="md-valid md-form-control" value="">' +
                    // '<label></label>'
                    '<span class="md-line"></span></div>' +
            '</td>' +
            
            '<td>' +
                '<div class="md-input-wrapper">' +
                    '<input type="text" name="contacts[' + $('#contactForm tbody tr').size() + '][phone]" class="md-valid md-form-control" value="">' +
                    // '<label></label>'
                    '<span class="md-line"></span></div>' +
            '</td>' +
        '</tr>';
// console.log(tr);
    $('#contactForm tbody').append(tr);

});